phase_counter = False
duo = False
end_monomer = True
position_index = 0
previous = None
begin = None
for base in index:
	if base >= phase and not phase_counter:
		for i in range(position_index,position_previous,-1):
			if index[i] <= 0:
				begin = i
		if begin is None:
			begin = position_index
		phase_counter = True
	if base < previous and base > 0:
		duo = True
	if base >= phase and duo and end_monomer:
		sequence2 = '\n'.join((nucleotide)[pos:pos+60] for pos in range(begin, position_index + 1, 60))
		results.write( "%s\n" %sequence2 )
		end_monomer = False
	if base > 0:
		previous = base
		position_previous = position_index
	position_index += 1
