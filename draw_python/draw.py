#! /usr/local/bin/python3.4


import argparse
import csv
import svgwrite

def read_karyo_data(file):
	data={}
	h=open(file,'r')
	tab=csv.reader(h, delimiter='\t')
	for row in tab:
		print(row)
		if row[0] not in data:
			data[row[0]]={'length': 0, 'bands': []}
			print(row[0])
		if row[0] in data:
			if int(row[2]) > int(data[row[0]]['length']):
				data[row[0]]['length']=int(row[2])
				bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
				data[row[0]]['bands'].append(bd)
				print(data[row[0]]['length'])
	return(data)
		
def get_max_length(data):
	mx=0
	for chr in data:
		if data[chr]['length'] > mx:
			mx=data[chr]['length']
	return(mx)
	
			
parser = argparse.ArgumentParser()
parser.add_argument('-i', dest='infile', 
                   help='input file')
parser.add_argument('-o', dest='outfile', 
                   help='output file')
args = parser.parse_args()


width=1800
heigh=600
heigh_plus=100

data=read_karyo_data(args.infile)
nb_chr=len(data)
max_length=get_max_length(data)
print(max_length)

svg_document = svgwrite.Drawing(filename = args.outfile,
                                size = (width, heigh+heigh_plus))
				


print(svg_document.tostring())


x=width/nb_chr
y=heigh/max_length

nb=0

for chr in data:
	ins=(nb*x,  heigh-data[chr]['length']*y)
	siz=(x/2, data[chr]['length']*y)
	svg_document.add(svg_document.rect(insert = ins,
                                   size = siz,
                                   stroke_width = "1",
                                   stroke = "black",
                                   fill = "rgb(255,255,255)"))
	svg_document.add(svg_document.text(chr,
                                   insert = (nb*x, heigh)))
	st=heigh-data[chr]['length']*y
	for bd in data[chr]['bands']:
		ins=(nb*x, st+bd['start']*y+0.5*heigh_plus*y)
		siz=(x/2, (bd['end']-bd['start'])*y)
		print(siz)
		print(ins)
		print(bd)
		col="rgb(255,255,255)"
		if bd['type'] == 'gneg':
			col="rgb(255,255,255)"
		elif bd['type'] == 'gpos100':
			col='rgb(0, 0, 0)'
		elif bd['type'] == 'gpos75':
			col='rgb(169, 169, 169)'
		elif bd['type'] == 'gpos50':
			col='rgb(192, 192, 192)'
		elif bd['type'] == 'gpos25':
			col='rgb(211, 211, 211)'
		elif bd['type'] == 'acen':
			col='rgb( 0, 0, 255)'
			
		svg_document.add(svg_document.rect(insert = ins,
                                   size = siz,
                                   stroke_width = "1",
                                   stroke = "black",
                                   fill = col))
		
	#print(data[chr])
	nb=nb+1
	
svg_document.save()
