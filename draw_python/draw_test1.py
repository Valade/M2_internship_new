#! /usr/local/bin/python3.4


import argparse
import csv
import svgwrite

def read_karyo_data(file):
	data={}
	h=open(file,'r')
	tab=csv.reader(h, delimiter='\t')
	for row in tab:
		#print(row)
		if row[0] not in data:
			data[row[0]]={'length': 0, 'bands': []}
			#print(row[0])
		if row[0] in data:
			if int(row[2]) > int(data[row[0]]['length']):
				data[row[0]]['length']=int(row[2])
				bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
				data[row[0]]['bands'].append(bd)
				#print(data[row[0]]['length'])
	return(data)
		
def get_max_length(data):
	mx=0
	for chr in data:
		if data[chr]['length'] > mx:
			mx=data[chr]['length']
	return(mx)
	
			
parser = argparse.ArgumentParser()
parser.add_argument('-i', dest='infile', 
				   help='input file')
parser.add_argument('-o', dest='outfile', 
				   help='output file')
args = parser.parse_args()


width=18000
heigh=6000
heigh_plus=1000

data=read_karyo_data(args.infile)
nb_chr=len(data)
max_length=get_max_length(data)
#print(max_length)

svg_document = svgwrite.Drawing(filename = args.outfile,
												size = (width, heigh+heigh_plus),profile='full')
				


#print(svg_document.tostring())

#Fraction de la largeur pour chaque chromosome
x=width/nb_chr
#Ratio nb de pixel par pb
y=heigh/max_length
length = 1000
nb=0

for chr in data:
	print(chr)
	ins=(nb*x,  heigh - data[chr]['length']*y)
	siz=(x/2, data[chr]['length']*y)
	#Ajouter un element
	#marker = svg_document.marker(insert=(1000,5000), size=(100000,100000000))
	svg_document.add(svg_document.rect(insert = ins,
									#largeur/hauteur
									size = siz,
									#largeur de trait
									stroke_width = "1",
									#couleur
									stroke = "black",
									#remplissage en blanc
									fill = "rgb(255,255,255)"))
	g = svg_document.g(style="font-size:200;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
	g.add(svg_document.text(chr[3:],
									insert = (nb*x, heigh + 400),
									fill = 'black'
									))
	svg_document.add(g)
	#svg_document.add(svg_document.text.TextArea(chr[3:],x = nb*x, y =  heigh + 800, width = x, height = (nb*x, heigh + 400) - (heigh - data[chr]['length']*y)))
	st=heigh - data[chr]['length']*y
	for bd in data[chr]['bands']:
		ins=(nb*x, st+bd['start']*y+0.5*heigh_plus*y)
		siz=(x/2, (bd['end']-bd['start'])*y)
		print(siz)
		print(ins)
		print("bd")
		print(bd)
		#noir
		col="rgb(255,255,255)"
		if bd['type'] == 'gneg':
			#blanc
			col="rgb(255,255,255)"
			#Ajouter un rectangle pour les bandes
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
		elif bd['type'] == 'gpos100':
			#noir
			col='rgb(0, 0, 0)'
			#Ajouter un rectangle pour les bandes
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
		elif bd['type'] == 'gpos75':
			#gris foncé
			col='rgb(169, 169, 169)'
			#Ajouter un rectangle pour les bandes
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
		elif bd['type'] == 'gpos50':
			#gris clair
			col='rgb(192, 192, 192)'
			#Ajouter un rectangle pour les bandes
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
		elif bd['type'] == 'gpos25':
			#gris très clair
			col='rgb(211, 211, 211)'
			#Ajouter un rectangle pour les bandes
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
		elif bd['type'] == 'acen':
			#bleu
			col='rgb( 0, 0, 255)'
			#Ajouter un rectangle pour les bandes
			ins=((nb*x)+((x/2)*(1/4)), st+bd['start']*y+0.5*heigh_plus*y)
			siz = (x/4, (bd['end']-bd['start'])*y)
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
		elif bd['type'] == 'gvar':
			#blanc
			col='rgb( 255, 255, 255)'
			#Ajouter un rectangle pour les bandes
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
		elif bd['type'] == 'stalk':
			#rouge
			col='rgb( 200, 0, 0)'
			#Ajouter un rectangle pour les bandes
			svg_document.add(svg_document.rect(insert = ins,
												size = siz,
												stroke_width = "1",
												stroke = "black",
												fill = col))
	#print(data[chr])
	nb=nb+1
	
svg_document.save()
