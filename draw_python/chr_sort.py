#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
import re
def update_cytoband(cyto,output):
	"""add the cytobands informations(name, end, begin, giemsa stain)
	 on the chromosomes documents"""
	for line in cyto:
		infos = line.rstrip("\n").split("\t")
		parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", infos[0])
		if parse:
			output.write(line)
# Ouverture du fichier source
source = open("Pt_cytoBandIdeo.txt", "r")
 
# Ouverture du fichier destination
destination = open("Pt_cytoBandIdeo_test.txt", "w")
 
 
# Appeler la fonction de traitement
update_cytoband(source,destination)
 
 
# Fermeture du fichier destination
destination.close()
 
# Fermerture du fichier source
source.close()
