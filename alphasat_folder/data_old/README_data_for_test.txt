FILES:

nt_011630.fst:                  contig sequence (Human)
nt_011630_region.region.fst:    block sequences
nt_011630_region.monomers.fst:  monomer sequences

Pt_chr1.fa:                    chromosome sequence (Chimp)
Pt_chr1.region.fa:             block sequences


NAMING: 
NT_011630.14_230270_262823_-1                  contigName_begin_end_strand
NT_011630.14_230270_262823_-1:29740-29910      contigName_begin_end_strand:begin_end 

strand 1 and -1 (reverse strand)
begin/end for blocks:   are positions on the contig
begin/end for monomers: are positions on the block (becarefull when the block is in the the reverse strand)
