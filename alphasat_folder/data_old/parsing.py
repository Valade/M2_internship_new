#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import sys

def parsing(entree,sortie1,sortie2):
	param = "chr1"
	param2 = "chr2"
	for ligne in SeqIO.parse(entree,"fasta"):
		if param in str(ligne.id):
			#sortie1.write(str(ligne.description)+"\n")
			#sequence = str(ligne.seq)
			#print(sequence)
			SeqIO.write(ligne,sortie1,"fasta")
		if param2 in str(ligne.id):
			#sortie2.write(str(ligne.description)+"\n")
			SeqIO.write(ligne,sortie2,"fasta")
			

#monFichierEntree = open("Homo_sapiens_monomers.fst","r")
#monFichierSortie1 = open("Hs_monomers_set1.fst","w")
#monFichierSortie2 = open("Hs_monomers_set2.fst","w")

monFichierEntree = open(sys.argv[1],"r")
monFichierSortie1 = open(sys.argv[2],"w")
monFichierSortie2 = open(sys.argv[3],"w")

try:
	parsing(monFichierEntree,monFichierSortie1,monFichierSortie2)
finally:
	monFichierEntree.close()
	monFichierSortie1.close()
	monFichierSortie2.close()
