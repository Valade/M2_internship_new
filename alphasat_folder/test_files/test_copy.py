#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import io
import psycopg2
import psycopg2.extras
import time

buffer_file = io.StringIO()
monoName = "name"
block_id = 1
sequence_id = 1
beginMono = 0
end = 1000
monomer_seq = ["a","g","c","t"]
monomer = [1,2,3,4]
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)


cursor.execute('INSERT INTO Monomer(monomer_index) VALUES (%s);',(monomer,))
buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginMono),str(end)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+"{"+",".join(map(str,monomer_seq))+"}"+"\n")
buffer_file.seek(0)
#cursor.execute("INSERT INTO Taxon(taxon_name) VALUES('blabla');")
T1 = time.time()
C1 = time.clock()
try:
	cursor.copy_expert("COPY Monomer(monomer_name,block_id,sequence_id,monomer_begin,monomer_end,monomer_index,monomer_nucleotide) FROM STDIN;",buffer_file)
	#cursor.execute("COPY Monomer(monomer_name,block_id,sequence_id,monomer_begin,monomer_end,monomer_index,monomer_nucleotide) FROM 'buffer_file';")
	#cursor.copy_from(buffer_file,'Monomer',sep='\t',columns=('monomer_name','block_id','sequence_id','monomer_begin','monomer_end','monomer_index','monomer_nucleotide',))
except psycopg2.Error as e:
	print (e.pgerror)
T2 = time.time()
C2 = time.clock()
print("Temps total de la commande :  %f" %(T2 - T1))
print("Temps CPU de la commande : %f" %(C2 - C1))
conn.commit()
#cursor.execute("INSERT INTO Taxon(taxon_name) VALUES('blabla1');")
cursor.close()
##cursor.execute("INSERT INTO Taxon(taxon_name) VALUES('blabla2');")
conn.close()
#cursor.execute("INSERT INTO Taxon(taxon_name) VALUES('blabla3');")
