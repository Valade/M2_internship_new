# -*- coding: utf-8 -*-
from ..Mpwo import *
from ..Gpwo import *

def getConfigValue(varName):
    return globals()[varName]

def getObjectClass(className):
    return globals()[className]


def getObjectFromQuery(className, query):
    objectClass = getObjectClass(className)
    obj = objectClass(query=query)
    return obj


def getObjectFromId(className, mid):
    if className == 'Folder':
        f = Folder(mid=mid)
        f.fetch()
        collection = f.getValue('collection')
        className = collection + className
    objectClass = getObjectClass(className)
    obj = objectClass(mid=mid)
    return obj


def getObjectFromDocument(className, document):
    objectClass = getObjectClass(className)
    obj = objectClass(document=document)
    return obj


def getCollectionFromQuery(className, query):
    colClass = getObjectClass(className + 's')
    col = colClass(query=query)
    return col


def getCollectionFromDocuments(className, documents):
    colClass = getObjectClass(className + 's')
    col = colClass(documents=documents)
    return col


def getRelated(obj, className):
    colClass = getObjectClass(className + 's')
    col = colClass(query=obj.getForeignQuery())
    return col


def executeObjectMethod(className, actionName, mid, data):
    cls = getObjectClass(className)
    obj = cls(mid=mid)
    mtd = getattr(cls, actionName)
    return mtd(obj, data)

def getFolderNames(self, className, mid, folderField='folders', titleField='title'):
        names = []
        for mid in self.getValue(folderField):
            f = MongoPersistentWebFolder(mid=mid)
            f.fetch()
            for dirname in f.getWebPaths():
                names.append(dirname + '/' + self.getValue(titleField))
        return names