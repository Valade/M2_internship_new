# -*- coding: utf-8 -*-
import os
UPLOAD_FOLDER = './upload'
TEMP_FOLDER = os.getcwd() + '/temp'
FASTQC_FOLDER = os.getcwd() + '/FastQC'
SSAHA2_FOLDER = os.getcwd() + '/ssaha2'
MULTALIN_FOLDER = os.getcwd() + '/multalin'
TEMP_URL = './temp'

ROOTURL = 'http://rddm503jc4'
PORT = 8000

FILE_EXTENSIONS = {
    'tar': ['gz', 'bz2'],
    'zip': ['zip'],
    'fasta': ['fas', 'fast', 'fasta', 'fst', 'mult', 'txt'],
    'fastq': ['fastq', 'fastaq', 'fastqsanger']
}

FILES_COLUMNS = {
    'Dsb': [
        {'label': 'File name', 'field': 'filename'},
        {'label': 'File type', 'field': 'type'},
        {'label': 'Records', 'field': 'records'},
        {'label': 'Status', 'field': 'status'}
    ]
}

PAGES = [
    'Tigrre',
    'Arche'
]

START_CSS_FILES = [
    'js/layout/layout-latest.css',
    'js/jqTree/jqtree.css',
    'js/jquery-ui/development-bundle/themes/base/jquery.ui.tooltip.css',
    'js/jquery.alerts/jquery.alerts.css',
    'js/jQuery-File-Upload/css/jquery.fileupload-ui.css',
    'stylesheets/jquery-ui-bootstrap/assets/css/bootstrap.min.css',
    'stylesheets/jquery-ui-bootstrap/css/custom-theme/jquery-ui-1.10.2.custom.css',
    'stylesheets/jquery-ui-bootstrap/assets/css/font-awesome.min.css',
    'stylesheets/jquery-ui-bootstrap/assets/css/docs.css',
    'stylesheets/jquery-ui-bootstrap/assets/js/google-code-prettify/prettify.css',
    'stylesheets/misc.css',
    'js/dataTables/media/css/jquery.dataTables.css',
    'js/dataTables/media/css/jquery.dataTables_themeroller.css',
    'js/dataTables/media/css/demo_table_jui.css',
]

START_ICON_FILES = [
    'stylesheets/jquery-ui-bootstrap/assets/ico/apple-touch-icon-144-precomposed.png',
    'stylesheets/jquery-ui-bootstrap/assets/ico/apple-touch-icon-114-precomposed.png',
    'stylesheets/jquery-ui-bootstrap/assets/ico/apple-touch-icon-72-precomposed.png',
    'stylesheets/jquery-ui-bootstrap/assets/ico/apple-touch-icon-57-precomposed.png',
    'stylesheets/jquery-ui-bootstrap/assets/ico/favicon.png'
]

START_JQUERY_FILES = [
    'js/jquery-ui/js/jquery-2.0.0.min.js',
    'js/jquery-migrate-1.1.1.min.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.core.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.widget.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.position.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.mouse.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.tabs.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.accordion.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.button.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.draggable.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.droppable.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.sortable.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.resizable.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.dialog.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.slider.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.spinner.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.menu.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.progressbar.js',
    'js/jquery-ui/development-bundle/ui/jquery.ui.tooltip.js',
    'js/jQuery-File-Upload/js/jquery.iframe-transport.js',
    'js/jQuery-File-Upload/js/jquery.fileupload.js',
    'js/jQuery-File-Upload/js/jquery.fileupload-process.js',
    'js/jquery.alerts/jquery.alerts.js',
    'js/form/jquery.form.js',
    'js/dataTables/media/js/jquery.dataTables.js',
    'js/dataTables/extras/TableTools/media/js/ZeroClipboard.js',
    'js/dataTables/extras/TableTools/media/js/TableTools.js',
   'js/jqTree/tree.jquery.js',
   'js/cookie/jquery.cookie.js',
   'js/layout/jquery.layout-latest.min.js',
   'js/jquery.dform/dist/jquery.dform-1.0.1.min.js',
   'js/underscore/underscore-min.js',
   'js/backbone/backbone-min.js'
]

END_JQUERY_FILES = [
    'stylesheets/jquery-ui-bootstrap/assets/js/bootstrap.min.js',
    'stylesheets/jquery-ui-bootstrap/assets/js/holder.js',
    'stylesheets/jquery-ui-bootstrap/assets/js/google-code-prettify/prettify.js'
]

CORE_GPWO_JS_FILES = [
    'js/gpwo/widget.js',
    'js/gpwo/layoutInit.js',
    'js/gpwo/base.js',
    'js/gpwo/tab.js',
    'js/gpwo/work.js',
    'js/gpwo/panel.js',
    'js/gpwo/form.js',
    'js/gpwo/table.js',
    'js/gpwo/remote.js',
    'js/gpwo/user.js',
    'js/gpwo/tree.js',
    'js/gpwo/folder.js',
    'js/gpwo/view.js',
    'js/gpwo/chart.js'
]

EXT_GPWO_JS_FILES = [
    'Dsb/jsCode',
    'Source/jsCode',
    'Strand/jsCode',
    'BlastAlignment/jsCode'
]

BLAST_OUTPUT_FIELDS = {
    'qseqid': 'Query Seq-id',
    'qgi': 'Query GI',
    'qacc': 'Query accession',
    'qaccver': 'Query accession.version',
    'qlen': 'Query sequence length',
    'sseqid': 'Subject Seq-id',
    'sallseqid': 'All subject Seq-id(s), separated by a ;',
    'sgi': 'Subject GI',
    'sallgi': 'All subject GIs',
    'sacc': 'Subject accession',
    'saccver': 'Subject accession.version',
    'sallacc': 'All subject accessions',
    'slen': 'Subject sequence length',
    'qstart': 'Start of alignment in query',
    'qend': 'End of alignment in query',
    'sstart': 'Start of alignment in subject',
    'send': 'End of alignment in subject',
    'qseq': 'Aligned part of query sequence',
    'sseq': 'Aligned part of subject sequence',
    'evalue': 'Expect value',
    'bitscore': 'Bit score',
    'score': 'Raw score',
    'length': 'Alignment length',
    'pident': 'Percentage of identical matches',
    'nident': 'Number of identical matches',
    'mismatch': 'Number of mismatches',
    'positive': 'Number of positive-scoring matches',
    'gapopen': 'Number of gap openings',
    'gaps': 'Total number of gaps',
    'ppos': 'Percentage of positive-scoring matches',
    'frames': 'Query and subject frames separated by a slash',
    'qframe': 'Query frame',
    'sframe': 'Subject frame',
    'btop': 'Blast traceback operations (BTOP)'
}
