# -*- coding: utf-8 -*-
import pymongo


MONGO = pymongo.Connection('localhost', 27017)
INFO = MONGO.fsync(lock=True)
DECO = MONGO.close()

CONNECTOR = {
    'host': 'localhost',
    'port': 27017,
    'mongo': MONGO,
    'deco' : DECO,
    'info' : INFO,
    'db': 'dna'
}

DB = 'test'
DB1 = 'test1'
DB2 = 'test2'
DB3 = 'test3'
DB4 = 'test4'
DB5 = 'test5'
