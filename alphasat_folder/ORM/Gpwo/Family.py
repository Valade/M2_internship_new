# -*- coding: utf-8 -*-
from ..Mpwo.mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from ..Mpwo.mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders


class Family(Folder):

    def getCollection(self):
        return 'Family'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)


class Families(Folders):

    def getCollection(self):
        return 'Family'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
