from ..Mpwo.mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from ..Mpwo.mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders


class Monomer(Object):

    def getCollection(self):
        return 'Monomer'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
    
    def setBlockFromName(blockName):
        return setParentFromField(Block, "name", blockName, create = True)



class Monomers(Objects):

    def getCollection(self):
        return 'Monomer'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
    
    
    def setBlockFromName(blockName):
        return setParentFromField(Block, "name", blockName, create = True)
        
    def getChild(self, document):
        return Monomer(document=document)

