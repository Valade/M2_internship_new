from ..Mpwo.mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from ..Mpwo.mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders


class ChromosomeChunk(Object):

    def getCollection(self):
        return 'ChromosomeChunk'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)


class ChromosomeChunks(Objects):

    def getCollection(self):
        return 'ChromosomeChunk'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
