# -*- coding: utf-8 -*-
from ..Mpwo.mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from ..Mpwo.mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders


class Hor(Object):

    def getCollection(self):
        return 'Hor'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)


class Hors(Objects):

    def getCollection(self):
        return 'Hor'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
