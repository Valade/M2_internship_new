# -*- coding: utf-8 -*-
from ..Mpwo.mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from ..Mpwo.mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders


class Taxon(Object):

    def getCollection(self):
        return 'Taxon'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
    
    def getChromosomes(self):
        return Chromosomes(query = self.getForeignQuery())


class Taxons(Objects):

    def getCollection(self):
        return 'Taxon'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
    
    def getChild(self, document):
        return Taxon(document=document)

from .Chromosome import Chromosomes
