# -*- coding: utf-8 -*-
from .mpwo import MongoPersistentWebObject


class MongoPersistentWebUser(MongoPersistentWebObject):

    def getCollection(self):
        return 'webUser'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)


from .mpwo import MongoPersistentWebObjects


class MongoPersistentWebUsers(MongoPersistentWebObjects):

    def getCollection(self):
        return 'webUser'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)

