# -*- coding: utf-8 -*-
from bson.objectid import ObjectId
from ..ObjectRouter.connector import *
from tempfile import *
import gridfs
import subprocess
import re
import numpy as np
from pandas import Series, DataFrame, concat
from bson.objectid import ObjectId
import matplotlib
matplotlib.use('Agg')
import zipfile
from pylab import *
import scipy.stats
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import io
import csv
#import xlsxwriter
from multiprocessing import *
from multiprocessing.pool import ThreadPool
from threading import Thread
import urllib
import urllib.request
from Bio import SeqIO
TEMP_FOLDER = gettempdir()

class MongoPersistentWebObject(object):

    def __init__(self, **kwargs):

        self._db = kwargs.pop('db', DB)
        self._connector = kwargs.pop('connector', CONNECTOR)
        query = kwargs.pop('query', None)
        if query is not None:
            try:
                self._document = self._connector['mongo'][self._db][self.collection].find_one(query)
                self._id = self._document['_id']
                self._query = {'_id': self._id}
            except:
                self._document = query
                self._id = None
                self._query = None
        else:
            self._document = kwargs.pop('document', {})
            mid = kwargs.pop('mid', None)
            if mid is None:
                if '_id' in self._document:
                    self._id = self._document['_id']
                    self._query = {'_id': self._id}
                else:
                    self._id = None
                    self._query = None
            else:
                self._id = ObjectId(mid)
                self._query = {'_id': self._id}
                self._document['_id'] = self._id
            self.postInit()
            
    def getdocument(self):
        # ERREUR: ça ne copie pas _document, du coup del supprime l'id de _document
        # pourquoi on veut supprimer l'id ?
        #c = self._document
        #if '_id' in c:
        # del c['_id']
        #if 'id' in c:
        # del c['id']
        #return c
        return dict(self._document)
        
    def setdocument(self, value):
        c = self._document
        try:
            for name in value:
                c[name] = value[name]
        except:
            print('wrong document')
        self._document = c

    def deldocument(self):
        del self._document

    document = property(getdocument, setdocument, deldocument, 'I am the document property')
    
    def getmid(self):
        return str(self._id)
       
    def setmid(self, value):
        self._id = ObjectId(value)

    def delmid(self):
        del self._id

    mid = property(getmid, setmid, delmid, 'I am the mid property')
    
    def setParentFromField(self, parentClass, fieldName, fieldValue, create = True):
        """ Chromosome(Taxon, name, "Homo Sapiens hg 19", False 
        (le taxon doit exister sinon le chromosome ne sera pas rentré) """
        parentClassName = parentClass.__name__
        parentFieldName =  "%s_id" % parentClassName[:-1]
        parentObjects = parentClass(query = {fieldName : fieldValue})
        n = len(parentObjects.documents)
        if n == 0:
            if create:
                document={fieldName: fieldValue}
                parentObject = parentObjects.getChild(document = document)
                mid = parentObject.insert()
                self.setValue(parentFieldName, parentObject.mid)
                return True
            else:
                return False
                
        elif n == 1:
            parentObject = parentObjects.getChild(document = documents[0])
            self.setValue(parentFieldName, parentObject.mid)
            return True
            
        else: 
            return False

     
    def download(self, url, filename=None):
        c = urllib.request.urlretrieve(url, filename)
        #retour c au cas où il y aurait une levée d'exception
        return c
    
    def sequence_fasta(self):
        """recupère la sequence"""
        for seq_record in SeqIO.parse("nt_011630.fst", "fasta"):
            sequence = seq_record.seq
        return sequence
        
    def getParentClasses(self):
        """This method returns parent classes for the current object, 
        supposing any parent class object gives an attribute
         to this object ending by '_id' """
        parentClasses = []
        for key in self.document:
            if key[-3:] == '_id':
                parentClasses.append(key[:-3])
            return parentClasses

    
    def hasUniqueFieldForParent(self, field, parentClassName):
        """This method determines if other records in db has 
        the same field value and parentId, returns false if many, 
        returns true if one"""
        documents = []
        query = {}
        query[field] = self.getValue(field) #Oh no, not getValue again!
        parentField = parentClass+'_id'
        query[parentField] = self.getValue(parentField)
        cursor = self._connector['mongo'][self._db][self.collection].find(query)
        n = 0
        for document in cursor:
            n+=1
            if n > 1:
                return False
        if n is 1:
            return True
        else:
            return False
    
    def get_if_unique(self, field):
        """This method determines if other records in db has 
        the same field value and parentId, returns None if many, 
        returns the unique record if one"""
        #on crée un dictionnaire où field prend la valeur self....
        query = {field: self.getValue(field)} #Oh no, not getValue again!
        #résultats dans la base, de la recherche
        cursor = self._connector['mongo'][self._db][self.collection].find(query)
        #on obtient une liste à partir de cursor (itérateur à la base)
        documents = list(cursor)
        #si yen a qu'un seul, on le retourne, sinon retourne False(par défaut)
        if len(documents) == 1:
            return documents[0]

    def getfolder(self):
        return self._folder

    def setfolder(self, path):
        self._folder = path

    def delfolder(self):
        del self._folder

    folder = property(getfolder, setfolder, delfolder, 'I am the folder property')

    def extractSubKeys(self, key):
        r = re.compile(r'([_a-zA-Z0-9 ]+)\[([_a-zA-Z0-9 ]+)\]([\[_a-zA-Z0-9 \]]*)')
        m = re.search(r, key)
        if m:
            field = m.group(1)
            subfield = m.group(2)
            subsubfields = m.group(3)
            return field, subfield + subsubfields
        else:
            return key, None

    def extractValue(self, key, document):
        if key in document:
            return document[key]
        else:
            field, subkey = self.extractSubKeys(key)
            if field in document and subkey != None:
                subdocument = document[field]
                return self.extractValue(subkey, subdocument)
            else:
                return None

    def cleanValue(self, value):
        try:
            return int(value)
        except:
            return value

    def getValue(self, key, **params):
        if self.document == {}:
            self.fetch()
        value = self.extractValue(key, self.document)
        if value == None:
            field, subkey = self.extractSubKeys(key)
            if field is None or subkey is None:
                methodName = 'get' + key[0].upper() + key[1:]
            else:
                methodName = 'get' + field[0].upper() + field[1:]
            try:
                mtd = getattr(self, methodName)
                if params is None:
                    subdocument = mtd()
                else:
                    subdocument = mtd(**params)
                if type(subdocument) is dict and subkey is not None:
                    return self.extractValue(subkey, subdocument)
                else:
                    return self.cleanValue(subdocument)
            except:
                return None
        else:
            return self.cleanValue(value)

    def extractList(self, keys, **params):
        data = []
        for key in keys:
            data.append(self.getValue(key, **params))
        return data

    def extractSerie(self, keys, **params):
        cleankeys = []
        data = self.extractList(keys, **params)
        index = self.getCleanKeys(keys)
        return Series(data, index=index)

    def setValue(self, field, value):
        c = self._document
        c[field] = value
        self._document = c

    def addValue(self, field, value):
        if field in self._document.keys():
            self._document[field].append(value)
        else:
            self._document[field] = [value]

    def delValue(self, field):
        try:
            del self._document[field]
            return True
        except:
            return False

    def postInit(self):
        return self

    def fetch(self):
        if self._query is not None:
            document = self._connector['mongo'][self._db][self.collection].find_one(self._query)
            if document is not None:
                self._document = document

    def getForeignQuery(self):
        query = {}
        query[self.collection + '_id'] = ObjectId(self.mid)
        return query

    def update(self, data):
        self._connector['mongo'][self._db][self.collection].update(self._query, {'$set': data})
        for field in data:
            self.setValue(field, data[field])
        return self._id

    def update_raw(self, data):
        self._connector['mongo'][self._db][self.collection].update(self._query, data)
        for field in data:
            self.setValue(field, data[field])
        return self._id

    def remove(self):
        self._connector['mongo'][self._db][self.collection].remove(self._query)

    def insert(self):
        self._id = self._connector['mongo'][self._db][self.collection].insert(self.document)
        self._query = {'_id': self._id}
        return self._id

    def save(self):
        if self._query is None and self._id is not None:
            self._query = {'_id': self._id}
        if self._query is not None:
            return self.update(self.document)
        else:
            return self.insert()

    def process(self, cmd, shell=True, input=None):
        if input is not None:
            p = subprocess.Popen(cmd, shell=shell, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            o, e = p.communicate(input)
        else:
            p = subprocess.Popen(cmd, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            o, e = p.communicate()
        p.wait()
        return p.returncode, o.decode(), e.decode()

    def countRelated(self, collection, query):
        query[self.collection + '_id'] = self.mid
        c = list(self._connector['mongo'][self._db][collection].find(query))
        return len(c)

    def toDict(self):
        d = self.document
        d['id'] = self.mid
        return d

    def getGfs(self):
        return gridfs.GridFS(self._connector['mongo'][self._db], collection=self.collection + 'Files')

    def storeFile(self, stream, filename, content_type):
        gfs = self.getGfs()
        gfs.put(stream, filename=self.mid + '_' + filename, content_type=content_type)

    def getFile(self, filename):
        gfs = gridfs.GridFS(self._connector['mongo'][self._db], collection=self.collection + 'Files')
        return gfs.get_last_version(self.mid + '_' + filename)

    def deleteFile(self, filename):
        fileids = []
        gfs = gridfs.GridFS(self._connector['mongo'][self._db], collection=self.collection + 'Files')
        finder = self._connector['mongo'][self._db][self.collection + 'Files'].find({'filename': self.mid + '_' + filename})
        for document in finder:
            fileids.append(document['_id'])
        for _id in fileids:
            gfs.delete(_id)

    def getFolderNames(self, folderField='folders', titleField='title'):
        names = []
        for mid in self.getValue(folderField):
            f = MongoPersistentWebFolder(mid=mid)
            f.fetch()
            for dirname in f.getWebPaths():
                names.append(dirname + '/' + self.getValue(titleField))
        return names

    def getRandomData(self):
        data = {}
        data['labels'] = np.arange(20)
        data['key1'] = np.random.rand(20)
        data['key2'] = np.random.rand(20)
        data['key3'] = np.random.rand(20)
        return data

    colors = [
    'AliceBlue',
    'AntiqueWhite',
    'Aqua',
    'Aquamarine',
    'Azure',
    'Beige',
    'Bisque',
    'Black',
    'BlanchedAlmond',
    'Blue',
    'BlueViolet',
    'Brown',
    'BurlyWood',
    'CadetBlue',
    'Chartreuse',
    'Chocolate',
    'Coral',
    'CornflowerBlue',
    'Cornsilk',
    'Crimson',
    'Cyan',
    'DarkBlue',
    'DarkCyan',
    'DarkGoldenRod',
    'DarkGray',
    'DarkGreen',
    'DarkKhaki',
    'DarkMagenta',
    'DarkOliveGreen',
    'DarkOrange',
    'DarkOrchid',
    'DarkRed',
    'DarkSalmon',
    'DarkSeaGreen',
    'DarkSlateBlue',
    'DarkSlateGray',
    'DarkTurquoise',
    'DarkViolet',
    'DeepPink',
    'DeepSkyBlue',
    'DimGray',
    'DodgerBlue',
    'FireBrick',
    'FloralWhite',
    'ForestGreen',
    'Fuchsia',
    'Gainsboro',
    'GhostWhite',
    'Gold',
    'GoldenRod',
    'Gray',
    'Green',
    'GreenYellow',
    'HoneyDew',
    'HotPink',
    'IndianRed ',
    'Indigo ',
    'Ivory',
    'Khaki',
    'Lavender',
    'LavenderBlush',
    'LawnGreen',
    'LemonChiffon',
    'LightBlue',
    'LightCoral',
    'LightCyan',
    'LightGoldenRodYellow',
    'LightGray',
    'LightGreen',
    'LightPink',
    'LightSalmon',
    'LightSeaGreen',
    'LightSkyBlue',
    'LightSlateGray',
    'LightSteelBlue',
    'LightYellow',
    'Lime',
    'LimeGreen',
    'Linen',
    'Magenta',
    'Maroon',
    'MediumAquaMarine',
    'MediumBlue',
    'MediumOrchid',
    'MediumPurple',
    'MediumSeaGreen',
    'MediumSlateBlue',
    'MediumSpringGreen',
    'MediumTurquoise',
    'MediumVioletRed',
    'MidnightBlue',
    'MintCream',
    'MistyRose',
    'Moccasin',
    'NavajoWhite',
    'Navy',
    'OldLace',
    'Olive',
    'OliveDrab',
    'Orange',
    'OrangeRed',
    'Orchid',
    'PaleGoldenRod',
    'PaleGreen',
    'PaleTurquoise',
    'PaleVioletRed',
    'PapayaWhip',
    'PeachPuff',
    'Peru',
    'Pink',
    'Plum',
    'PowderBlue',
    'Purple',
    'Red',
    'RosyBrown',
    'RoyalBlue',
    'SaddleBrown',
    'Salmon',
    'SandyBrown',
    'SeaGreen',
    'SeaShell',
    'Sienna',
    'Silver',
    'SkyBlue',
    'SlateBlue',
    'SlateGray',
    'Snow',
    'SpringGreen',
    'SteelBlue',
    'Tan',
    'Teal',
    'Thistle',
    'Tomato',
    'Turquoise',
    'Violet',
    'Wheat',
    'White',
    'WhiteSmoke',
    'Yellow',
    'YellowGreen'
    ]
SVG_XML_HEADER = '<?xml version="1.0" encoding="utf-8" standalone="no"?>'
SVG_DOCTYPE_HEADER = '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"> <!-- Created with matplotlib (http://matplotlib.sourceforge.net/) -->'
    
class MongoPersistentWebObjects(object):
    
    def __init__(self, **kwargs):
        self._db = kwargs.pop('db', DB)
        self._connector = kwargs.pop('connector', CONNECTOR)
        self._documents = []
        documents = kwargs.pop('documents', None)
        query = kwargs.pop('query', None)
        mids = kwargs.pop('mids', None)
        sortby = kwargs.pop('sortby', None)
        if documents is not None:
            self._ids = None
            self._query = None
            self._documents = documents
            if sortby is not None:
                self._documents = sorted(documents, key=lambda k: k[sortby])
        if mids is not None:
            ids = []
            for mid in mids:
                ids.append(ObjectId(mid))
            query = {'_id': {'$in': ids}}
        if query is not None:
            self._query = query
            self.fetch(sortby=sortby)
            
    def getdocuments(self):
        cleancs = []
        d = {}
        for c in self._documents:
            for name in c.keys():
                if name is not '_id':
                    d[name] = c[name]
            cleancs.append(c)
        return cleancs
                
                
    def setdocuments(self, value):
        self._documents = value

    def deldocuments(self):
        del self._documents

    documents = property(getdocuments, setdocuments, deldocuments, 'I am the documents property')
    
    def getmids(self):
        mids = []
        for c in self._documents:
                mids.append(str(c.get('_id')))
        return mids

    def setmids(self, value):
        self._ids = ObjectId(value)

    def delmids(self):
        del self._ids

    mids = property(getmids, setmids, delmids, 'I am the mids property')
    
    def getsuffix(self):
        return self._suffix

    def setsuffix(self, suffix):
        self._suffix = suffix

    def delsuffix(self):
        del self._suffix

    suffix = property(getsuffix, setsuffix, delsuffix, 'I am the suffix property')

    def getfolder(self):
        return self._suffix

    def setfolder(self, suffix):
        self._suffix = suffix

    def delfolder(self):
        del self._suffix

    folder = property(getfolder, setfolder, delfolder, 'I am the suffix property')

    def getValues(self, field):
        ds = []
        for d in self._documents:
            if field in d:
                ds.append(d[field])
        return ds

    def setValues(self, data):
        ds = []
        for d in self._documents:
            for name in data:
                d[name] = data[name]
            ds.append(d)
        self._documents = ds

    def getChild(self, document):
        return MongoPersistentWebObject(document=document)

    def process(self, cmd, shell=True, input=None):
        if input is not None:
            p = subprocess.Popen(cmd, shell=shell, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            o, e = p.communicate(input)
        else:
            p = subprocess.Popen(cmd, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            o, e = p.communicate()
        p.wait()
        return p.returncode, o, e
    
    def download(self, url, filename=None):
        #utilise la fonction urlretrieve pour dl les fichiers à partir d'une url
        c = urllib.request.urlretrieve(url, filename)
        #retour c au cas où il y aurait une levée d'exception
        return c
    
    def sequence_fasta(self):
        """recupère la sequence"""
        for seq_record in SeqIO.parse("nt_011630.fst", "fasta"):
            sequence = seq_record.seq
        return sequence 
        
    def setParentFromField(self, parentClass, fieldName, fieldValue, create = True):
        """ Chromosome(Taxon, name, "Homo Sapiens hg 19", False 
        (le taxon doit exister sinon le chromosome ne sera pas rentré) """
        parentClassName = parentClass.__name__
        parentFieldName =  "%s_id" % parentClassName[:-1]
        parentObjects = parentClass(query = {fieldName : fieldValue})
        n = len(parentObjects.documents)
        if n == 0:
            if create:
                document={fieldName: fieldValue}
                parentObject = parentObjects.getChild(document = document)
                mid = parentObject.insert()
                self.setValues(parentFieldName, parentObject.mid)
                return True
            else:
                return False
                
        elif n == 1:
            parentObject = parentObjects.getChild(document = documents[0])
            self.setValues(parentFieldName, parentObject.mid)
            return True
            
        else: 
            return False
    
    def groupDocumentsByField(self, field):
        """This method return a dict. Its keys are the distinct values 
        of the given field for all documents. Each key is assigned 
        to a list of documents """
        result = {}
        for document in self.documents:
            v = document[field]
            if v not in result:
                result[v] = []
                result[v].append(document)
        return result
        
    def fetch(self, **kwargs):
        sortby = kwargs.pop('sortby', None)
        direction = kwargs.pop('direction', 1)
        self._documents = []
        cursor = self._connector['mongo'][self._db][self.collection].find(self._query)
        if sortby is None:
            for document in cursor:
                self._documents.append(document)
        else:
            for document in cursor.sort(sortby, direction):
                self._documents.append(document)

    def update(self, data):
        self._connector['mongo'][self._db][self.collection].update(self._query, {"$set": data})

    def save(self):
        for _document in self._documents:
            self._connector['mongo'][self._db][self.collection].save(_document)

    def remove(self):
        self._connector['mongo'][self._db][self.collection].remove(self._query)

    def insert(self):
        mids = []
        q, r = divmod(len(self._documents), 1000)
        for i in range(0, q):
            inserted_mids = list(self._connector['mongo'][self._db][self.collection].insert(self._documents[i * 1000:(i + 1) * 1000]))
            mids.extend(inserted_mids)
        inserted_mids = list(self._connector['mongo'][self._db][self.collection].insert(self._documents[q * 1000:]))
        mids.extend(inserted_mids)
        self._ids = mids
        self._query = {'$in': {'_id': self._ids}}
        return mids

    def toList(self):
        l = []
        for c in self._documents:
            c['id'] = str(c['_id'])
            del c['_id']
            l.append(c)
        return l

    def groupBy(self, groupby, key, sort, ignore):
        s = {'all': []}
        for document in self.documents:
            child = self.getChild(document)
            group = child.getValue(groupby)
            value = child.getValue(key)
            if ignore is None or group not in ignore:
                if group not in s:
                    s[group] = [value]
                else:
                    s[group].append(value)
                s['all'].append(value)
        if sort is None:
            return s
        else:
            t = {}
            for subkey in sort:
                t[subkey] = []
                for group in s:
                    if group in sort[subkey]:
                        t[subkey] += s[group]
            t['all'] = s['all']
            return t

    def groupCount(self, key, sort, ignore):
        s = {}
        for document in self.documents:
            child = self.getChild(document)
            value = child.getValue(key)
            if ignore is None or value not in ignore:
                if value not in s:
                    s[value] = 1
                else:
                    s[value] += 1
        if sort is None:
            return s
        else:
            t = {}
            for subkey in sort:
                t[subkey] = 0
                for value in s:
                    if value in sort[subkey]:
                        t[subkey] += s[value]
            return t

    def extractValue(self, key, document):
        child = self.getChild(document)
        return child.getValue(key)


    def extractValues(self, key):
        values = []
        for document in self.documents:
            values.append(self.extractValue(key, document))
        return values

    def extractDataFrame(self, keys, indexkey=None, **params):
        data = {}
        if indexkey == None:
            index = self.mids
        else:
            index = []
        for key in keys:
            data[key] = []
        for document in self.documents:
            child = self.getChild(document)
            if indexkey != None:
                index.append(child.getValue(indexkey, **params))
            for key in keys:
                data[key].append(child.getValue(key, **params))
        return DataFrame(data, columns=keys, index=index)

    def logResult(self, result):
        self.log.append(result)

    def getSubDocuments(self, eachDocument):
        if eachDocument:
            l = [[document] for document in self.documents]
        else:
            n = cpu_count()*2
            m = len(self.documents)
            r = m % n
            q = m // n
            l = [self.documents[i*q: (i+1)*q] for i in range(n)] + [self.documents[m-r:]]
        return l

    def getSubCollections(self, eachDocument):
        subCollections = []
        c = self.__class__
        for documents in self.getSubDocuments(eachDocument):
            subCollections.append(c(documents=documents))
        print(str(len(subCollections))+' processes will be created')
        return subCollections

    def getMultiThreadResult(self, methodName, args, eachDocument):
        results = []
        scs = self.getSubCollections(eachDocument)
        p = ThreadPool(processes= cpu_count())
        for c in scs:
            f = getattr(c, methodName)
            results.append(p.apply_async(f, args))
        return [result.get() for result in results]

    def getMultiThreadDataFrame(self, methodName, args, eachDocument=False):
        return concat(self.getMultiThreadResult(methodName, args, eachDocument))

    def extractSvg(self, output):
        h = re.compile(r'<svg (.*)>(.*)<\/svg>')
        m = re.search(h, output.replace('\n', '').replace('\t', ''))
        if m:
            return '<svg ' + m.group(1).decode('utf-8') + '>' + m.group(2).decode('utf-8') + '</svg>'
        else:
            return '<svg></svg>'

    def boxPlotsFromDataFrame(self, dataframe, title, xlabels, areas, start, stop):
        svgs = {}
        xsize = int(len(xlabels) / 10)
        m, n = dataframe.shape
        if start is None and stop is None:
            start = 0
            stop = n
        elif start is None:
            start = 0
        elif stop is None:
            stop = start + n
        fig, ax = plt.subplots()
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(7)
        fig.set_size_inches(xsize, 5)
        dataframe.boxplot(ax=ax)
        ax.set_title(title)
        ax.set_xticklabels(xlabels)
        for area in areas:
            plt.axvspan(area['start'], area['end'], color=area['shade'] + area['color'], alpha=0.5)
        self.rstyle(ax)
        plt.grid(True)
        output = io.StringIO()
        plt.savefig(output, transparent=True, format='svg')
        svg = self.extractSvg(output.getvalue())
        output.close()
        plt.close('all')
        return svg


from .mpwf import MongoPersistentWebFolder
