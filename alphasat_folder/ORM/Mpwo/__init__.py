# -*- coding: utf-8 -*-
from .mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from .mpwu import MongoPersistentWebUser as User
from .mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders
