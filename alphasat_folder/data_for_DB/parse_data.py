
def filtrer(src,dst):
	counter = 0
	for ligne in src:
		infos = ligne.rstrip('\n\r').split(';')
		if infos[0] == 'X10' and counter > 0 and counter <= 1000:
			print("ok")
			dst.write(ligne)
			counter += 1
		elif counter == 0:
			counter +=1

source = open("X10.species_chr_seq.dat", "r")
destination = open("out_X10_seq.csv","w")
try:
	filtrer(source,destination)
finally:
	destination.close()
	source.close()
