from Bio import SeqIO
def fasta(src,file,dst):
	counter = 1
	seq_liste = list()
	for ligne in src:
		infos = ligne.rstrip('\n\r').split(';')
		seq_liste.append(infos[2])
	#~ print(seq_liste)
	print(len(seq_liste))
	for line in file:
		infos = line.rstrip('\n\r').split(' ')
		name = infos[0].rstrip('\n\r').split('_')
		mono = name[0]+"_"+name[1]+"_"+name[2]+"_"+name[3]
		if mono in seq_liste:
			dst.write(line)
				
source = open("out_X10_seq.csv", "r")
source2 = open("X10.fa.regions.fst_np_L30.fst.rc.index", "r")

destination = open("out_X10_seq.index","w")
try:
	fasta(source,source2,destination)
finally:
	destination.close()
	source.close()
