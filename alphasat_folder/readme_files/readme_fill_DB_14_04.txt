"""Fichier readme pour le remplissage de la base de données"""
#Commande de suppression de la base de données complète
python3.3 createPGDB_09_04_stable.py -cleanAll Y
#Commande de suppression d'un seul taxon avec tous les éléments qui appartiennent à ce taxon
python3.3 createPGDB_09_04_stable.py -t nom_taxon -clean Y
#Commande de création d'une séquence de référence (indispensable avant de rentrer un taxon dans la BDD)
python3.3 createPGDB_09_04_stable.py -r fichier_reference
#Commande de création d'un taxon avec les séquences, les blocks et les monomers associés
python3.3 createPGDB_09_04_stable.py -t nom_taxon -i fichier_index -c fichier_sequence -m fichier_blocks -k N
#Commande de création de familles
python3.3 createPGDB_09_04_stable.py -t nom_taxon -f ../data_old/test_file_family.tsv
#Commande de création de supertaxons
python3.3 createPGDB_09_04_stable.py -T fichier_supertaxon
#Script qui compte les différents éléments de la BDD pour chaque taxon et remplit les attributs correspondants
python3.3 count_elements.py
