#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import io
import time 
import sys
import argparse
import re
import psycopg2
import psycopg2.extras
from parsing_fonctions import (memory_usage, absolue_location, insert_chromosome_chunk)
import subprocess

extended_help = """Several modes:

	MONOMER MODE:
	============
	
	BEWARE: in this mode, if you choose -clean Y you clean the entiere database

	For this mode, -t -c -m -k -clean options are required
	This mode can be associated with Family mode and BigFamily mode
	
	eg:
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -k N -clean Y
	to enter a taxon name : hg19, clean the data base but not entered the sequences associated
	
	FAMILY MODE:
	===========
	
	BEWARE : if clean option is choose in this mode, only the monomers' families
	dependences are removed
	
	For this mode, -t -f -clean options are required
	This mode can be associated with Monomer mode and BigFamily mode 
	
	eg:
	-t hg19 -f family_data.fst -clean N
	
	Here, you enter families data but you keep the informations 
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -clean Y
	
	If you want to clean the database you have to enter chromosome data 
	and family data
	
	BIGFAMILY MODE:
	==============
	
	BEWARE : if clean option is choose in this mode, the entiere family data
	are removed and the monomers' families dependances
	
	For this mode, -t -clean options are required
	This mode can be associated with Family mode and Monomer/Family mode
	
	eg:
	-t hg19 -F bigfamily_data.fst -clean N
	
	Here, you enter bigfamilies data but you keep the informations and the families data
	are updated 
	
	-t hg19 -f family_data.fst -F bigfamily_data.fst -clean N
	
	Here, you keep monomers' and chromosomes' informations and you add 
	families and bigfamilies informations. The monomers' informations are
	updated
	
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -F bigfamily_data.fst-clean Y
	
	Here, you create en entiere new base with all the informations
	
	CYTOBAND MODE:
	==============
	
	For this mode, -t -clean options are required
	This mode can be associated with Family mode and Monomer/Family/BigFamily mode
	
	Here, you keep monomers', chromosomes' and families' informations 
	and you add cytobands informations. The chromosomes' informations are
	updated
	
	e.g :
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -f family_data.fst -F bigfamily_data.fst -cy cytoband_data.fst -k N -clean Y
	
	Here, you create en entiere new base with all the informations
	
	-t hg19 -cy cytoband_data.fst -k N -clean Y
	
	Here, you clean the chromosomes' informations about the cytoband and update with new one
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -cy cytoband_data.fst -clean Y
	"""

parser = argparse.ArgumentParser(
epilog = extended_help, formatter_class = argparse.RawTextHelpFormatter)
parser.add_argument("-t", help="taxon name", type=str)
parser.add_argument("-T", help="path taxonparents name", type=str)
parser.add_argument("-r", help="path reference' file", type=str)
parser.add_argument("-i", help="path index' file", type=str)
parser.add_argument("-s", help="path sequences' file", type=str)
parser.add_argument("-b", help="path blocks' file",  type=str)
parser.add_argument("-f", help="path family file", type=str)
parser.add_argument("-F", help="path familyparents file", type=str)
parser.add_argument("-cy", help="path cytoband file", type=str)
parser.add_argument("-classif", help="family classification file", type=str)
parser.add_argument("-clean", help="the taxon will be totally cleaned",  action='store_true', default=False)
parser.add_argument("-cleanAll", help="the database will be totally cleaned", action='store_true', default=False)
parser.add_argument("-k", help="the sequence will be entered or not Y/N", type=str)
parser.add_argument("-v", help="verbose", type=str)
args = parser.parse_args()
"""Connexion a la base de donnees"""
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
	#conn = psycopg2.connect(database = args.db)
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
def clean_taxon(taxon):
	sequences_id = list()
	chros_id = list()
	blocks_id = list()
	monomers_id = list()
	chromosomes_id = list()
	if taxon:
		try:
			cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = %s;",(taxon,))
		except psycopg2.Error as e:
			print (e.pgerror)
		taxon_id = cursor.fetchone()
		if taxon_id:
			cursor.execute('commit')
			try:
				cursor.execute("SELECT sequence_id,chromosome_id FROM Sequence WHERE taxon_id = %s;",(taxon_id[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
			output_seq = cursor.fetchall()
			if output_seq:
				for i in output_seq:
					sequences_id.append(i['sequence_id'])
					chromosomes_id.append(i['chromosome_id'])
				cursor.execute('commit')
				try:
					cursor.execute("SELECT chromosome_id FROM Chromosome WHERE chromosome_id = ANY(%s);",(chromosomes_id,))
				except psycopg2.Error as e:
					print (e.pgerror)
				output_chr = cursor.fetchall()
				if output_chr:
					for i in output_chr:
						chros_id.append(i[0])
					cursor.execute('commit')
				try:
					cursor.execute("SELECT block_id FROM Block WHERE sequence_id = ANY(%s);",(sequences_id,))
				except psycopg2.Error as e:
					print (e.pgerror)
				output_block = cursor.fetchall()
				if output_block:
					for i in output_block:
						blocks_id.append(i[0])
					cursor.execute('commit')
					try:
						cursor.execute("SELECT monomer_id FROM Monomer WHERE sequence_id = ANY(%s);",(sequences_id,))
					except psycopg2.Error as e:
						print (e.pgerror)
					output_monomer = cursor.fetchall()
					if output_monomer:
						for i in output_monomer:
							monomers_id.append(i[0])		
						cursor.execute('commit')
						try:
							cursor.execute("DELETE FROM Monomer WHERE monomer_id = ANY(%s);",(monomers_id,))
						except psycopg2.Error as e:
							print (e.pgerror)
						cursor.execute('commit')
					try:
						cursor.execute("DELETE FROM Block WHERE block_id = ANY(%s);",(blocks_id,))
					except psycopg2.Error as e:
						print (e.pgerror)
					cursor.execute('commit')
				try:
					cursor.execute("DELETE FROM Sequence WHERE sequence_id = ANY(%s);",(sequences_id,))
				except psycopg2.Error as e:
					print (e.pgerror)
				cursor.execute('commit')
			try:
				cursor.execute("DELETE FROM Taxon WHERE taxon_id = %s;",(taxon_id[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
			cursor.execute('commit')
	else:
		print("This taxon doesn't exist")
		
def clean_bank():
	"""clean all the Collections - drop the database"""
	T1 = time.time()
	try:
		cursor.execute("SELECT tablename FROM pg_tables WHERE tablename !~ '^pg_' AND tablename !~ '^sql_';")
	except psycopg2.Error as e:
		print (e.pgerror)
	listeTables = list()
	for row in cursor:
		for element in row:
			listeTables.append(element)
			print (str(element))
	cursor.execute('commit')
	for i in listeTables:
		cursor.execute("DROP TABLE "+i+" CASCADE;")
	cursor.execute("""create table Taxon(
	taxon_id serial UNIQUE, 
	taxon_name varchar(100),
	number_sequences int,
	number_blocks int,
	number_monomers int,
	supertaxon_id int,
	primary key(taxon_id),
	foreign key(supertaxon_id) references Taxon(taxon_id) on delete CASCADE);""")
	cursor.execute('commit')
	cursor.execute("""create table Chromosome(
	chromosome_id serial UNIQUE,
	chromosome_name varchar(100),
	chromosome_length int,
	number_sequences int,
	number_blocks int,
	number_monomers int,
	primary key(chromosome_id));""")
	cursor.execute('commit')
	cursor.execute("""create table Sequence(
	sequence_id serial UNIQUE,
	sequence_name varchar(100),
	sequence_length int,
	sequence_nucleotide text,
	sequence_cytoband text[][],
	number_blocks int,
	number_monomers int,
	primary key(Sequence_id), 
	taxon_id int,
	chromosome_id int,
	foreign key(chromosome_id) references Chromosome(chromosome_id) on delete CASCADE,
	foreign key(taxon_id) references Taxon(taxon_id) on delete CASCADE );""")
	cursor.execute('commit')		
	cursor.execute("""create table Reference(
	reference_id serial UNIQUE,
	reference_name varchar(100),
	reference_nucleotide text,
	reference_description text,
	primary key(reference_id));""")
	cursor.execute('commit')
	cursor.execute("""create table Block(
	block_id serial unique,
	block_name varchar(100),
	sequence_id int,
	block_begin int, 
	block_end int, 
	block_strand int,
	block_length int,
	block_nucleotide text,
	block_index integer[],
	reference_id int,
	number_monomers int,
	primary key(block_id),
	foreign key(reference_id) references Reference(reference_id) on delete CASCADE,
	foreign key(sequence_id) references Sequence(sequence_id) on delete CASCADE);""")
	cursor.execute('commit')
	cursor.execute("""create table Monomer(
	monomer_id serial UNIQUE,
	block_id int,
	sequence_id int,
	monomer_name text,
	monomer_begin int, 
	monomer_end int,
	monomer_begin_sequence int, 
	monomer_end_sequence int,			
	monomer_strand int,
	monomer_length int,
	monomer_dimere text,
	monomer_index integer[],
	monomer_sequence text,
	primary key(monomer_id),
	foreign key(sequence_id) references Sequence(sequence_id) on delete CASCADE,
	foreign key(block_id) references Block(block_id) on delete CASCADE);""")
	cursor.execute('commit')
	cursor.execute("""create table Family(
	family_id serial UNIQUE,
	family_name varchar(100),
	family_classification varchar(100),
	superfamily_id int,
	primary key(family_id),
	foreign key(superfamily_id) references Family(family_id) on delete CASCADE);""")
	cursor.execute('commit')
	cursor.execute("""create table Belongs(
	monomer_id int,
	foreign key(monomer_id) references Monomer(monomer_id) on delete CASCADE,
	family_id int,
	foreign key(family_id) references Family(family_id) on delete CASCADE,
	primary key(monomer_id,family_id));""")
	cursor.execute('commit')
	"""Appel de la fonction create_index"""
	create_index()
	conn.commit()
	FT1 = time.time()
	print("Temps de la fonction clean_bank() :  %f" %(FT1 - T1))

def insert_taxon(taxon_name):
	"""insert the taxon"""
	T1 = time.time()
	#MV : Fichiers de taxons?
	try:
		cursor.execute("select taxon_name FROM Taxon WHERE taxon_name = (%s) ;", (taxon_name,))
	except psycopg2.Error as e:
			print (e.pgerror)	
	monCursor = cursor.fetchone()
	cursor.execute('commit')	
	if monCursor:
		print("Error, this taxon is already in the database")
		sys.exit(0)
	try:
		cursor.execute("insert into Taxon (taxon_name) values (%s) ;", (taxon_name,))
		cursor.execute('commit')
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute("select taxon_id from Taxon where taxon_name = %s ;", (taxon_name,))
	for row in cursor:
		for i in row:
			taxon_id = i
	cursor.execute('commit')
	conn.commit()
	FT1 = time.time()
	print("Temps de la fonction insert_taxon() :  %f" %(FT1 - T1))
	return (taxon_id)

def clean_families(families):
	"""clean only family Collection"""
	if families:
		families = families.lower()
		if families == 'y':
			try:
				cursor.execute("DELETE FROM Family;")
			except psycopg2.Error as e:
				print (e.pgerror)
			cursor.execute('commit')
			#~ cursor.execute("""create table Family(
			#~ family_id serial UNIQUE,
			#~ family_name varchar(30),
			#~ sequence_id int,
			#~ superfamily_id int,
			#~ primary key(family_id),
			#~ foreign key(sequence_id) references Sequence(sequence_id) on delete CASCADE,
			#~ foreign key(superfamily_id) references Family(family_id) on delete CASCADE);""")
			#~ cursor.execute('commit')
	conn.commit()
	
#~ def clean_monomer_dependency(dependency):
	#~ #à vérifier pour family ou families ? 
	#~ """clean monomers dependency to families"""
	#~ dependency = dependency.lower() 
	#~ if dependency == 'y':
		#~ M_MONGO.update({}, {"$unset" : {'families' : 1} }, upsert=True,multi=True)
		
def clean_cytoband_dependency(dependency):
	""" clean the cytobands informations on the chromosomes"""
	dependency = dependency.lower()
	if dependency == 'y':
		C_MONGO.update({}, { "$unset" :{'centromer' : 1} }, upsert=True, 
		multi=True)
		C_MONGO.update({}, { "$unset" : {'cytoband' : 1} }, upsert=True, 
		multi=True)


def insert_chromosome(sequence_chr_list, taxon_identifiant,description):
	""" insert chromsomome without their sequences.
	WARNING : the file enter cannot contain the sequence, 
	only chromosome name"""
	T1 = time.time()
	buff = io.StringIO()
	liste1 = list()
	liste2 = list()
	taxon_id = str(taxon_identifiant)
	try:
		cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(taxon_identifiant,))
	except psycopg2.Error as e:
		print (e.pgerror)
	taxon_name = cursor.fetchone()[0]
	for seq in sequence_chr_list:
		chromosome_name = seq[0]+"|"+taxon_name
		try:
			cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_name = %s;",(chromosome_name,))
		except psycopg2.Error as e:
			print (e.pgerror)
		chr_name = cursor.fetchone()
		cursor.execute('commit')		
		if not chr_name:
			try:
				cursor.execute("INSERT INTO Chromosome (chromosome_name) VALUES (%s);",(chromosome_name,))
			except psycopg2.Error as e:
				print (e.pgerror)
			cursor.execute('commit')
		try:
			cursor.execute("SELECT chromosome_id FROM Chromosome WHERE chromosome_name = %s;",(chromosome_name,))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			chr_id = i['chromosome_id']
		cursor.execute('commit')	
		liste2.extend([seq[1],str(chr_id)])
		liste2.append(taxon_id)
		liste1.extend(liste2)
		#print(liste1)
		liste2 = []
		buff.write("\t".join(liste1)+"\n")
		liste1 = []
	buff.seek(0)
	try:
		cursor.copy_from(buff,'Sequence',columns=('sequence_name','chromosome_id','taxon_id'))
		cursor.execute('commit')
	except psycopg2.Error as e:
		print (e.pgerror)
	conn.commit()
	FT1 = time.time()
	print("Temps de la fonction insert_chromosome() :  %f" %(FT1 - T1))



def gen_monomers_names(m_file):
	"""use by the generator, to get the monomers names"""
	with open(m_file) as monomer_file:
		for line in monomer_file:
			if not line.startswith('>'):
				continue
			yield line[1:-1]

def gen_monomers_dict_sequence(m_file):
	"""get a dict monomers name : sequence"""
	T1 = time.time()
	monomers = dict()
	for seq_record in SeqIO.parse(m_file, "fasta"):
		"""Dictionnaire nom_monomere : sequence_monomere"""
		monomers[seq_record.id] = str(seq_record.seq)
	FT1 = time.time()
	print("Temps de la fonction gen_monomers_dict_sequence() :  %f" %(FT1 - T1))
	return monomers
	

def gen_monomers_docs(m_file, seq_name_id):# ,i_file):
	""" generate the monomers docs for the generator"""
	T1 = time.time()
	t = io.StringIO()
	t2 = io.StringIO()
	liste1 = []
	"""Dictionnaire monomeres noms : sequences
	Appel de la fonction gen_monomers_dict_sequence"""
	block_sequence = gen_monomers_dict_sequence(m_file)
	for name in gen_monomers_names(m_file):
		parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1)$", name)
		if not parse:
			print ("Warning, invalid monomer %s " % name)
			continue
		"""Attribution des variables a leurs valeurs provenant du nom du monomere et separe par des "_" et recupere avec parse.groups()"""
		(sequence_name, block_begin, block_end, strand) = parse.groups()
		print(sequence_name, block_begin, block_end, strand)
		"""Obtention des positions sur le chromosome des monomeres a l'aide d'une fonction absolue_location()"""
		#(mono_begin, mono_end) = absolue_location(int(monomer_begin),int(monomer_end), int(block_begin), int(block_end), int(strand))
		"""Nom du block"""
		#block_name = '_'.join([chromosome_name, block_begin, block_end, strand])
		Sequence_id = seq_name_id[sequence_name]
		#sequence = block_sequence[name]
		#block_length = len(sequence)
		"""block_list[block_name] -> recuperation de block_id pour un nom de block donne
		block_sequence[name] -> recuperation de la sequence pour un nom de monomere donne"""
		liste1.extend([name,str(Sequence_id),block_begin,block_end,strand,block_sequence[name],str(len(block_sequence[name]))])
		#print(liste1)
		t.write("\t".join(liste1)+"\n")
		liste1 = []
	t.seek(0)
	#with open(i_file) as index_file:
		#for line in index_file:
			##print(line)
			#newline = line.split(" ")
			##print(newline)
			#block_name = newline[0]
			##print("nom")
			##print(block_name)
			#reference = newline[1]
			#index = newline[2]
			#for i in t:
				##bb +=1
				#bl_name = i.rstrip("\n").split("\t")[0]
				##print("buffer")
				##print(bl_name)
				#block_line = i.rstrip("\n")
				#if bl_name == block_name:
					#t2.write(block_line+"\t"+reference+"\t"+index+"\n")
			#t.seek(0)
	##print(t2.getvalue())
	#t2.seek(0)
	FT1 = time.time()
	print("Temps de la fonction gen_monomers_docs() :  %f" %(FT1 - T1))
	return(t)

def block_index_docs(i_file):
	ID1 = time.time()
	ID2 = time.clock()
	buffer_file = io.StringIO()
	#tmp = io.StringIO()	
	#liste_tmp = list()
	with open(i_file) as index_file:
		for line in index_file:
			newline = line.split(" ")
			block_name = newline[0]
			reference = newline[1]
			index = newline[2]
			index_array = index.rstrip("\n").lstrip(",").split(",")
			#print(index_array)
			index_array = list(map(int,index_array))
			#print(index_array)
			try:
				cursor.execute("SELECT reference_id FROM Reference WHERE reference_name = %s ;",(reference,))
			except psycopg2.Error as e:
				print (e.pgerror)
			reference_id = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("UPDATE Block SET block_index = %s, reference_id = %s WHERE block_name = %s ;",(index_array,reference_id[0],block_name,))
			except psycopg2.Error as e:
				print (e.pgerror)
			cursor.execute('commit')
			try:
				cursor.execute("SELECT block_name,block_id,sequence_id,block_nucleotide,block_index,block_strand,block_begin,block_end FROM Block WHERE block_name = %s ;",(block_name,))
			except psycopg2.Error as e:
				print (e.pgerror)
			block_info = cursor.fetchone()
			block_name,block_id,sequence_id,block_sequence,block_index,block_strand,block_begin,block_end = block_info
			cursor.execute('commit')
			#~ try:
				#~ cursor.execute("SELECT reference_nucleotide FROM Reference WHERE reference_id = %s ;",(reference_id[0],))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ for i in cursor:
				#~ reference = i
			#~ print(str(len(reference)))
			compteur_mono = False
			beginP = 0
			beginO = 0
			position = 0
			endP = None
			endO = None
			previous = -3
			#~ end_mono = 0
			#~ for j in block_index:
			#~ if j > end_mono:
				#~ end_mono = j
			"""Parcours de l'index"""
			for j in block_index:
				"""Condition fin d'un monomere :
				j inferieur au numero precedente
				j strictement superieur a 0
				position superieure a 0"""
				#if j < block_index[position - 1] and j > 0 and position != 0:
				if j < previous and j > 0:
					"""Condition fin d'un duo de monomeres"""
					if compteur_mono:
						endP = position - 1
						monomer_seq = block_sequence[beginO:endP+1]
						monomer = block_index[beginO:endP+1]
						monomer_unique = block_sequence[beginO:endO+1]
						monoName = "_".join((block_name,str(beginO),str(endO)))
						length = ((endO + 1) - (beginO + 1)) + 1
						monomer_begin = beginO + 1
						monomer_end = endO + 1				
						(monomer_begin_sequence,monomer_end_sequence) = absolue_location(monomer_begin,monomer_end,block_begin,block_end,block_strand)
						buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginO + 1),str(endO + 1),str(block_strand),str(length),str(monomer_begin_sequence),str(monomer_end_sequence)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\t"+monomer_unique+"\n")
						beginO = beginP
						beginP = position
						endO = endP
						"""Condition fin d'un monomere mais pas d'un duo de monomere"""
					else:
						compteur_mono = True
						endO = position - 1
						beginO = beginP
						beginP = position
				if position < (len(block_index)-1):
					position += 1
				if j > 0:
					previous = j
			"""Fin de la boucle"""
			if endO is None:
				endO = len(block_index) - 1
			monomer_seq = block_sequence[beginO:]
			monomer = block_index[beginO:]
			monomer_unique = block_sequence[beginO:endO+1]
			monoName = "_".join((block_name,str(beginO),str(endO)))
			length = ((endO + 1) - (beginO + 1)) + 1
			monomer_begin = beginO + 1
			monomer_end = endO + 1
			(monomer_begin_sequence,monomer_end_sequence) = absolue_location(monomer_begin,monomer_end,block_begin,block_end,block_strand)
			buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginO + 1),str(endO + 1),str(block_strand),str(length),str(monomer_begin_sequence),str(monomer_end_sequence)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\t"+monomer_unique+"\n")
			if beginO != beginP:
				monomer_seq = block_sequence[beginP:]
				monomer = block_index[beginP:]
				monomer_unique = block_sequence[beginO:endO+1]
				monoName = "_".join((block_name,str(beginP),str(len(block_index))))
				length = (len(block_index) - (beginP + 1)) + 1
				monomer_begin = beginP + 1
				monomer_end = len(block_index)
				(monomer_begin_sequence,monomer_end_sequence) = absolue_location(monomer_begin,monomer_end,block_begin,block_end,block_strand)
				buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginP + 1),str(len(block_index)),str(block_strand),str(length),str(monomer_begin_sequence),str(monomer_end_sequence)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\t"+monomer_unique+"\n")
	buffer_file.seek(0)
	#print(buffer_file.getvalue())
	try:
		cursor.copy_expert("COPY Monomer(monomer_name,block_id,sequence_id,monomer_begin,monomer_end,monomer_strand,monomer_length,monomer_begin_sequence,monomer_end_sequence,monomer_index,monomer_dimere,monomer_sequence) FROM STDIN;",buffer_file)
	except psycopg2.Error as e:
		print (e.pgerror)
	#print(buffer_file.getvalue())
	#print(str(index_array))
	conn.commit()
	FID1 = time.time()
	FID2 = time.clock()
	print("Temps total de la fonction block_index_docs() :  %f" %(FID1 - ID1))
	print("Temps CPU de la fonction block_index_docs() :  %f" %(FID2 - ID2))
			#liste_tmp.extend([block_name,reference,index])
			#tmp.write("\t".join(liste_tmp)+"\n")
			#liste_tmp = []
	#tmp.seek(0)
	#return(tmp)
		
#def gen_infos(m_file,chr_name_id):
	#"""with the monomers names, get all the informations 
	#about the monomers : begin, end, strand, block_begin, block_end, 
	#chromosome"""
	#T1 = time.time()
	#t2 = io.StringIO()
	#liste1 = []
	#liste2 = list()
	#for name in gen_monomers_names(m_file):
		#parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
		#if not parse:
			#print ("Warning, invalid monomer %s " % name)
			#continue
		##Idee faire un dico de dico
		##info = dict(zip(('chromosome', 'block_begin', 'block_end', 'strand', 'monomer_begin', 'monomer_end'), parse.groups()))
		#(chromosome, block_begin, block_end, strand, monomer_begin, monomer_end) = parse.groups()
		#"""Calcule la position genomique de debut et de fin d'un monomere"""
		#(monomer_begin, monomer_end) = absolue_location(int(monomer_begin),int(monomer_end), int(block_begin),int(block_end),int(strand))
		#Chromosome_id = chr_name_id[chromosome]
		#block_name = '_'.join([chromosome, block_begin, block_end, strand])
		#if block_name not in liste2:
			#liste2.append(block_name)
			#liste1.extend([str(block_begin),str(block_end),str(strand),str(block_name),str(Chromosome_id)])
			#t2.write("\t".join(liste1)+"\n")
			#liste1 = []
	#t2.seek(0)
	#FT1 = time.time()
	#print("Temps de la fonction gen_infos() :  %f" %(FT1 - T1))
	#return(t2)

def create_data(m_file, taxon_id, i_file):
	"""insert blocks and monomers informations with bulk insert"""
	T1 = time.time()
	seq_name_id = dict()
	cursor.execute("select sequence_name, sequence_id from Sequence where taxon_id = %s ;", (taxon_id,))
	for row in cursor:
		seq_name_id[row[0]] = row[1]
	cursor.execute('commit')
	print(memory_usage())
	Tc = time.time()
	try:
		"""Appel de la fonction gen_monomers_docs"""
		cursor.copy_from(gen_monomers_docs(m_file,seq_name_id),'Block',sep='\t',columns=('block_name','sequence_id','block_begin','block_end','block_strand','block_nucleotide','block_length',))#'reference_id','block_index',))
	except psycopg2.Error as e:
		print (e.pgerror)
	conn.commit()
	block_index_docs(i_file)
	Td = time.time()
	print("Temps de la requete monomer :  %f" %(Td - Tc))
	FT1 = time.time()	  
	print("Temps de la fonction create_data() :  %f" %(FT1 - T1))

def create_index():
	try:
		cursor.execute("CREATE INDEX block_id_idx ON Monomer(block_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX sequence_id_idx ON Block(sequence_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX taxon_id_idx ON Sequence(taxon_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX reference_id_idx ON Block(reference_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX reference_name_idx ON Reference(reference_name);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX block_name_idx ON Block(block_name);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	conn.commit()

def update_cytoband(cyto,tx):
	"""add the cytobands informations(name, end, begin, giemsa stain)
	 on the chromosomes documents"""
	try:
		cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = %s;",(tx,))
	except psycopg2.Error as e:
		print (e.pgerror)
	tx_id = cursor.fetchone()[0]
	if not tx_id:
		print("Error, this taxon is already in the database")
		sys.exit(0)
	caryotype_list = list()
	with open(cyto, "r") as cyto_file:
		for line in cyto_file:
			infos = line.rstrip("\n").split("\t")
			caryotype_list.append(infos)
			try:
				cursor.execute("SELECT sequence_name FROM Sequence WHERE taxon_id = %s AND sequence_name = %s;",(tx_id,infos[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_name = cursor.fetchone()
			cursor.execute('commit')
			if not sequence_name:
				continue
			else:
				parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", infos[0])
				if parse:
					try:
						cursor.execute("UPDATE Sequence SET sequence_cytoband = sequence_cytoband || ARRAY[%s] WHERE taxon_id = %s AND sequence_name = %s;",(infos,tx_id,infos[0],))
					except psycopg2.Error as e:
						print (e.pgerror)

def monomer_mode(c_file, m_file, k_option, i_file, taxon_id):
	"""'mode' activate to enter blocks, monomers, chromosomes and taxons
	informations"""
	T1 = time.time()
	sequence_name = list()
	chr_name = list()
	chr_description = dict()
	liste_sequence = list()
	infos_sequence = list()
	if k_option:
		k_option = k_option.lower()
		if k_option == 'y':
			for seq_record in SeqIO.parse(c_file, "fasta"):
				chromosomeid[seq_record.id] = insert_chromosome_chunk(str(seq_record.id), taxon_id, str(seq_record.description), str(seq_record.seq).upper())
				"""Appel de la fonction create_data"""
				create_data(m_file, taxon_id, i_file)
	else : 
		with open(c_file, "r") as read_file:
			for line in read_file:
				new_line = line.rstrip("\n").split(";")
				#new_line[-1] = new_line[-1].replace("\n", "")
				#~ if " " in new_line:
					#~ (seqname,chrname, chr_descr) = new_line.split(" ", 1)
					#~ liste_sequence.extend(seqname,chrname)
					#~ infos_sequence.append(liste_sequence)
					#~ liste_sequence = []
					#~ #sequence_name.append(seqname)
					#~ #chr_name.append(chrname)
					#~ chr_description[chrname] = chr_descr
				if not new_line[1]:
					#Construire une liste de liste
					chr_name = "unknown"
					liste_sequence.extend([chr_name,new_line[2]])
					infos_sequence.append(liste_sequence)
					liste_sequence = []
					#Construction d'un dictionnaire de description sans infos de description
					chr_description[new_line[2]] = new_line
				if new_line[1] == 'chrUn':
					chr_name = "unknown"
					#Construire une liste de liste
					liste_sequence.extend([chr_name,new_line[2]])
					infos_sequence.append(liste_sequence)
					liste_sequence = []
					#Construction d'un dictionnaire de description sans infos de description
					chr_description[new_line[2]] = new_line					
				else:
					#Construire une liste de liste
					liste_sequence.extend([new_line[1],new_line[2]])
					infos_sequence.append(liste_sequence)
					liste_sequence = []
					#chr_name.append(new_line[1])
					#sequence_name.append(new_line[2])
					#Construction d'un dictionnaire de description sans infos de description
					chr_description[new_line[2]] = new_line
	#print(infos_sequence)
	"""Appel de la fonction insert_chromosome"""
	insert_chromosome(infos_sequence, taxon_id, chr_description)
	"""Appel de la fonction create_data"""
	create_data(m_file, taxon_id, i_file)
	FT1 = time.time()
	print("Temps de la fonction monomer_mode() :  %f" %(FT1 - T1))

def reference_mode(r_file):
	for seq_record in SeqIO.parse(r_file, "fasta"):
		infos = str(seq_record.description)
		seq = str(seq_record.seq)
		infos = infos.split(" ")
		reference_name = infos[0]
		#print(reference_name)
		reference_comments = " ".join(infos[1:])
		try:
			cursor.execute("select reference_name FROM Reference WHERE reference_name = (%s) ;", (reference_name,))
			monCursor = cursor.fetchall()
			cursor.execute('commit')
		except psycopg2.Error as e:
				print (e.pgerror)	
		if monCursor:
			print("Error, this reference (%s) is already in the database" %(reference_name))
			sys.exit(0)
		try:
			cursor.execute("insert into Reference (reference_name,reference_description,reference_nucleotide) values (%s,%s,%s) ;", (reference_name,reference_comments,seq,))
			cursor.execute('commit')
		except psycopg2.Error as e:
			print (e.pgerror)
	conn.commit()

def family_infos(taxon,familyfile,classification):
	"""create informations about families file for the generator"""
	try:
		cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = %s;",(taxon,))
	except psycopg2.Error as e:
		print (e.pgerror)
	tx_id = cursor.fetchone()[0]
	print(classification)
	with open (familyfile) as family_file:
		for name in family_file:
			infos_list = list()
			name = name.replace("\n", "")
			parse = re.match(r"^(.*)( (\w+))+$", name)
			if not parse:
				print ("Warning, invalid format %s " % name)
				continue
			else:
				infos_list = [ i for i in name.rstrip("\n").split(" ") ]
				if len(infos_list) >= 2:
					try:
						cursor.execute("SELECT family_id FROM Family WHERE family_name = %s;",(infos_list[1],))
					except psycopg2.Error as e:
						print (e.pgerror)
					family_id = cursor.fetchone()
					if not family_id:											
						try:
							cursor.execute("INSERT INTO Family (family_name,family_classification) VALUES (%s,%s);",(infos_list[1],classification,))
							cursor.execute('commit')
						except psycopg2.Error as e:
							print (e.pgerror)
					try:
						cursor.execute("SELECT family_id FROM Family WHERE family_name = %s AND family_classification = %s;",(infos_list[1],classification,))
					except psycopg2.Error as e:
						print (e.pgerror)
					family_id = cursor.fetchone()[0]
					#parse_block = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1)$", infos_list[0])
					
					(sequence_name,block_begin,block_end,strand,monomer_begin) = infos_list[0].rstrip("\n").split("_")
					#~ print(sequence_name,block_begin,block_end,strand,monomer_begin)
					try:
						cursor.execute("SELECT sequence_id FROM Sequence WHERE sequence_name = %s AND taxon_id = %s;",(sequence_name,tx_id,))
					except psycopg2.Error as e:
						print (e.pgerror)
					sequence_id = cursor.fetchone()
					#~ print("sequence_id")
					#~ print(sequence_id)
					if sequence_id:
						try:
							cursor.execute("SELECT block_id FROM Block WHERE block_begin = %s AND block_end = %s and sequence_id = %s;",(block_begin,block_end,sequence_id[0],))
						except psycopg2.Error as e:
							print (e.pgerror)
						block_id = cursor.fetchone()
						monomer_begin = int(monomer_begin) + 1
						#~ print("block_id")
						#~ print(block_id)
						if block_id:
							try:
								cursor.execute("SELECT monomer_id FROM Monomer WHERE sequence_id = %s AND block_id = %s AND monomer_begin = %s;",(sequence_id[0],block_id[0],monomer_begin,))
							except psycopg2.Error as e:
								print (e.pgerror)
							monomer_id = cursor.fetchone()
							#~ print("monomer_id")
							#~ print(monomer_id)
							if monomer_id:
								#~ print("coucou")
								try:
									cursor.execute("INSERT INTO Belongs (family_id,monomer_id) VALUES (%s,%s);",(family_id,monomer_id[0],))
								except psycopg2.Error as e:
									print (e.pgerror)
							else:
								print("This monomer %s doesn't exist in the database" % infos_list[0])
						else:
							print("Warning, invalid format %s" % infos_list[0])						
	#~ cur.execute("select * from Belongs;")
	#~ belongs = cur.fetchall()
	#~ print(belongs)
#~ def create_family(familyfile):
	#~ """generator of families informations"""
	#~ for family in family_infos(familyfile):
		#~ yield  {
						#~ 'name' : family['family_name'],
				#~ }


def update_superfamilies(familiesfile,classification):
	"""add informations about superfamilies to the families"""
	with open(familiesfile, "r") as family_file:
		for line in family_file:
			infos_list = list()
			line = line.replace("\n", "")
			parse = re.match(r"^(.*)(	(\w+))+$", line)
			if not parse:
				print ("Warning, invalid format %s " % line)
				continue
			else:
				infos_list = [ i for i in line.rstrip("\n").split("\t") ]
				if len(infos_list) >= 2:
					try:
						cursor.execute("SELECT family_id FROM Family WHERE family_name = %s;",(infos_list[0],))
					except psycopg2.Error as e:
						print (e.pgerror)
					superfamily_id = cursor.fetchone()
					print(superfamily_id)
					if not superfamily_id:
						try:
							cursor.execute("INSERT INTO Family (family_name,family_classification) VALUES (%s,%s);",(infos_list[0],classification,))
						except psycopg2.Error as e:
							print (e.pgerror)
					try:
						cursor.execute("SELECT family_id FROM Family WHERE family_name = %s;",(infos_list[1],))
					except psycopg2.Error as e:
						print (e.pgerror)
					family_id = cursor.fetchone()
					if not family_id:	
						try:
							cursor.execute("SELECT family_id FROM Family WHERE family_name = %s;",(infos_list[0],))
						except psycopg2.Error as e:
							print (e.pgerror)
						superfamily_id = cursor.fetchone()
						try:
							cursor.execute("INSERT INTO Family (family_name,superfamily_id,family_classification) VALUES (%s,%s,%s);",(infos_list[1],superfamily_id[0],classification,))
							cursor.execute('commit')
						except psycopg2.Error as e:
							print (e.pgerror)
				else:
					print("Warning, invalid format ")


def families_mode(taxon,families,classification):
	"""add families informations to monomers, create families Collection
	and create an array of chromosome_ids in families Collection"""
	family_infos(taxon,families,classification)
	#~ F_MONGO.insert(create_family(families), continue_on_error=True)
	#~ dic_mono_family = {d['monomer_name']:d for d in family_infos(families)}
	#~ for name in dic_mono_family:
		#~ M_MONGO.update({'name' : dic_mono_family[name]['monomer_name']}, 
		#~ {"$push" : {'families' : { 'name' : 
		#~ dic_mono_family[name]['family_name']}}}, 
		#~ continue_on_error=True, upsert=True, multi=True)
	#~ 
	#~ for name in dic_mono_family:
		#~ dic_mono_family[name]['Chromosome_id'] = list(M_MONGO.find(
		#~ {'name' : name}))[0]['Chromosome_id']
	#~ for name in dic_mono_family:
		#~ F_MONGO.update({'name' : dic_mono_family[name]['family_name']}, 
		#~ {"$addToSet" :{'Chromosome_ids' :{ 'Chromosome_id':
		#~ dic_mono_family[name]['Chromosome_id'] }}},multi=True)
		
def bigfamilies_mode(superfamilies,classification):
	"""mode to add informations about superfamilies"""
	update_superfamilies(superfamilies,classification)

#à finir + à tester
def cytoband_mode(cyto,taxon):
	"""mode to add informations about cytoband"""
	update_cytoband(cyto,taxon)

def supertaxon_mode(supertaxon_file):
	try:
		cursor.execute("UPDATE Taxon SET supertaxon_id = NULL;")
		cursor.execute('commit')
	except psycopg2.Error as e:
		print (e.pgerror)	
	with open(supertaxon_file, "r") as taxon_file:
		for line in taxon_file:
			number_sequences = 0
			number_blocks = 0
			number_monomers = 0
			infos = line.rstrip("\n").split("\t")
			try:
				cursor.execute("select taxon_name FROM Taxon WHERE taxon_name = (%s) ;", (infos[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')			
			if taxon_name:
				print("The taxon  %s is already in the database" %taxon_name[0])
			else:
				try:
					cursor.execute("Insert into Taxon (taxon_name) values (%s) ;", (infos[0],))
					cursor.execute('commit')
				except psycopg2.Error as e:
					print (e.pgerror)
			try:
				cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = (%s) ;", (infos[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
			supertaxon_id = cursor.fetchone()
			cursor.execute('commit')
			supertaxon_id = supertaxon_id['taxon_id']
			print(infos[1])
			print(supertaxon_id)
			try:
				cursor.execute("UPDATE Taxon SET supertaxon_id = %s WHERE taxon_name = %s;", (supertaxon_id, infos[1],))
				cursor.execute('commit')
			except psycopg2.Error as e:
				print (e.pgerror)
			try:
				cursor.execute("SELECT taxon_id from Taxon WHERE supertaxon_id = %s;", (supertaxon_id,))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxons = cursor.fetchall()
			for i in taxons:
				print(i)
				try:
					cursor.execute("SELECT number_sequences,number_blocks,number_monomers from Taxon WHERE taxon_id = %s;", (i['taxon_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				numbers = cursor.fetchone()
				if numbers[0]:
					number_sequences += int(numbers['number_sequences'])
					number_blocks += int(numbers['number_blocks'])				
					number_monomers += int(numbers['number_monomers'])		
					try:
						cursor.execute("UPDATE Taxon SET number_sequences = %s,number_blocks = %s,number_monomers = %s WHERE taxon_id = %s;", (number_sequences,number_blocks,number_monomers,supertaxon_id,))
					except psycopg2.Error as e:
						print (e.pgerror)
			
if args.t and args.i and args.s and args.b and args.F is None and args.f is None and args.cy is None \
and args.cleanAll is False:
	print("Monomer Mode")
	print(memory_usage())
	T1 = time.time()
	FT1 = time.time()
	print("Temps de l'appel de la fonction clean_banque() : %f" %(FT1 - T1))
	T2 = time.time() 
	"""Appel de la fonction insert_taxon"""
	TAXON_ID = insert_taxon(args.t)
	TF2 = time.time()
	print("Temps de l'appel de la fonction insert_taxon() : %f" %(TF2 - T2))	
	print("New Taxon created")
	T3 = time.time()
	"""Appel de la fonction monomer_mode"""
	monomer_mode(args.s, args.b, args.k, args.i, TAXON_ID)
	TF3 = time.time()
	print("Temps de l'appel de la fonction monomer_mode() : %f" %(TF3 - T3))
	cursor.close()
	conn.close()
	TPS1 = time.time()
	TPS2 = time.time() 
	print("temps s %f" %(TPS2 - TPS1))
	TPC1 = time.clock()
	TPC2 = time.clock()
	print("temps CPU final %f" %(TPC2 - TPC1))
	sys.exit(0)
	
if args.r and args.s is None and args.b is None and args.F is None \
	and args.cy is None and args.k is None and args.cleanAll is False:
	print("Reference Mode")
	F1 = time.time()
	F2 = time.time()
	print("Temps de l'appel de la fonction clean_banque() : %f" %(F2 - F1))
	T1 = time.time()
	reference_mode(args.r)
	cursor.close()
	conn.close()
	T2 = time.time()
	print("temps s %f" %(T2 - T1))
	print(memory_usage())
	sys.exit(0)

if args.t and args.f and args.classif and args.s is None and args.b is None and args.F is None \
	and args.cy is None and args.k is None and args.cleanAll is False:
	print("Family Mode")
	families_mode(args.t,args.f,args.classif)
	sys.exit(0)

if args.f and args.s and args.b and args.k and args.F is None \
	and args.cy is None and args.cleanAll is False:
	print("Monomer and Family Mode")
	TAXON_ID = insert_taxon(args.t)
	monomer_mode(args.s, args.b, args.k, TAXON_ID)
	families_mode(args.f)
	TPS2 = time.time() 
	print("temps seconde : %s" %(TPS2 - TPS1))
	sys.exit(0)

if args.f and args.F and args.b is None and args.s is None \
	and args.cy is None and args.k is None and args.cleanAll is False:
	print("Family and BigFamily Mode")
	families_mode(args.f)
	bigfamilies_mode(args.F)
	TPS2 = time.time() 
	print("temps seconde %s" %(TPS2 - TPS1))
	sys.exit(0)

if args.F and args.classif and args.f is None and args.b is None and args.s is None \
	and args.cy is None and args.k is None and args.cleanAll is False:
	print("BigFamily Mode")
	clean_families(args.clean)
	bigfamilies_mode(args.F,args.classif)
	sys.exit(0)

if args.T and args.f is None and args.b is None and args.s is None \
	and args.cy is None and args.k is None and args.cleanAll is False:
	print("Supertaxon Mode")
	supertaxon_mode(args.T)
	sys.exit(0)
	
if args.cleanAll and args.f is None and args.b is None and args.s is None \
	and args.cy is None and args.k is None and args.T is None and args.t is None:
	print("Delete the database")
	clean_bank()
	sys.exit(0)

if args.s and args.b and args.f and args.F and args.cy is None \
	and args.k:
	print("Monomer, Family and BigFamily Mode")
	TAXON_ID = insert_taxon(args.t)
	monomer_mode(args.s, args.b, args.k, TAXON_ID)
	families_mode(args.f)
	bigfamilies_mode(args.F)
	TPS2 = time.time() 
	print("temps seconde %s" %TPS2 - TPS1)
	sys.exit(0)

if args.s and args.b and args.f and args.F and args.cy and args.k:
	print("Monomer, Family, BigFamily Mode, Cytoband Mode")
	TAXON_ID = insert_taxon(args.t)
	monomer_mode(args.s, args.b, args.k, TAXON_ID)
	families_mode(args.f)
	bigfamilies_mode(args.F)
	cytoband_mode(args.cy)
	TPS2 = time.time() 
	print("temps seconde %s" %TPS2 - TPS1)
	sys.exit(0)

if args.s and args.b and args.cy and args.f is None and args.F is None \
	and args.k:
	print("Monomer and Cytoband Mode")
	TAXON_ID = insert_taxon(args.t)
	print("New Taxon created")
	monomer_mode(args.s, args.b, args.k, TAXON_ID)
	cytoband_mode(args.cy)
	TPS2 = time.time() 
	print("temps s %f" %(TPS2 - TPS1))
	TPC2 = time.clock()
	print("temps CPU final %s" %(TPC2-TPC1))
	sys.exit(0)

if args.cy and args.b is None and args.s is None and args.f is None \
 and args.F is None and args.k is None:
	print("Cytoband Mode")
	#à voir pour $unset mongo ou gestion liste python ?? 
	#clean_cytoband_dependency(args.clean)
	cytoband_mode(args.cy,args.t)
	sys.exit(0)

if args.t and args.cleanAll is False and args.f is None and args.b is None and args.s is None \
	and args.cy is None and args.k is None and args.T is None:
	print("Delete taxon Mode")
	clean_taxon(args.t)
else:
	sys.exit("Syntax Error. Please check the help option -h")
