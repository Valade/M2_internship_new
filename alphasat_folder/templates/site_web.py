from ORM import *
from flask import *
import io
import json
import ORM
import pymongo
import ipdb
import gzip
import json
import base64
import re
import time
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
app = Flask(__name__)

t = ORM.CONNECTOR['mongo'][ORM.DB]['Taxon']
c = ORM.CONNECTOR['mongo'][ORM.DB]['Chromosome']
b =  ORM.CONNECTOR['mongo'][ORM.DB]['Block']
m = ORM.CONNECTOR['mongo'][ORM.DB]['Monomer']
f = ORM.CONNECTOR['mongo'][ORM.DB]['Family']

@app.route("/")
def hello():
    return ("Hello Loic !!!!")
    
@app.route("/list/Taxon", methods=['POST', 'GET'])
#affiche tous les taxons de la base
def taxon_all():
    results =list(t.find({}))
    resultsf = dict()
    taxon_name = list()
    for i in results:
        taxon_name.append(i['name'])
        resultsf[i['name']] =  i['_id']
    taxon_encode = dumps(resultsf)
    return render_template("taxon_selection.html", taxon_name = taxon_name, taxon_dict = taxon_encode)

def hidden_encode(data):
    return base64.b64encode(json.dumps(data).encode('utf-8')).decode('ascii')
    
def hidden_decode(text):
    return json.loads(base64.b64decode(text).decode('utf-8'))

@app.route("/taxons/chromosomes", methods=['POST', 'GET'])
def taxon_chromosomes():
    list_taxon_name = request.form.getlist('Taxons')
    taxon_list = request.form.getlist('taxon_dict')[0]
    taxon_decode = loads(taxon_list)
    chromosomes_list = dict()
    family_name = list()
    family = dict()
    chromosome_name_id = dict()
    print(taxon_decode)
    taxon_dict2 = dict()
   
    t1 = time.time()
    taxon_id = [taxon_decode[name] for name in list_taxon_name]
    chromosomes = list(c.find( {'Taxon_id' : {"$in" : taxon_id} }))
    chromosomes_id = {d['name'] : d['_id'] for d in chromosomes}
    all_chromosomes_id = [d['_id'] for d in chromosomes]
    
    dict2 = {str(taxon_decode[d]) : d for d in taxon_decode}
    chromosome_list = {"%s : %s" %(dict2[str(d['Taxon_id'])], d['name']) : d['_id']for d in chromosomes}
    t2 = time.time()
    print(t2-t1)
    
    t1 = time.time()
    #23h47 super mongo!!! on passe de 7s avec une boucle et 0,0999593734741211 avec une requête mongo bien ficelée!!
    family_id = list(f.find({'Chromosome_ids' : {"$elemMatch" : {'Chromosome_id' : {"$in" : all_chromosomes_id} }}}))
    t3 = time.time()
    print(t3-t1)
    if family_id != list():
        family_name = [d for d in family_id]
        family_name = list({v['_id']:v for v in family_name}.values())
        family_name_id = {str(d['_id']) : d['name'] for d in family_id}
        for g in family_name : 
            family[g['name']] = g['_id']
    t2 = time.time()
    print(t2-t1)
    return render_template("taxons_chromosomes.html", chromosome = chromosome_list, 
    family = family, list_taxon = taxon_id, chro_dict = dumps(all_chromosomes_id))


@app.route("/taxons/monomers_output", methods=['POST', 'GET'])
def monomers_output():
    monomerlist = request.form.getlist('Monomers')
    ipdb.set_trace()
    resultsf = list()
    for name in monomerlist :
            ligne = {}
            monomer_info = list(m.find({'name' : name}))[0]
            ligne['name'] = monomerinfo['name']
            ligne['begin'] = monomerinfo['begin']
            ligne['end'] = monomerinfo['end']
            ligne['strand'] = monomerinfo['strand']
            ligne['chromosome_name'] = monomerlist[i][j]['name'].split("_")[0]
            resultsf.append(ligne)
    return render_template("taxons_monomers_output.html", monomer = resultsf)


def gen_chromosome_id_list(chromosome_list):
    chromosome_id = list()
    for name in chromosome_list:
        (taxon_name, chromosome_name) = name.split(" : ")
        taxon_id = list(t.find({'name' : taxon_name}))
        taxon_id = taxon_id[0]['_id']
        v = list(c.find({'name' : chromosome_name, 'Taxon_id' : taxon_id}))[0]['_id']
        chromosome_id.append(v)
    return chromosome_id
   
def find_blocks(chromosome_list):
    g = list(b.find({'Chromosome_id' : {"$in" : chromosome_list} }))
    blocks = [d['_id'] for d in g] 
    return blocks

def find_monomers(chromosome_list):
    v = list(m.find({'Chromosome_id' : {"$in" : chromosome_list}}))
    monomers = [d['_id'] for d in v]
    return monomers
    
@app.route("/taxons/filters", methods=['POST', 'GET'])
def filters():
    chromosome_list = request.form.getlist('Chromosomes')
    family_list = request.form.getlist('Family')
    base_begin = request.form.getlist('base_pair_begin')[0]
    base_end = request.form.getlist('base_pair_end')[0]
    distance_centro = request.form.getlist('distance_from_centromer')[0]
    block_name = request.form.getlist('textarea')[0]
    taxon_list = request.form.getlist('list_taxon')
    all_chromosomes_id = loads(request.form.getlist('list_chromosomes')[0])
    chromosome_id = list()
    monomers = list()
    blocks = list()
    family_name_show = list()
    if base_end != str():
        b_end = int(base_end)
    else:
        b_end = 4000000000
    if base_begin !=str():
        b_begin = int(base_begin)
    else:
       b_begin = 0
    if distance_centro != str():
        distance_centro = int(distance_centro)
    
    if chromosome_list != list():
        chromosome_list = [ObjectId(d) for d in chromosome_list]
    
    if chromosome_list == list():
        chromosome_list = all_chromosomes_id
        chromosome_list = [str(d) for d in chromosome_list]
        chromosome_name = list()
        chromosome_list = [ObjectId(d) for d in chromosome_list]
    
    
    monomerstot = m.find({'Chromosome_id' : {"$in" : all_chromosomes_id} }).count()
    blockstot =  b.find({'Chromosome_id' : {"$in" : all_chromosomes_id} }).count()
    v = list(m.find( {'Chromosome_id' : {'$in': chromosome_list}, 'begin' : {"$gte" : b_begin} , 'end' : {"$lte" : b_end} }))
    monomers = [d['_id'] for d in v]
    blocks = find_blocks(chromosome_list)
                
    if distance_centro != '':
        chromosomes_array = list(c.find({'_id' : {'$in': chromosome_list}}))[0]
        if 'centromer' not in chromosomes_array:
            monomers = find_monomers(chromosome_id)
        else : 
            info_centromer = chromosomes_array['centromer']
            centromer_begin_list = [int(d['begin']) for d in info_centromer]
            centromer_begin = min(centromer_begin_list)
            centromer_end_list = [int(d['end']) for d in info_centromer]
            centromer_end = max(centromer_end_list)
            v = list(m.find( {'Chromosome_id' : {'$in': chromosome_list}, 'begin' : {"$gte" : (centromer_begin - distance_centro)} , 'end' : {"$lte" : (centromer_end + distance_centro)} }))
            monomers = [d['_id'] for d in v]
            blocks = find_blocks(chromosome_id)

    
    if family_list != list():
        family_list = [ObjectId(d) for d in family_list]
        v = list(m.find({'families' : {"$elemMatch" :{'family_id' :  {"$in" : family_list}}}}))
        monomers_list_family =  [d['_id'] for d in v]
        monomers = list(set(monomers)& set(monomers_list_family))
        family_list_to_name = list(f.find({'_id' : {"$in" : family_list} }))
        family_name_show = [d['name'] for d in family_list_to_name]
    
        
    if block_name != str():
        monomers2 = []
        block_list = block_name.split("\r\n")
        for block_name in block_list:
            blocks_id = list(b.find({'name' : block_name}))[0]['_id']
            v = list(m.find({'block_id' : ObjectId(blocks_id)}))
            monomers2 = monomers2 + v
        monomers2 = [d['_id'] for d in monomers2]
        monomers = [element for element in monomers if element in monomers2 ]
        block_name = block_list
   
    monomers_number = len(monomers)
    blocks_number = len(blocks)
    monomers_list_encode = dumps([d for d in monomers])
    blocks_list_encode = dumps([d for d in blocks])
    
    chromosome_list_to_name = list(c.find({'_id' : {"$in" : chromosome_list} }))
    chromosome_name_show = [d['name'] for d in chromosome_list_to_name]
    
    return render_template("show_filters.html", chromosomes = chromosome_list, family = family_list, monomers = monomers_number,
    monomerstot = monomerstot, blocks = blocks_number, blockstot = blockstot,
    monomers_list_encode = monomers_list_encode, blocks_list_encode = blocks_list_encode,
    base_begin = base_begin, base_end = base_end, distance_centro = distance_centro,
    block_name = block_name, family_name_show = family_name_show, chromosome_name_show = chromosome_name_show )

@app.route("/results/monomers",  methods=['POST', 'GET'])
def results():
    monomers_list = request.form.getlist("monomers_list_encode")[0]
    blocks_list = request.form.getlist("blocks_list_encode")[0]
    retrieve_monomers = request.form.getlist("retrieve_monomers")
    retrieve_blocks = request.form.getlist("retrieve_blocks")
    print(retrieve_monomers)
    print(retrieve_blocks)
    if retrieve_monomers == ['retrieve_monomers']:
        list_encode = monomers_list
        retrieve_type = "monomers"
    else:
        list_encode = blocks_list
        retrieve_type = "blocks"
    return render_template("results_monomers.html", list_encode = list_encode, retrieve_type = retrieve_type)

@app.route("/test_fasta")
def test_fasta():
    fst = """> coucou
ATTCTCTAGTA"""
    return Response(fst, mimetype='application/octet-stream')
    
@app.route("/test_fasta2")
def test_fasta2():
    fst = b"""> coucou
ATTCTCTAGTA"""
    response = Response(gzip.compress(fst))
    response.headers.add('Content-type', 'application/octet-stream')
    response.headers.add('Content-Disposition', 'attachment; filename="%s"' % 'monomer.fst.gz')
    return response
    
@app.route("/alphasat/accueil" , methods=['POST', 'GET'])
def accueil():
    return render_template("alphasat_accueil.html")

@app.route("/alphasat/contact", methods=['POST', 'GET'])
def contact():
    return render_template("contact.html")
    
@app.route("/alphasat/help_and_documentation", methods=['POST', 'GET'])
def documentation():
    return render_template("help_and_documentation.html")
    
@app.route("/results/monomers&blocks", methods=['POST', 'GET'])
def results_monomers_blocks():
    list_encode = loads(request.form.getlist("list_encode")[0])
    retrieve_type = request.form.getlist("retrieve_type")[0]
    format_output = request.form.getlist("format")[0]
    fasta_output = request.form.getlist("output")[0]
    csv_output = request.form.getlist("csv")[0]
    name_output = request.form.getlist("name_format")[0]
    results = io.StringIO()
    family_string = str()
    coll = {"monomers" : m, "blocks" : b}
    if format_output == 'format_fasta':
        for monomer_id in list_encode:
            #ipdb.set_trace()
            monomer_info = list(coll[retrieve_type].find({'_id' : monomer_id}))[0]
            results.write(">%s\n" %monomer_info['name'])
            sequence2 = '\n'.join((monomer_info['Sequence'])[pos:pos+60] for pos in range(0, len(monomer_info['Sequence']), 60))
            results.write( "%s\n" %sequence2 )
        sequence_output = results.getvalue()
        if fasta_output == 'fasta.gzip':
            response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
            filename = "%s.fasta.gz" %retrieve_type
        else:
            filename = "%s.fasta" %retrieve_type
            response=Response(sequence_output)
    else:
        for monomer_id in list_encode:
            separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
            monomer_info = list(coll[retrieve_type].find({'_id' : monomer_id}))[0]
            chromosome_name = monomer_info['name'].split('_',1)[0]
            results.write("%s%s" % (monomer_info['name'], separators[csv_output]))
            results.write("%s%s" % (chromosome_name, separators[csv_output]))
            results.write("%s%s" % (monomer_info['begin'], separators[csv_output]))
            results.write("%s%s" % (monomer_info['end'], separators[csv_output]))
            results.write("%s%s" % (monomer_info['strand'], separators[csv_output]))
            if 'families' in monomer_info:
                family_results = set()
                for families_name in monomer_info['families']:
                    family_results.add(families_name['name'])
                if len(family_results) == 1:
                    family_string = str(family_results)
                else: 
                    family_string = '-'.join(family_results)
                results.write("%s%s" % (family_string, separators[csv_output]))
            results.write("%s\n" %monomer_info['Sequence'])
        sequence_output = results.getvalue()
        #a garder si on veut pouvoir faire des gzip par la suite
        """response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
        response=Response(sequence_output)
        filename = "%s.txt" %retrieve_type
    response.headers.add('Content-type', 'application/octet-stream')
    response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
    return response
    

if __name__ == "__main__":
    app.run(port=PORT, host="0.0.0.0", debug=True)
