from flask import *
import io
import json
import ipdb
import gzip
import json
import base64
import re
import time
import psycopg2
import psycopg2.extras
from psycopg2.extensions import AsIs
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
from sort_functions import (atoi,natural_keys)
import svgwrite
import mimetypes
app = Flask(__name__)
PORT = 8000
from operator import itemgetter
"""Connexion a la base de donnees"""
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
taxon_id_list = []
seq_id_list = []
seq_data = []
chromosome_id_list = []
block_id_list = []
block_id_selection_list = []
family_id_list = []
monomer_id_list = []
monomers_list = []
liste_chr_old = []
liste_taxon_old = []
selection_old = []
@app.route("/list/Taxon", methods=['POST', 'GET'])
def page():
	classification_list = list()
	try:
		cursor.execute("SELECT family_classification from Family;")
	except psycopg2.Error as e:
		print (e.pgerror)
	classification = cursor.fetchall()
	for i in classification:
		classification_list.append(i['family_classification'])
	return render_template("filter_page.html", classification_list = classification_list)

@app.route("/tree",methods=['GET'])
def tree():
	tables = ['taxon', 'chromosome', 'sequence']
	#taxonSelection, , , = getVariables(request.args)
	oid = request.args.get('id', None)
	result_taxons = dict()
	taxon_name = list()
	element_name = list()
	result_element = dict()
	dico_test = list()
	results2 = list()
	#Tree vide
	if oid is None:
		sequences_number = 0
		blocks_number = 0
		monomers_number = 0
		try:
			cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id IS NULL;")
		except psycopg2.Error as e:
			print (e.pgerror)
		results = cursor.fetchall()
		cursor.execute('commit') 		
		#Pas de supertaxon racine
		if not results:
			try:
				cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id IS NOT NULL;")
			except psycopg2.Error as e:
				print (e.pgerror)
			results = cursor.fetchall()
			cursor.execute('commit')
		for result in results:
			try:
				cursor.execute("SELECT number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id = %s;",(result['taxon_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			#~ for i in cursor:
				#~ if i[0]:
					#~ result['number_sequences'] = int(result['number_sequences']) + int(i['number_sequences'])
					#~ result['number_blocks'] = int(result['number_blocks']) + int(i['number_blocks'])
					#~ result['number_monomers'] = int(result['number_monomers']) + int(i['number_monomers'])
			result_taxons[result['taxon_id']] =  [result['taxon_name'],result['number_sequences'],result['number_blocks'],result['number_monomers']]
			
		#Envoie des résultats
		for i in result_taxons:
			dico_test.append({'id':'taxon_'+str(i),'text':result_taxons[i][0]+" ( sequences : "+str(result_taxons[i][1])+" , blocks : "+str(result_taxons[i][2])+" , monomers : "+str(result_taxons[i][3])+" )","iconCls":"icon-blank", 'state':'closed'})
	#Tree non vide
	else:
		sequences_number = 0
		blocks_number = 0
		monomers_number = 0
		info = oid.split("_")
		table = info[0]
		table_id = info[1]
		if table in tables:
			k = tables.index(table)
			if k < (len(tables) - 1):
				childrentable = tables[k+1]
			#Condition taxon
			if table == "taxon":
				#Condition si taxon est un supertaxon
				try:
					cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon WHERE supertaxon_id = %s;",(table_id,))
				except psycopg2.Error as e:
					print (e.pgerror)
				results = cursor.fetchall()
				cursor.execute('commit')
				childrentable = "taxon"
				k = 0
				#Condition pas un supertaxon
				if not results:
					try:
						cursor.execute("SELECT chromosome_id from Sequence WHERE taxon_id = %s;",(table_id,))
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosomes_id = cursor.fetchall()
					for chromosome in chromosomes_id:
						try:
							cursor.execute("SELECT chromosome_name,number_sequences,number_blocks,number_monomers from Chromosome WHERE chromosome_id = %s;",(chromosome['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						results = cursor.fetchall()
						cursor.execute('commit')
						childrentable = "chromosome"
						k = 1			
						for i in results:
						#~ try:
							#~ cursor.execute("SELECT count(DISTINCT block_id) from Block WHERE sequence_id = %s;",(i["sequence_id"],))
						#~ except psycopg2.Error as e:
							#~ print (e.pgerror)
						#~ number_blocks = cursor.fetchone()
						#~ cursor.execute('commit')
						#~ try:
							#~ cursor.execute("SELECT count(DISTINCT monomer_id) from Monomer WHERE sequence_id = %s;",(i["sequence_id"],))
						#~ except psycopg2.Error as e:
							#~ print (e.pgerror)
						#~ number_monomers = cursor.fetchone()
						#~ cursor.execute('commit')											
							result_element[chromosome['chromosome_id']] =  [i['chromosome_name'].split("|")[0],i['number_sequences'], i['number_blocks'], i['number_monomers']]
					for i in result_element:
						dico_test.append({'id':childrentable+"_"+str(i),'text':result_element[i][0]+" ( sequences : "+str(result_element[i][1])+" , blocks : "+str(result_element[i][2])+" , monomers : "+str(result_element[i][3])+" )", "iconCls":"icon-blank",'state':'opened'}) #+" ( blocks: "+str(result_element[i][1])+" ,monomers: "+str(result_element[i][2])+")"
				else:
					for result in results:
						try:
							cursor.execute("SELECT number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id = %s;",(result['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						#~ for i in cursor:
							#~ if i[0]:
								#~ result['number_sequences'] = int(result['number_sequences']) + int(i['number_sequences'])
								#~ result['number_blocks'] = int(result['number_blocks']) + int(i['number_blocks'])
								#~ result['number_monomers'] = int(result['number_monomers']) + int(i['number_monomers'])
						result_element[result['taxon_id']] =  [result['taxon_name'],result['number_sequences'],result['number_blocks'],result['number_monomers']]
					for i in result_element:
						dico_test.append({'id':childrentable+"_"+str(i),'text':result_element[i][0]+" ( sequences : "+str(result_element[i][1])+" , blocks : "+str(result_element[i][2])+" , monomers : "+str(result_element[i][3])+" )", "iconCls":"icon-blank",'state':'closed'})
	dico_test.sort(key=lambda k: natural_keys(k['text']))
	return json.dumps(dico_test)
	
@app.route("/search_form", methods=['POST', 'GET'])
def search():
	#Renvoie le formulaire
	return render_template("search.html")

@app.route("/results",methods=['GET','POST'])
def results_form():
	phase = request.args.get("_phase")
	list_encode = request.args.get("monomer_list")
	retrieve_type = "toto"
	return render_template("results.html", list_encode = list_encode, retrieve_type = retrieve_type, phase = phase)

def get_max_length(data):
	mx=0
	for chr in data:
		if data[chr]['length'] > mx:
			mx=data[chr]['length']
	return(mx)

@app.route("/datagrid_sequences", methods=['POST', 'GET'])
def sequences_data():
	global seq_data
	global taxon_id_list
	global chromosome_id_list
	global seq_id_list
	global liste_chr_old
	global liste_taxon_old
	global selection_old
	seq_id_list = []
	selection = request.args.getlist('selection[]')
	search = request.args.get('search')
	liste_chr = list()
	liste_taxon = list()
	seq_id = []
	taxon_chr = []
	for i in selection:
		info = i.split("_")
		if info[0] == "chromosome":
			liste_chr.append(info[1])
		elif info[0] == "taxon":
			liste_taxon.append(info[1])
	liste_chr = list(map(int,liste_chr))
	liste_taxon = list(map(int,liste_taxon))
	taxon_id_list = liste_taxon
	chromosome_id_list = liste_chr
	subtaxon_id = []
	#Nouvelle sélection contient la sélection précédente
	print(selection_old)
	print(selection)
	if set(selection_old).issubset(selection):
		seq_id.extend(seq_data)
	if liste_chr:
		if liste_chr_old and set(liste_chr_old).issubset(liste_chr):
			liste_chr_unique = [x for x in liste_chr if x not in liste_chr_old]
		else:
			liste_chr_unique = liste_chr
		if liste_chr_unique:
			try:
				cur.execute("SELECT Sequence.sequence_name,Sequence.sequence_id,Sequence.sequence_length,Sequence.number_blocks,Sequence.number_monomers,Taxon.taxon_name,Taxon.taxon_id,Chromosome.chromosome_name FROM Sequence LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id WHERE Sequence.chromosome_id IN %s;",(tuple(liste_chr_unique),))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequences = cur.fetchall()
			seq_id.extend(sequences)
			#Liste des taxons uniques des chromosomes sélectionnés
			taxon_chr = [i['taxon_id'] for i in sequences if i['taxon_id'] not in taxon_chr]
			taxon_set = set(taxon_chr)
			#Liste des taxons sélectionnés qui ne sont pas dans la liste de taxons uniques des chromosomes
			if liste_taxon:
				taxon_unique = [x for x in liste_taxon if x not in taxon_set]
		else:
			taxon_unique = liste_taxon
	else:
		if liste_taxon:
			taxon_unique = liste_taxon
	if liste_taxon:
		#initialisation de la variable
		supertaxon = False
		if liste_taxon_old and set(liste_taxon_old).issubset(liste_taxon):
			taxon_unique_2 = [x for x in taxon_unique if x not in liste_taxon_old]
		else:
			taxon_unique_2 = taxon_unique
		for i in taxon_unique_2:
			if i not in subtaxon_id:
				try:
					cursor.execute("SELECT taxon_id FROM Taxon WHERE supertaxon_id = %s;",(i,))
				except psycopg2.Error as e:
					print (e.pgerror)
				taxon_id = cursor.fetchall()
				if taxon_id:
					for tx in taxon_id:
						if tx['taxon_id'] in liste_taxon:
							supertaxon = True
					if not supertaxon:
						for i in taxon_id:
							subtaxon_id.extend(i)
					supertaxon = False
					while taxon_id:
						subtaxon_new = []
						for j in taxon_id:
							try:
								cursor.execute("SELECT taxon_id FROM Taxon WHERE supertaxon_id = %s;",(int(j['taxon_id']),))
							except psycopg2.Error as e:
								print (e.pgerror)
							taxons = cursor.fetchall()
							subtaxon_new.extend(taxons)
							for k in taxons:
								if k['taxon_id'] in liste_taxon:
									supertaxon = True
							if not supertaxon:
								for l in taxons:
									subtaxon_id.extend(l)
						taxon_id = subtaxon_new
					supertaxon = False
				else:
					subtaxon_id.append(i)
		if subtaxon_id:
			try:
				cur.execute("SELECT Sequence.sequence_name,Sequence.sequence_id,Sequence.sequence_length,Sequence.number_blocks,Sequence.number_monomers,Taxon.taxon_name,Chromosome.chromosome_name FROM Sequence LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id WHERE Taxon.taxon_id IN %s;",(tuple(subtaxon_id),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_sequences = cur.fetchall()
			seq_id.extend(taxon_sequences)
	for i in seq_id:
		i['chromosome_name'] = i['chromosome_name'].split("|")[0]
		seq_id_list.append(i['sequence_id'])
	selection_old = selection
	liste_chr_old = liste_chr
	liste_taxon_old = liste_taxon
	seq_data = seq_id
	return json.dumps(seq_id)


@app.route("/datagrid_blocks", methods=['POST', 'GET'])
def blocks_data():
	global seq_id_list
	global block_id_list
	block_id_list = []
	selection = request.args.getlist('selection[]')
	block_length = request.args.get('search[block_length]')
	block_list = list()
	length = None
	length_max = None
	length_min = None
	seq_id = []
	#Taille des blocks
	if block_length:
		monomers = []
		list_length = block_length.rstrip("\n").split(":")
		if block_length.isdigit():
			length = int(block_length)
		elif len(list_length) == 2:
			if list_length[0].isdigit():
				length_min = int(list_length[0])
			if list_length[1].isdigit():
				length_max = int(list_length[1])
	if not selection:
		selection = list(map(int,seq_id_list))
		
	else:
		selection = list(map(int,selection))
	if selection:
		if length:
			try:
				cur.execute("SELECT Block.block_name,Block.block_id,Block.block_length,Block.block_begin,Block.block_end,Block.block_strand,Block.number_monomers,Taxon.taxon_name,Chromosome.chromosome_name,Sequence.sequence_name FROM Block LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Block.sequence_id IN %s AND Block.block_length = %s;",(tuple(selection),length,))
			except psycopg2.Error as e:
					print (e.pgerror)
			block_list = cur.fetchall()
			if block_list:
				for i in block_list:
					i['chromosome_name'] = i['chromosome_name'].split("|")[0]
					block_id_list.append(i['block_id'])
		elif length_min and length_max:
			try:
				cur.execute("SELECT Block.block_name,Block.block_id,Block.block_length,Block.block_begin,Block.block_end,Block.block_strand,Block.number_monomers,Taxon.taxon_name,Chromosome.chromosome_name,Sequence.sequence_name FROM Block LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Block.sequence_id IN %s AND Block.block_length >= %s AND Block.block_length <= %s;",(tuple(selection),length_min,length_max,))
			except psycopg2.Error as e:
					print (e.pgerror)
			block_list = cur.fetchall()
			if block_list:
				for i in block_list:
					i['chromosome_name'] = i['chromosome_name'].split("|")[0]
					block_id_list.append(i['block_id'])
		elif length_min and not length_max:
			try:
				cur.execute("SELECT Block.block_name,Block.block_id,Block.block_length,Block.block_begin,Block.block_end,Block.block_strand,Block.number_monomers,Taxon.taxon_name,Chromosome.chromosome_name,Sequence.sequence_name FROM Block LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Block.sequence_id IN %s AND Block.block_length >= %s;",(tuple(selection),length_min,))
			except psycopg2.Error as e:
					print (e.pgerror)
			block_list = cur.fetchall()
			if block_list:
				for i in block_list:
					i['chromosome_name'] = i['chromosome_name'].split("|")[0]
					block_id_list.append(i['block_id'])
		elif not length_min and length_max:
			try:
				cur.execute("SELECT Block.block_name,Block.block_id,Block.block_length,Block.block_begin,Block.block_end,Block.block_strand,Block.number_monomers,Taxon.taxon_name,Chromosome.chromosome_name,Sequence.sequence_name FROM Block LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Block.sequence_id IN %s AND Block.block_length <= %s;",(tuple(selection),length_max,))
			except psycopg2.Error as e:
					print (e.pgerror)
			block_list = cur.fetchall()
			if block_list:
				for i in block_list:
					i['chromosome_name'] = i['chromosome_name'].split("|")[0]
					block_id_list.append(i['block_id'])
		else:
			try:
				cur.execute("SELECT Block.block_name,Block.block_id,Block.block_length,Block.block_begin,Block.block_end,Block.block_strand,Block.number_monomers,Taxon.taxon_name,Chromosome.chromosome_name,Sequence.sequence_name FROM Block LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Block.sequence_id IN %s;",(tuple(selection),))
			except psycopg2.Error as e:
					print (e.pgerror)
			block_list = cur.fetchall()
			if block_list:
				for i in block_list:
					i['chromosome_name'] = i['chromosome_name'].split("|")[0]
					block_id_list.append(i['block_id'])
	return json.dumps(block_list)

@app.route("/datagrid_families", methods=['POST', 'GET'])
def families_data():
	global block_id_list
	global family_id_list
	families = []
	family_id_list = []
	monomers_id = []
	selection = request.args.getlist('selection[]')
	seq_id = []
	families_id = []
	monomers_list = []
	if not selection:
		selection = list(map(int,block_id_list))
	else:
		selection = list(map(int,selection))
	if selection:
		try:
			cur.execute("SELECT DISTINCT Family.family_id,Family.family_name,Family.superfamily_id,Family.family_classification FROM Family LEFT OUTER JOIN Belongs ON Family.family_id = Belongs.family_id LEFT OUTER JOIN Monomer ON Belongs.monomer_id = Monomer.monomer_id LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id WHERE Block.block_id IN %s;",(tuple(selection),))
		except psycopg2.Error as e:
			print (e.pgerror)
		families = cur.fetchall()
		for i in families:
			if i['superfamily_id']:
				try:
					cursor.execute("SELECT family_name FROM Family WHERE family_id = %s;",(i['superfamily_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				superfamily_name = cursor.fetchone()
				i['superfamily_name'] = superfamily_name[0]
			else:
				i['superfamily_name'] = None
			try:
				cursor.execute("SELECT count(monomer_id) FROM Belongs WHERE family_id = %s;",(i['family_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			monomers_number = cursor.fetchone()
			cursor.execute('commit')
			i['monomers_number'] = monomers_number[0]
	return json.dumps(families)


@app.route("/datagrid_monomers", methods=['POST', 'GET'])
def monomers_data():
	global block_id_list
	global block_id_selection_list
	global monomer_id_list
	global monomers_list
	global family_id_list
	monomers_list = []
	monomers_temp = []
	selection = request.args.getlist('selection[]',None)
	selection_family = request.args.getlist('selection_family[]',None)
	family_id_list = selection_family
	monomer_length = request.args.get('search[monomer_length]')
	monomer_position = request.args.get('search[monomer_begin_end]')
	monomer_word = request.args.get('search[monomer_word]')
	length = None
	length_max = None
	length_min = None
	position = None
	position_min = None
	position_max = None
	seq_id = []
	families_id = []
	#Taille des monomers
	if monomer_length:
		list_length = monomer_length.rstrip("\n").split(":")
		if monomer_length.isdigit():
			length = int(monomer_length)
		elif len(list_length) == 2:
			if list_length[0].isdigit():
				length_min = int(list_length[0])
			if list_length[1].isdigit():
				length_max = int(list_length[1])
	if monomer_position:
		list_position = monomer_position.rstrip("\n").split(":")
		if monomer_position.isdigit():
			position = int(monomer_position)
		elif len(list_position) == 2:
			if list_position[0].isdigit():
				position_min = int(list_position[0])
			if list_position[1].isdigit():
				position_max = int(list_position[1])				

	if not selection and not selection_family:
		selection = list(map(int,block_id_list))
	elif selection and not selection_family:
		selection = list(map(int,selection))
	elif not selection and selection_family:
		try:
			cursor.execute("SELECT Monomer.monomer_id FROM Monomer LEFT OUTER JOIN Belongs ON Monomer.monomer_id = Belongs.monomer_id WHERE Belongs.family_id IN %s AND Monomer.monomer_id IN %s;",(tuple(selection_family),tuple(monomer_id_list),))
		except psycopg2.Error as e:
			print (e.pgerror)
		fam_mono = cursor.fetchall()
		for i in fam_mono:
			selection.append(i['monomer_id'])
		cur.execute('commit')
	if selection:	
		if length:
			if selection_family:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.monomer_id IN %s AND Monomer.monomer_length = %s;",(tuple(selection),length,))
				except psycopg2.Error as e:	
					print (e.pgerror)				
			else:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.block_id IN %s AND Monomer.monomer_length = %s;",(tuple(selection),length,))
				except psycopg2.Error as e:	
					print (e.pgerror)
			monomers_list = cur.fetchall()
			for i in monomers_list:
				i['chromosome_name'] = i['chromosome_name'].split("|")[0]
				monomer_id_list.append(i['monomer_id'])			
		elif length_min and length_max:
			if selection_family:			
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.monomer_id IN %s AND Monomer.monomer_length >= %s AND Monomer.monomer_length <= %s;",(tuple(selection),length_min,length_max,))
				except psycopg2.Error as e:	
					print (e.pgerror)
			else:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name,Monomer.monomer_sequence, Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.block_id IN %s AND Monomer.monomer_length >= %s AND Monomer.monomer_length <= %s;",(tuple(selection),length_min,length_max,))
				except psycopg2.Error as e:	
					print (e.pgerror)				
			monomers_list = cur.fetchall()
			for i in monomers_list:
				i['chromosome_name'] = i['chromosome_name'].split("|")[0]
				monomer_id_list.append(i['monomer_id'])
		elif length_min and not length_max:
			if selection_family:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.monomer_id IN %s AND Monomer.monomer_length >= %s;",(tuple(selection),length_min,))
				except psycopg2.Error as e:	
					print (e.pgerror)
			else:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.block_id IN %s AND Monomer.monomer_length >= %s;",(tuple(selection),length_min,))
				except psycopg2.Error as e:	
					print (e.pgerror)
			monomers_list = cur.fetchall()
			for i in monomers_list:
				i['chromosome_name'] = i['chromosome_name'].split("|")[0]
				monomer_id_list.append(i['monomer_id'])
		elif not length_min and length_max:
			if selection_family:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.monomer_id IN %s AND Monomer.monomer_length <= %s;",(tuple(selection),length_max,))
				except psycopg2.Error as e:	
					print (e.pgerror)
			else:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.block_id IN %s AND Monomer.monomer_length <= %s;",(tuple(selection),length_max,))
				except psycopg2.Error as e:	
					print (e.pgerror)				
			monomers_list = cur.fetchall()
			for i in monomers_list:
				i['chromosome_name'] = i['chromosome_name'].split("|")[0]
				monomer_id_list.append(i['monomer_id'])
		else:
			if selection_family:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.monomer_id IN %s;",(tuple(selection),))
				except psycopg2.Error as e:	
					print (e.pgerror)
			else:
				try:
					cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_name, Monomer.monomer_sequence,Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.block_id IN %s;",(tuple(selection),))
				except psycopg2.Error as e:	
					print (e.pgerror)
			monomers_list = cur.fetchall()
			for i in monomers_list:
				i['chromosome_name'] = i['chromosome_name'].split("|")[0]
				monomer_id_list.append(i['monomer_id'])
		#Filtre positions des monomers
		if monomer_position:
			monomers_list2 = monomers_list
			monomers_list = []
			monomer_id_list = []
			if position:
				for mono in monomers_list2:
					if int(mono['monomer_begin_sequence']) <= position and int(mono['monomer_end_sequence']) >= position:
						monomers_list.append(mono)
						monomer_id_list.append(mono['monomer_id'])
			elif position_min and position_max:
				for mono in monomers_list2:
					if (int(mono['monomer_begin_sequence']) >= position_min and int(mono['monomer_begin_sequence']) <= position_max) or (int(mono['monomer_end_sequence']) <= position_min and int(mono['monomer_end_sequence']) >= position_max) or (int(mono['monomer_begin_sequence']) <= position_min and int(mono['monomer_end_sequence']) >= position_max):
						monomers_list.append(mono)
						monomer_id_list.append(mono['monomer_id'])
			elif position_min and not position_max:
				for mono in monomers_list2:
					if int(mono['monomer_end_sequence']) >= position_min:
						monomers_list.append(mono)
						monomer_id_list.append(mono['monomer_id'])
			elif position_max and not position_min:
				for mono in monomers_list2:
					if int(mono['monomer_begin_sequence']) <= position_max:
						monomers_list.append(mono)
						monomer_id_list.append(mono['monomer_id'])
		#Filtre mots
		if monomer_word:
			monomers_list = word_search(monomer_word,monomers_list)
	return json.dumps(monomers_list)

def word_search(word,selection):
	T1 = time.time()
	new_selection = []
	#~ regular = "^"
	regular = ""
	word = word.upper()
	for i in word:
		if i == 'N' or i == 'X':
			regular += "[A|C|G|T]"
		elif i == 'K':
			regular += "[G|T]"
		elif i == 'M':
			regular += "[A|C]"
		elif i == 'R':
			regular += "[A|G]"
		elif i == 'Y':
			regular += "[C|T]"
		elif i == 'W':
			regular += "[A|T]"
		elif i == 'B':
			regular += "[C|G|T]"
		elif i == 'V':
			regular += "[A|C|G]"
		elif i == 'H':
			regular += "[A|C|T]"
		elif i == 'D':
			regular += "[A|G|T]"
		elif i == '.':
			regular += "[^A,C,G,T]"
		#~ elif i == 'A' or i == 'G' or i == 'T' or i == 'C':
			#~ regular += i
		else:
			regular += i
	#~ regular += "$"
	re.compile(regular)
	T2 = time.time()
	print("time regex %s" %(T2 - T1))
	print(regular)
	#~ try:
		#~ cur.execute("SELECT Monomer.monomer_id, Monomer.monomer_sequence, Monomer.monomer_name, Monomer.sequence_id, Monomer.monomer_length, Monomer.monomer_begin_sequence, Monomer.monomer_end_sequence, Monomer.monomer_strand,Block.block_name,Sequence.sequence_name,Taxon.taxon_name,Chromosome.chromosome_name FROM Monomer LEFT OUTER JOIN Block ON Monomer.block_id = Block.block_id LEFT OUTER JOIN Sequence ON Block.sequence_id = Sequence.sequence_id LEFT OUTER JOIN Chromosome ON Sequence.chromosome_id = Chromosome.chromosome_id LEFT OUTER JOIN Taxon ON Sequence.taxon_id = Taxon.taxon_id WHERE Monomer.block_id IN %s;",(tuple(selection),))
	#~ except psycopg2.Error as e:
		#~ print (e.pgerror)
	#~ monomers = cur.fetchall()
	T3 = time.time()
	for seq in selection:
		#~ for i in range(len(seq['monomer_sequence'])):
		parse = re.search(regular, seq['monomer_sequence'].upper())#seq['monomer_sequence'][i:i+len(word)].upper())
		match = re.match(regular, seq['monomer_sequence'].upper())
		if parse or match:
			new_selection.append(seq)
	T4 = time.time()
	print("time boucle %s"%(T4 - T3))
	return(new_selection)

def monomer_phase(index,phase,length):
	#~ print(index)
	#~ print(phase)
	begin = None
	end = None
	ii = 0
	#~ print(type(phase))
	#~ print("les tailles")
	#~ print(str(length))
	#~ print(str(len(index)-1))
	#~ print(str(index[0]))
	if ii >= (len(index)-1):
		#~ print("elao")
		begin = ii
	elif ii  < (len(index)-1) and index[ii] >= phase:
		#~ print("tote")
		begin = ii
	else:
		#~ print("toto")		
		while ii < (len(index)-1) and index[ii] < phase:
			begin = ii + 1
			ii += 1
			#~ print(begin)
	#~ print("tata")
	jj = begin - 1
	while jj >= 0 and index[jj] <= 0:
		begin = jj
		jj -= 1
	yy = length
	if yy == len(index):
		#~ print("alo")
		end = yy - 1
	elif yy <= len(index) and index[yy] >= phase:
		#~ print("tota")
		end = yy - 1
	else:
		while yy <= (len(index) - 1) and index[yy] < phase:
			#~ print("titi")
			end = yy
			yy += 1
	zz = end
	while zz >= 0  and index[zz] <= 0:
		#~ print("tete")
		end = zz
		zz -= 1
	return(begin,end)

@app.route("/results/files",methods=['GET','POST'])
def files_results():
	#parametres
	mimetypes.add_type('image/svg+xml', '.svg', True)
	mimetypes.add_type("image/svg+xml", ".svgz", True)
	global taxon_id_list
	global chromosome_id_list
	global monomer_id_list
	list_encode = monomer_id_list
	#~ phase = request.form.get('monomer_phase',None)
	phase = request.form.get('phase',None)
	if phase.isdigit():
		phase = int(phase)
	else:
		phase = 1
	print("phase")
	print(str(phase))
	#variable
	monomer_selection = request.form.getlist("monomer_selection")
	print(monomer_selection)
	liste_monomer = []
	if monomer_selection[0]:
		liste_monomer = list(map(int,monomer_selection[0].rstrip('\n').split(',')))
	else:
		liste_monomer = list(map(int,monomer_id_list))
	format_output = request.form.getlist("format")[0]
	print("format")
	print(format_output)
	csv_output = request.form.get("csv")
	#Type de nom
	name_option = request.form.get("name_option")
	#Infos contenues dans le nom
	name_format = request.form.getlist("name_format")
	print("name_format")
	print(name_format)
	separator_format = request.form.get("separator_format")
	results = io.StringIO()
	family_string = str()
	coll = {"monomers" : 'Monomer', "blocks" : 'Block'}
	mono_counter = 0
	block_counter = 0
	liste_name = list()
	##temp
	retrieve_type = "monomers"
	if coll[retrieve_type] == 'Monomer':
		if format_output == 'format_fasta':
			T1 = time.time()
			try:
				cursor.execute("SELECT monomer_index,monomer_dimere,monomer_length,monomer_id FROM Monomer WHERE monomer_id IN %s ;",(tuple(list_encode),) )
			except psycopg2.Error as e:
				print (e.pgerror)
			fasta_monomer = cursor.fetchall()
			cursor.execute('commit')
			for monomer in fasta_monomer:
				name = monomer_name(name_option,name_format,monomer['monomer_id'],separator_format)				
				index = monomer['monomer_index']
				nucleotide = monomer['monomer_dimere']
				length = monomer['monomer_length']
				results.write(">%s\n" %name)		
				if phase == 1:
					#~ TA = time.time()
					try:
						cursor.execute("SELECT monomer_sequence FROM Monomer WHERE monomer_id = %s ;",(monomer['monomer_id'],) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_seq = cursor.fetchone()
					cursor.execute('commit')
					monomer_unique = monomer_seq['monomer_sequence']
					sequence2 = '\n'.join((monomer_unique)[pos:pos+60] for pos in range(0,len(monomer_unique), 60))
					results.write( "%s\n" %sequence2 )
					#~ TB = time.time()
					#~ print( "time if %f"%(TB - TA))
				else:
					begin,end = monomer_phase(index,phase,length)
					seq = nucleotide[begin:end+1]
					sequence2 = '\n'.join(seq[pos:pos+60] for pos in range(0, len(seq),60))
					results.write( "%s\n" %sequence2 )
			sequence_output = results.getvalue()			
			#~ if fasta_output == 'fasta.gzip':
				#~ response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				#~ filename = "%s.fasta.gz" %retrieve_type
			filename = "%s.fasta" %retrieve_type
			response=Response(sequence_output)
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
			T2 = time.time()
			print( "time fasta %f"%(T2 - T1))
		elif format_output == 'format_csv':
			#Fichier CSV
			separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
			classif_list = list()
			for identifiant in list_encode:
				try:
					cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;", (identifiant,))
				except psycopg2.Error as e:
					print (e.pgerror)
				families_id = cursor.fetchall()
				for i in families_id:
					try:
						cursor.execute("SELECT family_classification FROM Family WHERE family_id = %s ;", (i['family_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					family_name = cursor.fetchone()
					if family_name[0] not in classif_list:
						classif_list.append(family_name[0])
			classif_list.sort()
			header = "name"+separators[csv_output]+"chromosome"+separators[csv_output]+"begin"+separators[csv_output]+"end"+separators[csv_output]+"strand"+separators[csv_output]+"length"+separators[csv_output]+separators[csv_output].join(classif_list)+separators[csv_output]+"sequence"+"\n"
			results.write(header)
			for identifiant in list_encode:
				family_dict = dict()
				name = monomer_name(name_format,identifiant,separator_format)
				try:
					cursor.execute("SELECT monomer_id,monomer_dimere,monomer_begin_sequence,monomer_end_sequence,monomer_strand,sequence_id,monomer_length FROM Monomer WHERE monomer_id = %s ;", (identifiant,))
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(monomer_id,nucleotide,begin,end,strand,sequence_id,length) = info				
				try:
					cursor.execute("SELECT chromosome_id FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_id = cursor.fetchone()
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s ;",(chromosome_id[0],) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()			
				cursor.execute('commit')
				try:
					cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(monomer_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				families_list = cursor.fetchall()
				for i in families_list:
					try:
						cursor.execute("SELECT family_name,family_classification FROM Family WHERE family_id = %s ;",(i['family_id'],) )
					except psycopg2.Error as e:
						print (e.pgerror)
					family_infos = cursor.fetchall()
					for i in family_infos:
						family_dict[i['family_classification']] = i['family_name']
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name[0].split('|')[0], separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				results.write("%s%s" % (length, separators[csv_output]))
				for i in classif_list:
					if i in family_dict.keys():
						results.write("%s%s" % (family_dict[i], separators[csv_output]))
					else:
						results.write("%s%s" % ("NA", separators[csv_output]))
				results.write("%s\n" % nucleotide)
				"""if 'families' in monomer_info:
					family_results = set()
					for families_name in monomer_info['families']:
						family_results.add(families_name['name'])
					if len(family_results) == 1:
						family_string = str(family_results)
					else:
						family_string = '-'.join(family_results)
					results.write("%s%s" % (family_string, separators[csv_output]))"""
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.csv" %retrieve_type
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
		elif format_output == 'format_svg':
			#Fichier SVG
			taxon_list = taxon_id_list
			print(chromosome_id_list)
			if chromosome_id_list:
				taxon_list = []
				try:
					cursor.execute("SELECT taxon_id FROM Sequence WHERE chromosome_id = ANY(%s) ;",(chromosome_id_list,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				taxons = cursor.fetchall()
				for i in taxons:
					if i['taxon_id'] not in taxon_list:
						taxon_list.append(i['taxon_id'])
			nb_tx = len(taxon_list)
			family_id = list()
			family_dict = dict()
			family_dict["no family"] = [None,None,"pink"]
			color = ['blue','green','orange','purple','red']
			filename = "%s.svg" %retrieve_type
			width=18000 * nb_tx
			width_name = 5000
			heigh=6000 * nb_tx
			heigh_plus=1000
			svg_document = svgwrite.Drawing(filename = filename,size = (width+width_name, heigh+heigh_plus),profile='full')
			taxon_space = 0
			nb_tx = len(taxon_list)
			taxon_number = 1
			max_chro = 0
			for tx in taxon_list:
				#~ #print("tx")
				#~ #print(tx)
				try:
					cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				seq_id = cursor.fetchall()
				#~ #print("tailles")
				#~ #print(str(len(seq_id)))
				#~ #print(str(max_chro))
				if len(seq_id) > max_chro:
					max_chro = len(seq_id)
			for tx in taxon_list:
				print("taxon")
				print(tx)
				data={}
				monomer_data = {}
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				tx_name = cursor.fetchone()[0]
				try:
					cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				seq_id = cursor.fetchall()
				for i in seq_id:
					monomer_data[i['sequence_name']] = { 'monomers': [] }
					parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", i['sequence_name'])
					if parse:
						#print(i['sequence_name'])
						try:
							cursor.execute("SELECT sequence_cytoband FROM Sequence WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						cytoband_list = cursor.fetchone()
						try:
							cursor.execute("SELECT block_id FROM Block WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						blocks_id = cursor.fetchall()
						try:
							cursor.execute("SELECT monomer_id,monomer_begin_sequence,monomer_end_sequence,monomer_length FROM Monomer WHERE block_id = ANY(%s) ;",(blocks_id,) )
						except psycopg2.Error as e:
							print (e.pgerror)												
						monomers = cursor.fetchall()
						for j in monomers:							
							monomers_infos = {'monomer_id' : j['monomer_id'],'monomer_begin_sequence' : j['monomer_begin_sequence'],'monomer_end_sequence' : j['monomer_end_sequence'],'monomer_length' : j['monomer_length']}
							monomer_data[i['sequence_name']]['monomers'].append(monomers_infos)
						if cytoband_list[0] is not None:
							for row in cytoband_list['sequence_cytoband']:
								if row[0] not in data:
									data[row[0]]={'length': 0, 'bands': []}
								if row[0] in data:
									if int(row[2]) > int(data[row[0]]['length']):
										data[row[0]]['length']=int(row[2])
										bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
										data[row[0]]['bands'].append(bd)
				heigh_tx = (heigh / nb_tx)
				heigh_chr = heigh_tx * taxon_number
				heigh_plus_tx = (heigh_plus / nb_tx)
				nb_chr=len(data)
				#~ print("data")
				#~ print(data)
				#print(nb_chr)
				max_length = get_max_length(data)
				#X et Y
				#Fraction de la largeur pour chaque chromosome
				x=width/max_chro
				#Ratio nb de pixel par pb
				y=(heigh_tx - 500)/(max_length - 10000)
				nb=0
				g = svg_document.g(style="font-size:280;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:5;fill:none")
				g.add(svg_document.text(tx_name,
												insert = (200, heigh_chr - 500),
												fill = 'black'
												))
				chromosomes = list(data.keys())
				chromosomes.sort(key=lambda k: natural_keys(k))
				for chro in chromosomes:
					ins=((nb*x) + 5000,  (heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y)
					siz=(x/2, data[chro]['length']*y)
					#Ajouter un element
					a = svg_document.g(style="font-size:240;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
					g.add(svg_document.text(chro[3:],
													insert = ((nb*x + x/8) + 5000, heigh_chr + 400),
													fill = 'black'
													))
					svg_document.add(g)
					st=(heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y
					for bd in data[chro]['bands']:
						ins=((nb*x) + 5000, st+bd['start']*y)
						siz=(x/2, (bd['end']-bd['start'])*y)
						col="rgb(255,255,255)"
						if bd['type'] == 'gneg':
							#blanc
							col="rgb(255,255,255)"
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos100':
							#noir
							col='rgb(0, 0, 0)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos75':
							#gris foncé
							col='rgb(169, 169, 169)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos50':
							#gris clair
							col='rgb(192, 192, 192)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos25':
							#gris très clair
							col='rgb(211, 211, 211)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'acen':
							#noir
							col='rgb( 0, 0, 0)'
							#Ajouter un rectangle pour les bandes
							ins=(((nb*x)+((x/2)*(1/4)))+ 5000, st+bd['start']*y)
							siz = (x/4, (bd['end']-bd['start'])*y)
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gvar':
							#blanc
							col='rgb( 255, 255, 255)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'stalk':
							#rouge
							col='rgb( 220, 220, 220)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
					for seq in monomer_data:
						if seq == chro:
							for mono in monomer_data[seq]['monomers']:
								if liste_monomer:
									if mono['monomer_id'] in liste_monomer:
										family_id = []
										try:
											cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
										except psycopg2.Error as e:
											print (e.pgerror)												
										family = cursor.fetchall()
										if family:
											for i in family:
												if i['family_id'] not in family_id:
													family_id.append(i['family_id'])
											try:
												cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
											except psycopg2.Error as e:
												print (e.pgerror)										
											family_infos = cursor.fetchall()
											print(family_dict)								
											if family_infos:
												for i in family_infos:
													if i['family_id'] not in family_dict.keys():
														family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
														del color[0]
										else:
											family_id.append("no family")
										ins=((nb*x)+ 5000, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
										siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*1000)
										svg_document.add(svg_document.rect(insert = ins,
																		size = siz,
																		stroke_width = "1",
																		stroke = family_dict[family_id[0]][2],
																		fill = family_dict[family_id[0]][2]))
										ins2=(ins[0]-x/2, ins[1])
										svg_document.add(svg_document.text("*", insert=ins2, fill = family_dict[family_id[0]][2]))
								else:
									family_id = []
									try:
										cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
									except psycopg2.Error as e:
										print (e.pgerror)												
									family = cursor.fetchall()
									if family:
										for i in family:
											if i['family_id'] not in family_id:
												family_id.append(i['family_id'])
										try:
											cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
										except psycopg2.Error as e:
											print (e.pgerror)										
										family_infos = cursor.fetchall()								
										if family_infos:
											for i in family_infos:
												if i['family_id'] not in family_dict.keys():
													family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
													del color[0]
									else:
										family_id.append("no family")
									ins=((nb*x)+ 5000, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
									siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*1000)
									svg_document.add(svg_document.rect(insert = ins,
																	size = siz,
																	stroke_width = "1",
																	stroke = family_dict[family_id[0]][2],
																	fill = family_dict[family_id[0]][2]))
									ins2=(ins[0]-x/2, ins[1])
									svg_document.add(svg_document.text("*", ins2, fill = family_dict[family_id[0]][2]))
					nb=nb+1
				taxon_number += 1
			svg_document.save()
			###fin
			svg_file = svg_document.tostring()
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response = Response(svg_file)		
			response.headers.add('Content-type', 'image/svg+xml')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	elif coll[retrieve_type] == Block:
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				try:
					cursor.execute("SELECT block_name,block_nucleotide FROM Block WHERE block_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(name,nucleotide) = info
				results.write(">%s\n" %name)
				sequence2 = '\n'.join((nucleotide)[pos:pos+60] for pos in range(0, len(nucleotide), 60))
				results.write( "%s\n" %sequence2 )
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		else:
			for identifiant in list_encode:
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				if name_format == "database_name":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(name,nucleotide,begin,end,strand,sequence_id) = info
				elif name_format == "chr_begin_end_strand":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(block_name,nucleotide,begin,end,strand,sequence_id) = info
					try:
						cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosome_name = cursor.fetchone()
					cursor.execute('commit')
					name = chromosome_name[0]+"_"+str(begin)+"_"+str(end)+"_"+str(strand)			
				elif name_format == "simple_numbering":
					try:
						cursor.execute("SELECT block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,begin,end,strand,sequence_id) = info
					block_counter += 1
					name = str(block_counter)
				try:
					cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name, separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	return response

@app.route("/karyotype", methods=['POST', 'GET'])
def caryotype():
	mimetypes.add_type('image/svg+xml', '.svg', True)
	mimetypes.add_type("image/svg+xml", ".svgz", True)
	global taxon_id_list
	global chromosome_id_list
	global monomer_id_list
	#variable
	monomer_selection = request.form.getlist("monomer_selection")
	print(monomer_selection)
	liste_monomer = []
	if monomer_selection[0]:
		liste_monomer = list(map(int,monomer_selection[0].rstrip('\n').split(',')))
	else:
		liste_monomer = list(map(int,monomer_id_list))
	print(liste_monomer)
	#Fichier SVG
	taxon_list = taxon_id_list
	print(chromosome_id_list)
	if chromosome_id_list:
		taxon_list = []
		try:
			cursor.execute("SELECT taxon_id FROM Sequence WHERE chromosome_id = ANY(%s) ;",(chromosome_id_list,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		taxons = cursor.fetchall()
		for i in taxons:
			if i['taxon_id'] not in taxon_list:
				taxon_list.append(i['taxon_id'])
	nb_tx = len(taxon_list)
	family_id = list()
	family_dict = dict()
	family_dict["no family"] = [None,None,"pink"]
	color = ['blue','green','orange','purple','red']
	retrieve_type = "monomer"
	filename = "%s.svg" %retrieve_type
	width=1800 * nb_tx
	width_name = 500
	heigh=600 * nb_tx
	heigh_plus=100
	svg_document = svgwrite.Drawing(filename = filename,size = (width+width_name, heigh+heigh_plus),profile='full')
	taxon_space = 0
	taxon_number = 1
	max_chro = 0
	for tx in taxon_list:
		#~ #print("tx")
		#~ #print(tx)
		try:
			cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		seq_id = cursor.fetchall()
		#~ #print("tailles")
		#~ #print(str(len(seq_id)))
		#~ #print(str(max_chro))
		if len(seq_id) > max_chro:
			max_chro = len(seq_id)
	for tx in taxon_list:
		print("taxon")
		print(tx)
		data={}
		monomer_data = {}
		try:
			cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s ;",(tx,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		tx_name = cursor.fetchone()[0]
		try:
			cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		seq_id = cursor.fetchall()
		for i in seq_id:
			monomer_data[i['sequence_name']] = { 'monomers': [] }
			parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", i['sequence_name'])
			if parse:
				#print(i['sequence_name'])
				try:
					cursor.execute("SELECT sequence_cytoband FROM Sequence WHERE sequence_id = %s ;",(i['sequence_id'],) )
				except psycopg2.Error as e:
					print (e.pgerror)
				cytoband_list = cursor.fetchone()
				try:
					cursor.execute("SELECT block_id FROM Block WHERE sequence_id = %s ;",(i['sequence_id'],) )
				except psycopg2.Error as e:
					print (e.pgerror)
				blocks_id = cursor.fetchall()
				try:
					cursor.execute("SELECT monomer_id,monomer_begin_sequence,monomer_end_sequence,monomer_length FROM Monomer WHERE block_id = ANY(%s) ;",(blocks_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)												
				monomers = cursor.fetchall()
				for j in monomers:							
					monomers_infos = {'monomer_id' : j['monomer_id'],'monomer_begin_sequence' : j['monomer_begin_sequence'],'monomer_end_sequence' : j['monomer_end_sequence'],'monomer_length' : j['monomer_length']}
					monomer_data[i['sequence_name']]['monomers'].append(monomers_infos)
				if cytoband_list[0] is not None:
					for row in cytoband_list['sequence_cytoband']:
						if row[0] not in data:
							data[row[0]]={'length': 0, 'bands': []}
						if row[0] in data:
							if int(row[2]) > int(data[row[0]]['length']):
								data[row[0]]['length']=int(row[2])
								bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
								data[row[0]]['bands'].append(bd)
		heigh_tx = (heigh / nb_tx)
		heigh_chr = heigh_tx * taxon_number
		heigh_plus_tx = (heigh_plus / nb_tx)
		nb_chr=len(data)
		#~ print("data")
		#~ print(data)
		#print(nb_chr)
		max_length = get_max_length(data)
		#X et Y
		#Fraction de la largeur pour chaque chromosome
		x=width/max_chro
		#Ratio nb de pixel par pb
		y=(heigh_tx - 50)/(max_length - 1000)
		nb=0
		g = svg_document.g(style="font-size:20;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
		g.add(svg_document.text(tx_name,
										insert = (20, heigh_chr - 500),
										fill = 'black'
										))
		chromosomes = list(data.keys())
		chromosomes.sort(key=lambda k: natural_keys(k))
		for chro in chromosomes:
			ins=((nb*x) + 500,  (heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y)
			siz=(x/2, data[chro]['length']*y)
			#Ajouter un element
			a = svg_document.g(style="font-size:20;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
			g.add(svg_document.text(chro[3:],
											insert = ((nb*x + x/8) + 500, heigh_chr + 40),
											fill = 'black'
											))
			svg_document.add(g)
			st=(heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y
			for bd in data[chro]['bands']:
				ins=((nb*x) + 500, st+bd['start']*y)
				siz=(x/2, (bd['end']-bd['start'])*y)
				col="rgb(255,255,255)"
				if bd['type'] == 'gneg':
					#blanc
					col="rgb(255,255,255)"
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos100':
					#noir
					col='rgb(0, 0, 0)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos75':
					#gris foncé
					col='rgb(169, 169, 169)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos50':
					#gris clair
					col='rgb(192, 192, 192)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos25':
					#gris très clair
					col='rgb(211, 211, 211)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'acen':
					#noir
					col='rgb( 0, 0, 0)'
					#Ajouter un rectangle pour les bandes
					ins=(((nb*x)+((x/2)*(1/4)))+ 500, st+bd['start']*y)
					siz = (x/4, (bd['end']-bd['start'])*y)
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gvar':
					#blanc
					col='rgb( 255, 255, 255)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'stalk':
					#rouge
					col='rgb( 220, 220, 220)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
			for seq in monomer_data:
				if seq == chro:
					for mono in monomer_data[seq]['monomers']:
						if liste_monomer:
							if mono['monomer_id'] in liste_monomer:
								family_id = []
								try:
									cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
								except psycopg2.Error as e:
									print (e.pgerror)												
								family = cursor.fetchall()
								if family:
									for i in family:
										if i['family_id'] not in family_id:
											family_id.append(i['family_id'])
									try:
										cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
									except psycopg2.Error as e:
										print (e.pgerror)										
									family_infos = cursor.fetchall()
									print(family_dict)								
									if family_infos:
										for i in family_infos:
											if i['family_id'] not in family_dict.keys():
												family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
												del color[0]
								else:
									family_id.append("no family")
								ins=((nb*x)+ 500, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
								siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*100)
								svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = family_dict[family_id[0]][2],
																fill = family_dict[family_id[0]][2]))
								#~ ins2=(ins[0]-x/2, ins[1])
								#~ svg_document.add(svg_document.text("*", insert=ins2, fill = family_dict[family_id[0]][2]))
						else:
							family_id = []
							try:
								cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
							except psycopg2.Error as e:
								print (e.pgerror)												
							family = cursor.fetchall()
							if family:
								for i in family:
									if i['family_id'] not in family_id:
										family_id.append(i['family_id'])
								try:
									cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
								except psycopg2.Error as e:
									print (e.pgerror)										
								family_infos = cursor.fetchall()								
								if family_infos:
									for i in family_infos:
										if i['family_id'] not in family_dict.keys():
											family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
											del color[0]
							else:
								family_id.append("no family")
							ins=((nb*x)+ 500, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
							siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*100)
							svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = family_dict[family_id[0]][2],
															fill = family_dict[family_id[0]][2]))
							#~ ins2=(ins[0]-x/2, ins[1])
							#~ svg_document.add(svg_document.text("*", insert=ins2, fill = family_dict[family_id[0]][2]))
			nb=nb+1
		taxon_number += 1
	svg_document.save()
	###fin
	svg_file = svg_document.tostring()
	return render_template("caryotype.html", svg = svg_file)

def monomer_name(name_option,name_format,identifiant,separator_format):
	liste_name = []
	print(separator_format)
	if name_option == "database_name":
		try:
			cursor.execute("SELECT monomer_name FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_database = cursor.fetchone()
		cursor.execute('commit')
		name = monomer_database['monomer_name']
	elif name_option == "custom_name":
		if "block_name" in name_format:
			try:
				cursor.execute("SELECT block_id FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)		
			block_id = cursor.fetchone()
			cursor.execute('commit')					
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s ;",(block_id[0],) )
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(block_name['block_name'])
		if "monomer_id" in name_format:
			liste_name.append(identifiant)
		if "begin" in name_format:
			try:
				cursor.execute("SELECT monomer_begin FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_begin = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_begin['monomer_begin']))
		if "end" in name_format:
			try:
				cursor.execute("SELECT monomer_end FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_end = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_end['monomer_end']))
		if "strand" in name_format:
			try:
				cursor.execute("SELECT monomer_strand FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_strand = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_strand['monomer_strand']))
		if "monomer_length" in name_format:
			try:
				cursor.execute("SELECT monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_length = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_length['monomer_length']))
		if "family_name" in name_format:
			family_list = []
			try:
				cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			families = cursor.fetchall()
			cursor.execute('commit')
			if families:
				for i in families:
					try:
						cursor.execute("SELECT family_name FROM Family WHERE family_id = %s ;",(i['family_id'],) )
					except psycopg2.Error as e:
						print (e.pgerror)
					family_name = cursor.fetchone()
					if family_name[0]:
						family_list.append(family_name[0])
					else:
						family_list.append("NA")
			else:
				family_list.append("NA")
			family_list.sort()
			liste_name.extend(family_list)		
		liste_name2 = list(map(str,liste_name))
		name = separator_format.join(liste_name2)
	else:
		print("Wrong name otions")
	return(name)

def hidden_encode(data):
	return base64.b64encode(json.dumps(data).encode('utf-8')).decode('ascii')
	
def hidden_decode(text):
	return json.loads(base64.b64decode(text).decode('utf-8'))

@app.route("/alphasat/accueil" , methods=['POST', 'GET'])
def accueil():
	return render_template("alphasat_accueil.html")

@app.route("/alphasat/contact", methods=['POST', 'GET'])
def contact():
	return render_template("contact.html")
	
@app.route("/alphasat/help_and_documentation", methods=['POST', 'GET'])
def documentation():
	return render_template("help_and_documentation.html")
	
if __name__ == "__main__":
	app.run(port=PORT, host="0.0.0.0", debug=True)

