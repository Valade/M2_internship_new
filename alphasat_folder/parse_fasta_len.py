from Bio import SeqIO
def fasta(src,dst):
	for seq_record in SeqIO.parse(src,"fasta"):
		if len(seq_record) > 170:
			SeqIO.write(seq_record,dst,"fasta")			
source = open("monomers_Cs_Hs.fasta", "r")

destination = open("monomers_Cs_Hs_update.fasta","w")
try:
	fasta(source,destination)
finally:
	destination.close()
	source.close()
