#!/bin/bash

echo "db size repetition cpu requete iteration time"
for FF in *.log
	do
	SIZE=${FF%.log}
	SIZE=${SIZE##*[a-z]}
	DB=${FF%[1-9]*}
	awk -v db=$DB -v size=$SIZE '$1~"###" {rep++} 
	$1=="Temps" {
			print db, size, rep,$2,$6,$8,$10
		}' $FF

	done
	
