#!/bin/bash
j=1
while test $j != 11
	do
	i=1
	while test $i != 6
		do
		python3.3 script_benchmark.py test$i &>> out_index${i}.log
		i=$(($i + 1))
	done
	j=$(($j + 1))
done

y=1
while test $y != 11
	do
	x=1
	while test $x != 6
		do
		python3.3 script_benchmark_mongo_2.py -db $x &>> out_mongo${x}.log
		x=$(($x + 1))
	done
	y=$(($y + 1))
done

w=1
while test $w != 11
	do
	v=1
	while test $v != 6
		do
		python3.3 script_benchmark_mongo.py -db $v &>> out_mongo_list${v}.log
		v=$(($v + 1))
	done
	w=$(($w + 1))
done
