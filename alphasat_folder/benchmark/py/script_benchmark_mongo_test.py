import time
import random
import argparse
import ORM
import numpy as np

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-db", help="database", type=int)
args = parser.parse_args()

if args.db == 1:
	t = ORM.CONNECTOR['mongo'][ORM.DB1]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB1]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB1]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB1]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB1]['Family']
elif args.db == 2:
	t = ORM.CONNECTOR['mongo'][ORM.DB2]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB2]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB2]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB2]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB2]['Family']
elif args.db == 3:
	t = ORM.CONNECTOR['mongo'][ORM.DB3]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB3]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB3]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB3]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB3]['Family']
elif args.db == 4:
	t = ORM.CONNECTOR['mongo'][ORM.DB4]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB4]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB4]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB4]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB4]['Family']
elif args.db == 5:
	t = ORM.CONNECTOR['mongo'][ORM.DB5]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB5]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB5]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB5]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB5]['Family']
	
#Requete 1
print("########################## NOUVEAU TEST ##########################")
for t1 in range(2):
	A1 = time.time()
	A2 = time.clock()
	monomers = list(m.find({}))
	print(str(len(monomers)))
	B1 = time.time()
	B2 = time.clock()
	print("Temps total de la requete 1 iteration %i : %f" %(t1,B1 - A1))
	print("Temps CPU de la requete 1 iteration %i : %f" %(t1,B2 - A2))


#Requete 2
for t2 in range(2):
	C1 = time.time()
	C2 = time.clock()
	chromosomes = list(c.find({}))
	print(str(len(chromosomes)))
	D1 = time.time()
	D2 = time.clock()
	print("Temps total de la requete 2 iteration %i : %f" %(t2,D1 - C1))
	print("Temps CPU de la requete 2 iteration %i : %f" %(t2,D2 - C2))

#Requete 3
#Selection 1 element
i = random.randint(0, len(chromosomes) - 1)
for t3 in range(2):
	E1 = time.time()
	E2 = time.clock()
	chromosomes_id = list(c.find( { '_id' : chromosomes[i]['_id'] } ))
	print(str(len(chromosomes_id)))
	F1 = time.time()
	F2 = time.clock()
	print("Temps total de la requete 3 iteration %i : %f" %(t3,F1 - E1))
	print("Temps CPU de la requete 3 iteration %i : %f" %(t3,F2 - E2))

taxons = list(t.find({}))
for t4 in range(2):
	clock_time = 0
	total_time = 0
	for x in taxons:
		G1 = time.time()
		G2 = time.clock()
		taxons_id = list(t.find( {'name' : x['name'] } ) )
		print(taxons_id[0]['_id'])
		print(str(len(taxons_id)))
		res = list(c.find( { "$and" : [ { '_id' : chromosomes[i]['_id'] } ] } , { 'taxon_id' : x['_id'] } ))
		print(res)
		print(str(len(res)))
		results = list(c.find( { "$and" : [ { '_id' : chromosomes[i]['_id'] } ] } , { 'taxon_id' : taxons_id[0]['_id'] } ))
		print(results)
		print(str(len(results)))
		print(str(len(results)))
		H1 = time.time()
		H2 = time.clock()
		clock_time += (H1 - G1)
		total_time += (H2 - G2)
	print("Temps total de la requete 3bis iteration %i : %f" %(t4,total_time))
	print("Temps CPU de la requete 3bis iteration %i : %f" %(t4,clock_time))

#Requete 4
#Selection de 1000 elements
random = np.random.choice((len(chromosomes) - 1),1000,replace=False)
random = random.tolist()
random_list = [chromosomes[j]['_id'] for j in random]
#print(len(random_list))
#for t5 in range(1000):
	#j = random.randint(0, len(chromosomes) - 1)
	#random_list.append(chromosomes[j]['_id'])
for t6 in range(2):
	I1 = time.time()
	I2 = time.clock()
	chromosomes_id2 = list(c.find( { '_id' : { "$in" : random_list } } ))
	#print("toto")
	print(str(len(chromosomes_id2)))
	J1 = time.time()
	J2 = time.clock()
	print("Temps total de la requete 4 iteration %i : %f" %(t6, J1 - I1))
	print("Temps CPU de la requete 4 iteration %i : %f" %(t6, J2 - I2))

for t7 in range(2):
	clock_time2 = 0
	total_time2 = 0
	for y in taxons:
		K1 = time.time()
		K2 = time.clock()
		taxons_id2 = list(t.find({'name' : y['name']}))
		print(str(len(taxons_id2)))
		aa = list(c.find( { '_id' : { "$in" : random_list} } ))
		print(str(len(aa)))
		aaa = list(c.find( {'Taxon_id' : taxons_id2[0]['_id'] } ))
		print(str(len(aaa)))
		results2 = list(c.find( { '_id' : { "$in" : random_list} , 'Taxon_id' : taxons_id2[0]['_id'] } ) )
		#print(results2)
		res = list(c.find( { '_id' : { "$in" : random_list} , 'Taxon_id' : y['_id'] } ) )
		#print(res)
		#print(str(len(results2)))
		L1 = time.time()
		L2 = time.clock()
		clock_time2 += (L1 - K1)
		total_time2 += (L2 - K2)
	print("Temps total de la requete 4bis iteration %i : %f" %(t7, total_time2))
	print("Temps CPU de la requete 4bis iteration %i : %f" %(t7, clock_time2))

#Requete 5
#Selection de 10000 elements
#random_list2 = list()
#for t8 in range(10000):
	#k = random.randint(0, len(chromosomes) - 1)
	#random_list2.append(chromosomes[k]['_id'])
random2 = np.random.choice((len(chromosomes) - 1),10000,replace=False)
random2 = random2.tolist()
random_list2 = [chromosomes[k]['_id'] for k in random2]
for t9 in range(2):
	M1 = time.time()
	M2 = time.clock()
	chromosomes_id3 = list(c.find( { '_id' : { "$in" : random_list2 } } ))
	print(str(len(chromosomes_id3)))
	N1 = time.time()
	N2 = time.clock()
	print("Temps total de la requete 5 iteration %i : %f" %(t9, N1 - M1))
	print("Temps CPU de la requete 5 iteration %i : %f" %(t9, N2 - M2))

for t10 in range(2):
	clock_time3 = 0
	total_time3 = 0
	for z in taxons:
		O1 = time.time()
		O2 = time.clock()
		taxons_id3 = list(t.find( { 'name' : z['name'] } ))
		print(str(len(taxons_id3)))
		results3 = list(c.find( { '_id' : { "$in" : random_list2 } , 'Taxon_id' : taxons_id3[0]['_id'] } ) )
		#f = c.find( { "$and" : [ { 'taxon_id' : taxons_id3[0]['_id'] } , { '_id' : { "$in" : random_list2 } } ] } )
		print(str(len(results3)))
		P1 = time.time()
		P2 = time.clock()
		clock_time3 += (P1 - O1)
		total_time3 += (P2 - O2)
	print("Temps total de la requete 5bis iteration %i : %f" %(t10,total_time3))
	print("Temps CPU de la requete 5bis iteration %i : %f" %(t10, clock_time3))
