import time
import random
import psycopg2
import psycopg2.extras
import sys
import numpy as np

#Connexion a la base de donnees
try:
	conn = psycopg2.connect(database = sys.argv[1], user="testuser", password="testuser")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

#Requete 5
#Selection de 10000 elements

for t10 in range(2):
	O1 = time.time()
	O2 = time.clock()
	cursor.execute("select taxon_name,chromosome_name,block_name,monomer_name from Taxon LEFT OUTER JOIN Chromosome ON Taxon.taxon_id = Chromosome.taxon_id LEFT OUTER JOIN Block ON Chromosome.chromosome_id = Block.chromosome_id LEFT OUTER JOIN Monomer ON Block.block_id = Monomer.block_id WHERE Taxon.taxon_id = '1' ;")
	for i in cursor:
		a = 0
	P1 = time.time()
	P2 = time.clock()
	print("Temps total de la requete 5bis iteration %i : %f" %(t10,P1-O1))
	print("Temps CPU de la requete 5bis iteration %i : %f" %(t10,P2-O2))
	cursor.execute("commit")
	
for t11 in range(2):
	block = list()
	chro_list = list()
	block_list = list()
	mono_list = list()
	A1 = time.time()
	A2 = time.clock()
	cursor.execute("select taxon_id from Taxon WHERE taxon_id = '1' ;")
	for i in cursor:
		tx = i['taxon_id']
	cursor.execute("select chromosome_id from Chromosome WHERE taxon_id = %s;",(tx,))
	chro = cursor.fetchall()
	C1 = time.time()
	C2 = time.clock()
	for i in chro:
		cursor.execute("select block_id from Block WHERE chromosome_id = %s;",(i['chromosome_id'],))
		block.extend(cursor.fetchall())
	D1 = time.time()
	D2 = time.clock()
	E1 = time.time()
	E2 = time.clock()
	for i in chro:
		chro_list.append(i['chromosome_id'])
	cursor.execute("select block_id from Block WHERE chromosome_id IN %s;",(tuple(chro_list),))
	block_list = cursor.fetchall()
	F1 = time.time()
	F2 = time.clock()
	for i in block_list:	
		cursor.execute("select monomer_id,monomer_name from Monomer WHERE block_id = %s;",(i['block_id'],))
		mono_list.append(cursor.fetchall())
	H1 = time.time()
	H2 = time.clock()	
	print("Temps total de la requete 5bis iteration %i : %f" %(t11,H1-A1))
	print("Temps CPU de la requete 5bis iteration %i : %f" %(t11,H2-A2))
	print("Temps total du test1 iteration %i : %f" %(t11,D1-C1))
	print("Temps CPU du test1 iteration %i : %f" %(t11,D2-C2))
	print("Temps total du test2 iteration %i : %f" %(t11,F1-E1))
	print("Temps CPU du test2 iteration %i : %f" %(t11,F2-E2))
	cursor.execute("commit")
