from ORM import *
from flask import *
import io
import json
import ORM
import pymongo
import ipdb
import gzip
import json
import base64
import re
import time
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
app = Flask(__name__)

t = ORM.CONNECTOR['mongo'][ORM.DB]['Taxon']
c = ORM.CONNECTOR['mongo'][ORM.DB]['Chromosome']
b =  ORM.CONNECTOR['mongo'][ORM.DB]['Block']
m = ORM.CONNECTOR['mongo'][ORM.DB]['Monomer']
f = ORM.CONNECTOR['mongo'][ORM.DB]['Family']

@app.route("/")
def hello():
	return ("Hello Loic !!!!")
	
@app.route("/list/Taxon", methods=['POST', 'GET'])
#affiche tous les taxons de la base
def taxon_all():
	T1 = time.time()
	T2 = time.clock()
	results =list(t.find({}))
	resultsf = dict()
	taxon_name = list()
	for i in results:
		taxon_name.append(i['name'])
		resultsf[i['name']] =  i['_id']
	taxon_encode = dumps(resultsf)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction list/Taxon : %f" %(FT1 - T1))
	print("Temps CPU de la fonction list/Taxon : %f" %(FT2 - T2))
	return render_template("taxon_selection.html", taxon_name = taxon_name, taxon_dict = taxon_encode)

def hidden_encode(data):
	return base64.b64encode(json.dumps(data).encode('utf-8')).decode('ascii')
	
def hidden_decode(text):
	return json.loads(base64.b64decode(text).decode('utf-8'))

@app.route("/taxons/chromosomes", methods=['POST', 'GET'])
def taxon_chromosomes():
	T1 = time.time()
	T2 = time.clock()
	#Noms des taxons
	list_taxon_name = request.form.getlist('Taxons')
	#Liste des identifiants des taxons
	taxon_list = request.form.getlist('taxon_dict')[0]
	#Dictionnaire des noms des taxons et de leurs identifiants
	taxon_decode = loads(taxon_list)
	chromosomes_list = dict()
	family_name = list()
	family = dict()
	chromosome_name_id = dict()
	taxon_dict2 = dict()
   
	t1 = time.time()
	#Identifiant du taxon selectionne
	taxon_id = [taxon_decode[name] for name in list_taxon_name]
	#Requetes liste des chromosomes avec l'identifiant du taxon selectionne
	##
	chromosomes = list(c.find( {'Taxon_id' : {"$in" : taxon_id} }))
	#print(chromosomes)
	#Dictionnaire des noms et identifiants des chromosomes
	chromosomes_id = {d['name'] : d['_id'] for d in chromosomes}
	#Liste des identifiants des chromosomes
	all_chromosomes_id = [d['_id'] for d in chromosomes]
	
	#Dictionnaire des noms des taxons et de leurs identifiants
	dict2 = {str(taxon_decode[d]) : d for d in taxon_decode}
	#print(dict2)
	#Dictionnaire des identifiants des taxons : noms des chromosomes
	#testchromosome_list = {"%s : %s" %(dict2[str(d['Taxon_id'])],d['name']) : d['_id'] for d in chromosomes}
	#Modifications:
	#Liste de tri croissant des caracteres ASCII des clés du dictionnaire
	#testchr_list = sorted(testchromosome_list)
	#Dictionnaire des noms des chromosomes (nomEspece+nomChro) et des idenfiants des chromosomes
	chromosome_list = {"%s : %s" %(dict2[str(d['Taxon_id'])], d['name']) : d['_id']for d in chromosomes}
	#23h47 super mongo!!! on passe de 7s avec une boucle et 0,0999593734741211 avec une requête mongo bien ficelée!!
	##
	family_id = list(f.find({'Chromosome_ids' : {"$elemMatch" : {'Chromosome_id' : {"$in" : all_chromosomes_id} }}}))
	if not isinstance(family_id,list):
		family_name = [d for d in family_id]
		family_name = list({v['_id']:v for v in family_name}.values())
		family_name_id = {str(d['_id']) : d['name'] for d in family_id}
		for g in family_name : 
			family[g['name']] = g['_id']
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction /taxons/chromosomes : %f" %(FT1 - T1))
	print("Temps CPU de la fonction /taxons/chromosomes : %f" %(FT2 - T2))
	return render_template("taxons_chromosomes_test.html", chromosome = chromosome_list, 
	family = family, list_taxon = taxon_id, chro_dict = dumps(all_chromosomes_id))

def gen_chromosome_id_list(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	chromosome_id = list()
	for name in chromosome_list:
		(taxon_name, chromosome_name) = name.split(" : ")
		##
		taxon_id = list(t.find({'name' : taxon_name}))
		taxon_id = taxon_id[0]['_id']
		##
		v = list(c.find({'name' : chromosome_name, 'Taxon_id' : taxon_id}))[0]['_id']
		chromosome_id.append(v)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction gen_chromosome_id_list() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction gen_chromosome_id_list() : %f" %(FT2 - T2))
	return chromosome_id
   
def find_blocks(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	g = list(b.find({'Chromosome_id' : {"$in" : chromosome_list} }))
	blocks = [d['_id'] for d in g] 
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction find_blocks() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction find_blocks() : %f" %(FT2 - T2))
	return blocks

def find_monomers(chromosome_list):
	##
	v = list(m.find({'Chromosome_id' : {"$in" : chromosome_list}}))
	monomers = [d['_id'] for d in v]
	return monomers
	
@app.route("/taxons/filters", methods=['POST', 'GET'])
def filters():
	T1 = time.time()
	T2 = time.clock()
	chromosome_list = request.form.getlist('Chromosomes')
	family_list = request.form.getlist('Family')
	base_begin = request.form.getlist('base_pair_begin')[0]
	base_end = request.form.getlist('base_pair_end')[0]
	distance_centro = request.form.getlist('distance_from_centromer')[0]
	block_name = request.form.getlist('textarea')[0]
	taxon_list = request.form.getlist('list_taxon')
	all_chromosomes_id = loads(request.form.getlist('list_chromosomes')[0])
	chromosome_id = list()
	monomers = list()
	blocks = list()
	family_name_show = list()
	
	##Conditions si les differents champs sont remplis##
	
	if not isinstance(base_end,str):
		b_end = int(base_end)
	else:
		b_end = 4000000000
	if not isinstance(base_begin,str):
		b_begin = int(base_begin)
	else:
	   b_begin = 0
	if not isinstance(distance_centro,str):
		distance_centro = int(distance_centro)
	#else:
		
		#distance_centro = 4000000000


	if isinstance(chromosome_list,list):
		chromosome_list = [ObjectId(d) for d in chromosome_list]
	else:
		chromosome_list = all_chromosomes_id
		chromosome_list = [str(d) for d in chromosome_list]
		chromosome_name = list()
		chromosome_list = [ObjectId(d) for d in chromosome_list]
		#print(chromosome_list)
		#Tri de la liste de chromosomes selectionnes
		chromosome_list2 = sorted(chromosome_list)
		##
	monomerstot = m.find({'Chromosome_id' : {"$in" : all_chromosomes_id} }).count()
	##
	blockstot =  b.find({'Chromosome_id' : {"$in" : all_chromosomes_id} }).count()
	##
	v = list(m.find( {'Chromosome_id' : {'$in': chromosome_list}, 'begin' : {"$gte" : b_begin} , 'end' : {"$lte" : b_end} }))
	monomers = [d['_id'] for d in v]
	blocks = find_blocks(chromosome_list)
				
	if not isinstance(block_name,str):
		monomers2 = []
		block_list = block_name.split("\r\n")
		for block_name in block_list:
			##
			blocks_id = list(b.find({'name' : block_name}))[0]['_id']
			##
			v = list(m.find({'block_id' : ObjectId(blocks_id)}))
			monomers2 = monomers2 + v
		monomers2 = [d['_id'] for d in monomers2]
		monomers = [element for element in monomers if element in monomers2 ]
		block_name = block_list
	monomers_number = len(monomers)
	blocks_number = len(blocks)
	monomers_list_encode = dumps([d for d in monomers])
	blocks_list_encode = dumps([d for d in blocks])
	
	#Liste des identifiants des chromosomes selectionnes
	##
	chromosome_list_to_name = list(c.find({'_id' : {"$in" : chromosome_list} }))
	#Liste des noms des chromosomes selectionnes
	chromosome_name_show = [d['name'] for d in chromosome_list_to_name]
	#Modifications:
	#Tri des caracteres ASCII des noms des chromosomes par ordre croissant
	chromosome_name_show = sorted(chromosome_name_show)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction /taxons/filters : %f" %(FT1 - T1))
	print("Temps CPU de la fonction /taxons/filters : %f" %(FT2 - T2))
	#list_id = [ObjectId("54c60789e9ab8810c667201f"),ObjectId("54c60789e9ab8810c6672020")]
	#m.find({'_id' : {"$in" : list_id} })
	####TEST
	#TEST = time.time()
	#TEST2 = time.clock()
	#i = 0
	#while i < 100:
		#monotot = m.find({'Chromosome_id' : {"$in" : all_chromosomes_id} }).count()
		#i += 1
	#FTEST = time.time()
	#FTEST2 = time.clock()
	#print("Temps total requete test : %f" %(FTEST - TEST))
	#print("Temps CPU requete test : %f" %(FTEST2 - TEST2))
	#count = 0
	#for i in monotot:
	#	count += 1
	#print(str(count))
	#print(monotot)
	####FIN DU TEST
	#print(monomerstot)
	#a2 = [i for i in a]
	#print(a2[0:10])
	#print("taille de a", len(a2))

	return render_template("show_filters.html", chromosomes = chromosome_list, family = family_list, monomers = monomers_number,
	monomerstot = monomerstot, blocks = blocks_number, blockstot = blockstot,
	monomers_list_encode = monomers_list_encode, blocks_list_encode = blocks_list_encode,
	base_begin = base_begin, base_end = base_end, distance_centro = distance_centro,
	block_name = block_name, family_name_show = family_name_show, chromosome_name_show = chromosome_name_show )

@app.route("/alphasat/accueil" , methods=['POST', 'GET'])
def accueil():
	return render_template("alphasat_accueil.html")

@app.route("/alphasat/contact", methods=['POST', 'GET'])
def contact():
	return render_template("contact.html")
	
@app.route("/alphasat/help_and_documentation", methods=['POST', 'GET'])
def documentation():
	return render_template("help_and_documentation.html")
	
if __name__ == "__main__":
	app.run(port=PORT, host="0.0.0.0", debug=True)
