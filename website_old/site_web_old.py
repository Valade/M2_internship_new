from flask import *
import io
import json
import ipdb
import gzip
import json
import base64
import re
import time
import psycopg2
import psycopg2.extras
from psycopg2.extensions import AsIs
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
from sort_functions import (atoi,natural_keys)
import svgwrite
import mimetypes
app = Flask(__name__)
PORT = 8000
from operator import itemgetter
"""Connexion a la base de donnees"""
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
taxon_id_list = []
seq_id_list = []
chromosome_id_list = []
block_id_list = []
block_id_selection_list = []
family_id_list = []
monomer_id_list = []
monomers_list = []
@app.route("/list/Taxon", methods=['POST', 'GET'])
def page():
	classification_list = list()
	try:
		cursor.execute("SELECT family_classification from Family;")
	except psycopg2.Error as e:
		print (e.pgerror)
	classification = cursor.fetchall()
	for i in classification:
		classification_list.append(i['family_classification'])
	return render_template("filter_page.html", classification_list = classification_list)

@app.route("/tree",methods=['GET'])
def tree():
	tables = ['taxon', 'chromosome', 'sequence']
	#taxonSelection, , , = getVariables(request.args)
	oid = request.args.get('id', None)
	result_taxons = dict()
	taxon_name = list()
	element_name = list()
	result_element = dict()
	dico_test = list()
	results2 = list()
	#Tree vide
	if oid is None:
		sequences_number = 0
		blocks_number = 0
		monomers_number = 0
		try:
			cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id IS NULL;")
		except psycopg2.Error as e:
			print (e.pgerror)
		results = cursor.fetchall()
		cursor.execute('commit') 		
		#Pas de supertaxon racine
		if not results:
			try:
				cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id IS NOT NULL;")
			except psycopg2.Error as e:
				print (e.pgerror)
			results = cursor.fetchall()
			cursor.execute('commit')
		for result in results:
			try:
				cursor.execute("SELECT number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id = %s;",(result['taxon_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			for i in cursor:
				if i[0]:
					result['number_sequences'] = int(result['number_sequences']) + int(i['number_sequences'])
					result['number_blocks'] = int(result['number_blocks']) + int(i['number_blocks'])
					result['number_monomers'] = int(result['number_monomers']) + int(i['number_monomers'])
			
			result_taxons[result['taxon_id']] =  [result['taxon_name'],result['number_sequences'],result['number_blocks'],result['number_monomers']]
			
		#Envoie des résultats
		for i in result_taxons:
			dico_test.append({'id':'taxon_'+str(i),'text':result_taxons[i][0]+" ( sequences : "+str(result_taxons[i][1])+" , blocks : "+str(result_taxons[i][2])+" , monomers : "+str(result_taxons[i][3])+" )","iconCls":"icon-blank", 'state':'closed'})
	#Tree non vide
	else:
		sequences_number = 0
		blocks_number = 0
		monomers_number = 0
		info = oid.split("_")
		table = info[0]
		table_id = info[1]
		if table in tables:
			k = tables.index(table)
			if k < (len(tables) - 1):
				childrentable = tables[k+1]
			#Condition taxon
			if table == "taxon":
				#Condition si taxon est un supertaxon
				try:
					cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon WHERE supertaxon_id = %s;",(table_id,))
				except psycopg2.Error as e:
					print (e.pgerror)
				results = cursor.fetchall()
				cursor.execute('commit')
				childrentable = "taxon"
				k = 0
				#Condition pas un supertaxon
				if not results:
					try:
						cursor.execute("SELECT chromosome_id from Sequence WHERE taxon_id = %s;",(table_id,))
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosomes_id = cursor.fetchall()
					for chromosome in chromosomes_id:
						try:
							cursor.execute("SELECT chromosome_name,number_sequences,number_blocks,number_monomers from Chromosome WHERE chromosome_id = %s;",(chromosome['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						results = cursor.fetchall()
						cursor.execute('commit')
						childrentable = "chromosome"
						k = 1			
						for i in results:
						#~ try:
							#~ cursor.execute("SELECT count(DISTINCT block_id) from Block WHERE sequence_id = %s;",(i["sequence_id"],))
						#~ except psycopg2.Error as e:
							#~ print (e.pgerror)
						#~ number_blocks = cursor.fetchone()
						#~ cursor.execute('commit')
						#~ try:
							#~ cursor.execute("SELECT count(DISTINCT monomer_id) from Monomer WHERE sequence_id = %s;",(i["sequence_id"],))
						#~ except psycopg2.Error as e:
							#~ print (e.pgerror)
						#~ number_monomers = cursor.fetchone()
						#~ cursor.execute('commit')											
							result_element[chromosome['chromosome_id']] =  [i['chromosome_name'].split("|")[0],i['number_sequences'], i['number_blocks'], i['number_monomers']]
					for i in result_element:
						dico_test.append({'id':childrentable+"_"+str(i),'text':result_element[i][0]+" ( sequences : "+str(result_element[i][1])+" , blocks : "+str(result_element[i][2])+" , monomers : "+str(result_element[i][3])+" )", "iconCls":"icon-blank",'state':'opened'}) #+" ( blocks: "+str(result_element[i][1])+" ,monomers: "+str(result_element[i][2])+")"
				else:
					for result in results:
						try:
							cursor.execute("SELECT number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id = %s;",(result['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						for i in cursor:
							if i[0]:
								result['number_sequences'] = int(result['number_sequences']) + int(i['number_sequences'])
								result['number_blocks'] = int(result['number_blocks']) + int(i['number_blocks'])
								result['number_monomers'] = int(result['number_monomers']) + int(i['number_monomers'])
						result_element[result['taxon_id']] =  [result['taxon_name'],result['number_sequences'],result['number_blocks'],result['number_monomers']]
					for i in result_element:
						dico_test.append({'id':childrentable+"_"+str(i),'text':result_element[i][0]+" ( sequences : "+str(result_element[i][1])+" , blocks : "+str(result_element[i][2])+" , monomers : "+str(result_element[i][3])+" )", "iconCls":"icon-blank",'state':'closed'})
	dico_test.sort(key=lambda k: natural_keys(k['text']))
	return json.dumps(dico_test)
	
@app.route("/search_form", methods=['POST', 'GET'])
def search():
	#Renvoie le formulaire
	return render_template("search.html")

@app.route("/results",methods=['GET','POST'])
def results_form():
	phase = request.args.get("_phase")
	list_encode = request.args.get("monomer_list")
	retrieve_type = "toto"
	return render_template("results.html", list_encode = list_encode, retrieve_type = retrieve_type, phase = phase)

def get_max_length(data):
	mx=0
	for chr in data:
		if data[chr]['length'] > mx:
			mx=data[chr]['length']
	return(mx)
	
@app.route("/results/files",methods=['GET','POST'])
def files_results():
	#parametres
	mimetypes.add_type('image/svg+xml', '.svg', True)
	mimetypes.add_type("image/svg+xml", ".svgz", True)
	global taxon_id_list
	global chromosome_id_list
	global monomer_id_list
	list_encode = monomer_id_list
	phase = request.form.get('monomer_phase',None)
	if isinstance(phase,int):
		phase = int(phase)
	else:
		phase = 1
	#variable
	monomer_selection = request.form.getlist("monomer_selection")
	print(monomer_selection)
	liste_monomer = []
	if monomer_selection[0]:
		liste_monomer = list(map(int,monomer_selection[0].rstrip('\n').split(',')))
	else:
		liste_monomer = list(map(int,monomer_id_list))
	format_output = request.form.getlist("format")[0]
	fasta_output = request.form.getlist("output")[0]
	csv_output = request.form.getlist("csv")[0]
	name_option = request.form.getlist("name_option")
	name_format = request.form.getlist("name_format")
	separator_format = request.form.getlist("separator_format")[0]
	results = io.StringIO()
	family_string = str()
	coll = {"monomers" : 'Monomer', "blocks" : 'Block'}
	mono_counter = 0
	block_counter = 0
	liste_name = list()
	##temp
	retrieve_type = "monomers"
	if coll[retrieve_type] == 'Monomer':
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				name = monomer_name(name_format,identifiant,separator_format)
				try:
					cursor.execute("SELECT monomer_index,monomer_dimere,monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				index = info['monomer_index']
				nucleotide = info['monomer_dimere']
				length = info['monomer_length']
				results.write(">%s\n" %name)		
				if phase == 1:
					try:
						cursor.execute("SELECT monomer_sequence FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_seq = cursor.fetchone()
					cursor.execute('commit')
					monomer_unique = monomer_seq['monomer_sequence']
					sequence2 = '\n'.join((monomer_unique)[pos:pos+60] for pos in range(0,len(monomer_unique), 60))
					results.write( "%s\n" %sequence2 )
				else:
					begin = None
					end = None
					ii = 0
					while ii < (len(index)-1) and index[ii] < phase:
						begin = ii + 1
						ii += 1
					jj = begin - 1
					while jj >= 0 and index[jj] <= 0:
						begin = jj
						jj -= 1
					yy = length
					while yy < (len(index)-1) and index[yy] < phase:
						end = yy
						yy += 1
					zz = end
					while zz >= 0  and index[zz] <= 0:
						end = zz
						zz -= 1
					monomer = nucleotide[begin:end+1]
					sequence2 = '\n'.join(monomer[pos:pos+60] for pos in range(0, len(monomer),60))
					results.write( "%s\n" %sequence2 )					
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
		elif format_output == 'format_csv':
			#Fichier CSV
			for identifiant in list_encode:
				name = monomer_name(name_format,identifiant,separator_format)
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				try:
					cursor.execute("SELECT monomer_dimere,monomer_begin,monomer_end,monomer_strand,sequence_id FROM Monomer WHERE monomer_id = %s ;", (identifiant,))
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(nucleotide,begin,end,strand,sequence_id) = info				
				try:
					cursor.execute("SELECT chromosome_id FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_id = cursor.fetchone()
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s ;",(chromosome_id[0],) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()			
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name[0], separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				"""if 'families' in monomer_info:
					family_results = set()
					for families_name in monomer_info['families']:
						family_results.add(families_name['name'])
					if len(family_results) == 1:
						family_string = str(family_results)
					else:
						family_string = '-'.join(family_results)
					results.write("%s%s" % (family_string, separators[csv_output]))"""
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
		elif format_output == 'format_svg':
			#Fichier SVG
			print(liste_monomer)
			nb_tx = len(taxon_id_list)
			family_id = list()
			family_dict = dict()
			family_dict["no family"] = [None,None,"pink"]
			color = ['blue','green','orange','purple','red']
			filename = "%s.svg" %retrieve_type
			width=18000 * nb_tx
			width_name = 5000
			heigh=6000 * nb_tx
			heigh_plus=1000
			svg_document = svgwrite.Drawing(filename = filename,size = (width+width_name, heigh+heigh_plus),profile='full')
			taxon_space = 0
			nb_tx = len(taxon_id_list)
			taxon_number = 1
			max_chro = 0
			for tx in taxon_id_list:
				#~ #print("tx")
				#~ #print(tx)
				try:
					cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				seq_id = cursor.fetchall()
				#~ #print("tailles")
				#~ #print(str(len(seq_id)))
				#~ #print(str(max_chro))
				if len(seq_id) > max_chro:
					max_chro = len(seq_id)
			for tx in taxon_id_list:
				print("taxon")
				print(tx)
				data={}
				monomer_data = {}
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				tx_name = cursor.fetchone()[0]
				try:
					cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				seq_id = cursor.fetchall()
				for i in seq_id:
					monomer_data[i['sequence_name']] = { 'monomers': [] }
					parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", i['sequence_name'])
					if parse:
						#print(i['sequence_name'])
						try:
							cursor.execute("SELECT sequence_cytoband FROM Sequence WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						cytoband_list = cursor.fetchone()
						try:
							cursor.execute("SELECT block_id FROM Block WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						blocks_id = cursor.fetchall()
						try:
							cursor.execute("SELECT monomer_id,monomer_begin_sequence,monomer_end_sequence,monomer_length FROM Monomer WHERE block_id = ANY(%s) ;",(blocks_id,) )
						except psycopg2.Error as e:
							print (e.pgerror)												
						monomers = cursor.fetchall()
						for j in monomers:							
							monomers_infos = {'monomer_id' : j['monomer_id'],'monomer_begin_sequence' : j['monomer_begin_sequence'],'monomer_end_sequence' : j['monomer_end_sequence'],'monomer_length' : j['monomer_length']}
							monomer_data[i['sequence_name']]['monomers'].append(monomers_infos)
						if cytoband_list[0] is not None:
							for row in cytoband_list['sequence_cytoband']:
								if row[0] not in data:
									data[row[0]]={'length': 0, 'bands': []}
								if row[0] in data:
									if int(row[2]) > int(data[row[0]]['length']):
										data[row[0]]['length']=int(row[2])
										bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
										data[row[0]]['bands'].append(bd)
				heigh_tx = (heigh / nb_tx)
				heigh_chr = heigh_tx * taxon_number
				heigh_plus_tx = (heigh_plus / nb_tx)
				nb_chr=len(data)
				#~ print("data")
				#~ print(data)
				#print(nb_chr)
				max_length = get_max_length(data)
				#X et Y
				#Fraction de la largeur pour chaque chromosome
				x=width/max_chro
				#Ratio nb de pixel par pb
				y=(heigh_tx - 500)/(max_length - 10000)
				nb=0
				g = svg_document.g(style="font-size:280;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:5;fill:none")
				g.add(svg_document.text(tx_name,
												insert = (200, heigh_chr - 500),
												fill = 'black'
												))
				chromosomes = list(data.keys())
				chromosomes.sort(key=lambda k: natural_keys(k))
				for chro in chromosomes:
					ins=((nb*x) + 5000,  (heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y)
					siz=(x/2, data[chro]['length']*y)
					#Ajouter un element
					a = svg_document.g(style="font-size:240;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
					g.add(svg_document.text(chro[3:],
													insert = ((nb*x + x/8) + 5000, heigh_chr + 400),
													fill = 'black'
													))
					svg_document.add(g)
					st=(heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y
					for bd in data[chro]['bands']:
						ins=((nb*x) + 5000, st+bd['start']*y)
						siz=(x/2, (bd['end']-bd['start'])*y)
						col="rgb(255,255,255)"
						if bd['type'] == 'gneg':
							#blanc
							col="rgb(255,255,255)"
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos100':
							#noir
							col='rgb(0, 0, 0)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos75':
							#gris foncé
							col='rgb(169, 169, 169)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos50':
							#gris clair
							col='rgb(192, 192, 192)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gpos25':
							#gris très clair
							col='rgb(211, 211, 211)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'acen':
							#noir
							col='rgb( 0, 0, 0)'
							#Ajouter un rectangle pour les bandes
							ins=(((nb*x)+((x/2)*(1/4)))+ 5000, st+bd['start']*y)
							siz = (x/4, (bd['end']-bd['start'])*y)
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'gvar':
							#blanc
							col='rgb( 255, 255, 255)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
						elif bd['type'] == 'stalk':
							#rouge
							col='rgb( 220, 220, 220)'
							#Ajouter un rectangle pour les bandes
							svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = "black",
																fill = col))
					for seq in monomer_data:
						if seq == chro:
							for mono in monomer_data[seq]['monomers']:
								if liste_monomer:
									print("coucou")
									if mono['monomer_id'] in liste_monomer:
										print("hello")
										family_id = []
										try:
											cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
										except psycopg2.Error as e:
											print (e.pgerror)												
										family = cursor.fetchall()
										if family:
											for i in family:
												if i['family_id'] not in family_id:
													family_id.append(i['family_id'])
											try:
												cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
											except psycopg2.Error as e:
												print (e.pgerror)										
											family_infos = cursor.fetchall()
											print(family_dict)								
											if family_infos:
												for i in family_infos:
													if i['family_id'] not in family_dict.keys():
														family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
														del color[0]
										else:
											family_id.append("no family")
										ins=((nb*x)+ 5000, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
										siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*1000)
										svg_document.add(svg_document.rect(insert = ins,
																		size = siz,
																		stroke_width = "1",
																		stroke = family_dict[family_id[0]][2],
																		fill = family_dict[family_id[0]][2]))
								else:
									family_id = []
									try:
										cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
									except psycopg2.Error as e:
										print (e.pgerror)												
									family = cursor.fetchall()
									if family:
										for i in family:
											if i['family_id'] not in family_id:
												family_id.append(i['family_id'])
										try:
											cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
										except psycopg2.Error as e:
											print (e.pgerror)										
										family_infos = cursor.fetchall()								
										if family_infos:
											for i in family_infos:
												if i['family_id'] not in family_dict.keys():
													family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
													del color[0]
									else:
										family_id.append("no family")
									ins=((nb*x)+ 5000, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
									siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*1000)
									svg_document.add(svg_document.rect(insert = ins,
																	size = siz,
																	stroke_width = "1",
																	stroke = family_dict[family_id[0]][2],
																	fill = family_dict[family_id[0]][2]))									
					nb=nb+1
				taxon_number += 1
			svg_document.save()
			###fin
			svg_file = svg_document.tostring()
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response = Response(svg_file)		
			response.headers.add('Content-type', 'image/svg+xml')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	elif coll[retrieve_type] == Block:
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				try:
					cursor.execute("SELECT block_name,block_nucleotide FROM Block WHERE block_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(name,nucleotide) = info
				results.write(">%s\n" %name)
				sequence2 = '\n'.join((nucleotide)[pos:pos+60] for pos in range(0, len(nucleotide), 60))
				results.write( "%s\n" %sequence2 )
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		else:
			for identifiant in list_encode:
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				if name_format == "database_name":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(name,nucleotide,begin,end,strand,sequence_id) = info
				elif name_format == "chr_begin_end_strand":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(block_name,nucleotide,begin,end,strand,sequence_id) = info
					try:
						cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosome_name = cursor.fetchone()
					cursor.execute('commit')
					name = chromosome_name[0]+"_"+str(begin)+"_"+str(end)+"_"+str(strand)			
				elif name_format == "simple_numbering":
					try:
						cursor.execute("SELECT block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,begin,end,strand,sequence_id) = info
					block_counter += 1
					name = str(block_counter)
				try:
					cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name, separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	return response

@app.route("/caryotype", methods=['POST', 'GET'])
def caryotype():
	mimetypes.add_type('image/svg+xml', '.svg', True)
	mimetypes.add_type("image/svg+xml", ".svgz", True)
	global taxon_id_list
	global monomer_id_list
	#variable
	monomer_selection = request.form.getlist("monomer_selection")
	print(monomer_selection)
	liste_monomer = []
	if monomer_selection[0]:
		liste_monomer = list(map(int,monomer_selection[0].rstrip('\n').split(',')))
	else:
		liste_monomer = list(map(int,monomer_id_list))
	print(liste_monomer)
	nb_tx = len(taxon_id_list)
	family_id = list()
	family_dict = dict()
	family_dict["no family"] = [None,None,"pink"]
	color = ['blue','green','orange','purple','red']
	retrieve_type = "monomer"
	filename = "%s.svg" %retrieve_type
	width=1800 * nb_tx
	width_name = 500
	heigh=600 * nb_tx
	heigh_plus=100
	svg_document = svgwrite.Drawing(filename = filename,size = (width+width_name, heigh+heigh_plus),profile='full')
	taxon_space = 0
	nb_tx = len(taxon_id_list)
	taxon_number = 1
	max_chro = 0
	for tx in taxon_id_list:
		#~ #print("tx")
		#~ #print(tx)
		try:
			cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		seq_id = cursor.fetchall()
		#~ #print("tailles")
		#~ #print(str(len(seq_id)))
		#~ #print(str(max_chro))
		if len(seq_id) > max_chro:
			max_chro = len(seq_id)
	for tx in taxon_id_list:
		print("taxon")
		print(tx)
		data={}
		monomer_data = {}
		try:
			cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s ;",(tx,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		tx_name = cursor.fetchone()[0]
		try:
			cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		seq_id = cursor.fetchall()
		for i in seq_id:
			monomer_data[i['sequence_name']] = { 'monomers': [] }
			parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", i['sequence_name'])
			if parse:
				#print(i['sequence_name'])
				try:
					cursor.execute("SELECT sequence_cytoband FROM Sequence WHERE sequence_id = %s ;",(i['sequence_id'],) )
				except psycopg2.Error as e:
					print (e.pgerror)
				cytoband_list = cursor.fetchone()
				try:
					cursor.execute("SELECT block_id FROM Block WHERE sequence_id = %s ;",(i['sequence_id'],) )
				except psycopg2.Error as e:
					print (e.pgerror)
				blocks_id = cursor.fetchall()
				try:
					cursor.execute("SELECT monomer_id,monomer_begin_sequence,monomer_end_sequence,monomer_length FROM Monomer WHERE block_id = ANY(%s) ;",(blocks_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)												
				monomers = cursor.fetchall()
				for j in monomers:							
					monomers_infos = {'monomer_id' : j['monomer_id'],'monomer_begin_sequence' : j['monomer_begin_sequence'],'monomer_end_sequence' : j['monomer_end_sequence'],'monomer_length' : j['monomer_length']}
					monomer_data[i['sequence_name']]['monomers'].append(monomers_infos)
				if cytoband_list[0] is not None:
					for row in cytoband_list['sequence_cytoband']:
						if row[0] not in data:
							data[row[0]]={'length': 0, 'bands': []}
						if row[0] in data:
							if int(row[2]) > int(data[row[0]]['length']):
								data[row[0]]['length']=int(row[2])
								bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
								data[row[0]]['bands'].append(bd)
		heigh_tx = (heigh / nb_tx)
		heigh_chr = heigh_tx * taxon_number
		heigh_plus_tx = (heigh_plus / nb_tx)
		nb_chr=len(data)
		#~ print("data")
		#~ print(data)
		#print(nb_chr)
		max_length = get_max_length(data)
		#X et Y
		#Fraction de la largeur pour chaque chromosome
		x=width/max_chro
		#Ratio nb de pixel par pb
		y=(heigh_tx - 50)/(max_length - 1000)
		nb=0
		g = svg_document.g(style="font-size:24;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:2;fill:none")
		g.add(svg_document.text(tx_name,
										insert = (20, heigh_chr - 50),
										fill = 'black'
										))
		chromosomes = list(data.keys())
		chromosomes.sort(key=lambda k: natural_keys(k))
		for chro in chromosomes:
			ins=((nb*x) + 500,  (heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y)
			siz=(x/2, data[chro]['length']*y)
			#Ajouter un element
			a = svg_document.g(style="font-size:20;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
			g.add(svg_document.text(chro[3:],
											insert = ((nb*x + x/8) + 500, heigh_chr + 40),
											fill = 'black'
											))
			svg_document.add(g)
			st=(heigh_chr) - data[chro]['length']*y+0.5*heigh_plus*y
			for bd in data[chro]['bands']:
				ins=((nb*x) + 500, st+bd['start']*y)
				siz=(x/2, (bd['end']-bd['start'])*y)
				col="rgb(255,255,255)"
				if bd['type'] == 'gneg':
					#blanc
					col="rgb(255,255,255)"
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos100':
					#noir
					col='rgb(0, 0, 0)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos75':
					#gris foncé
					col='rgb(169, 169, 169)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos50':
					#gris clair
					col='rgb(192, 192, 192)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gpos25':
					#gris très clair
					col='rgb(211, 211, 211)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'acen':
					#noir
					col='rgb( 0, 0, 0)'
					#Ajouter un rectangle pour les bandes
					ins=(((nb*x)+((x/2)*(1/4)))+ 500, st+bd['start']*y)
					siz = (x/4, (bd['end']-bd['start'])*y)
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'gvar':
					#blanc
					col='rgb( 255, 255, 255)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
				elif bd['type'] == 'stalk':
					#rouge
					col='rgb( 220, 220, 220)'
					#Ajouter un rectangle pour les bandes
					svg_document.add(svg_document.rect(insert = ins,
														size = siz,
														stroke_width = "1",
														stroke = "black",
														fill = col))
			for seq in monomer_data:
				if seq == chro:
					for mono in monomer_data[seq]['monomers']:
						if liste_monomer:
							print("coucou")
							if mono['monomer_id'] in liste_monomer:
								print("hello")
								family_id = []
								try:
									cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
								except psycopg2.Error as e:
									print (e.pgerror)												
								family = cursor.fetchall()
								if family:
									for i in family:
										if i['family_id'] not in family_id:
											family_id.append(i['family_id'])
									try:
										cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
									except psycopg2.Error as e:
										print (e.pgerror)										
									family_infos = cursor.fetchall()
									print(family_dict)								
									if family_infos:
										for i in family_infos:
											if i['family_id'] not in family_dict.keys():
												family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
												del color[0]
								else:
									family_id.append("no family")
								ins=((nb*x)+ 500, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
								siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*1000)
								svg_document.add(svg_document.rect(insert = ins,
																size = siz,
																stroke_width = "1",
																stroke = family_dict[family_id[0]][2],
																fill = family_dict[family_id[0]][2]))
								
						else:
							family_id = []
							try:
								cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s ;",(mono['monomer_id'],) )
							except psycopg2.Error as e:
								print (e.pgerror)												
							family = cursor.fetchall()
							if family:
								for i in family:
									if i['family_id'] not in family_id:
										family_id.append(i['family_id'])
								try:
									cursor.execute("SELECT family_id,family_name,family_classification FROM Family WHERE family_id = ANY(%s) ;",(family_id,) )
								except psycopg2.Error as e:
									print (e.pgerror)										
								family_infos = cursor.fetchall()								
								if family_infos:
									for i in family_infos:
										if i['family_id'] not in family_dict.keys():
											family_dict[i['family_id']] = [i['family_name'],i['family_classification'],color[0]]
											del color[0]
							else:
								family_id.append("no family")
							ins=((nb*x)+ 500, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
							siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*100)
							svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = family_dict[family_id[0]][2],
															fill = family_dict[family_id[0]][2]))
							a = svg_document.g(style="font-size:20;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
							g.add(svg_document.text('*',
											insert = (((nb*x)+ 500) - 5, (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y)),
											fill = 'black'
											))
			nb=nb+1
		taxon_number += 1
	svg_document.save()
	###fin
	svg_file = svg_document.tostring()
	"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
	response = Response(svg_file)		
	response.headers.add('Content-type', 'image/svg+xml')
	response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	return render_template("caryotype.html", svg = svg_file)
@app.route("/datagrid_sequences", methods=['POST', 'GET'])
def sequences_data():
	global taxon_id_list
	global seq_id_list
	global chromosome_id_list
	seq_id_list = []
	selection = request.args.getlist('selection[]')
	search = request.args.get('search')
	liste_chr = list()
	liste_taxon = list()
	seq_id = []
	taxon_list = []
	for i in selection:
		info = i.split("_")
		if info[0] == "chromosome":
			liste_chr.append(info[1])
		elif info[0] == "taxon":
			liste_taxon.append(info[1])
	liste_chr = list(map(int,liste_chr))
	liste_taxon = list(map(int,liste_taxon))
	taxon_id_list = liste_taxon
	chromosome_id_list = liste_chr
	for j in liste_chr:
		try:
			cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(j,))
		except psycopg2.Error as e:
			print (e.pgerror)
		chr_name = cursor.fetchone()
		try:
			cur.execute("SELECT sequence_name,sequence_id,taxon_id,chromosome_id,sequence_length,number_blocks,number_monomers FROM Sequence WHERE chromosome_id = %s;",(j,))
		except psycopg2.Error as e:
			print (e.pgerror)
		identifiant = cur.fetchall()
		for i in identifiant:
			i['chromosome_name'] = chr_name[0].split("|")[0]
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(i['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			i['taxon_name'] = taxon_name[0]
			cursor.execute('commit')
			seq_id.append(i)
			seq_id_list.append(i['sequence_id'])
			if i['taxon_id'] not in taxon_list:
				taxon_list.append(i['taxon_id'])
		#~ print(seq_id)
	for k in liste_taxon:
		supertaxon = False
		if k not in taxon_list:
			try:
				cursor.execute("SELECT taxon_id FROM Taxon WHERE supertaxon_id = %s;",(k,))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_id = cursor.fetchall()
			for tx in taxon_id:
				if tx['taxon_id'] in liste_taxon:
					supertaxon = True
			if not supertaxon:
				sequences_list = []
				subtaxon_id = []
				subtaxon_id.append({'taxon_id':info[1]})
				subtaxon_id.extend(taxon_id)
				while taxon_id:
					for i in taxon_id:
						try:
							cursor.execute("SELECT taxon_id FROM Taxon WHERE supertaxon_id = %s;",(int(i['taxon_id']),))
						except psycopg2.Error as e:
							print (e.pgerror)
						taxon_id = cursor.fetchall()
						subtaxon_id.extend(taxon_id)
				if subtaxon_id:
					for i in subtaxon_id:
						try:
							cur.execute("SELECT sequence_name,sequence_id,chromosome_id,sequence_length,number_blocks,number_monomers FROM Sequence WHERE taxon_id = %s;",(int(i['taxon_id']),))
						except psycopg2.Error as e:
							print (e.pgerror)
						identifiant = cur.fetchall()
						sequences_list.extend(identifiant)
				else:
					try:
						cur.execute("SELECT sequence_name,sequence_id,chromosome_id,sequence_length,number_blocks,number_monomers FROM Sequence WHERE taxon_id = %s;",(k,))
					except psycopg2.Error as e:
						print (e.pgerror)
					identifiant = cur.fetchone()
					sequences_list.extend(identifiant)				
				for i in sequences_list:
					try:
						cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(i['chromosome_id']),))
					except psycopg2.Error as e:
						print (e.pgerror)
					chr_name = cursor.fetchone()
					i['chromosome_name'] = chr_name['chromosome_name'].split("|")[0]
					try:
						cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(k,))
					except psycopg2.Error as e:
						print (e.pgerror)
					taxon_name = cursor.fetchone()
					print("taxon_name")
					print(taxon_name)
					i['taxon_name'] = taxon_name['taxon_name']
					seq_id_list.append(i['sequence_id'])
					seq_id.append(i)
	print(seq_id)
	return json.dumps(seq_id)
	

@app.route("/datagrid_blocks", methods=['POST', 'GET'])
def blocks_data():
	global seq_id_list
	global block_id_list
	block_id_list = []
	selection = request.args.getlist('selection[]')
	block_length = request.args.get('search[block_length]')
	block_list = list()
	length = None
	length_max = None
	length_min = None
	seq_id = []
	#Taille des blocks
	if block_length:
		monomers = []
		list_length = block_length.rstrip("\n").split(":")
		if block_length.isdigit():
			length = int(block_length)
		elif len(list_length) == 2:
			if list_length[0].isdigit():
				length_min = int(list_length[0])
			if list_length[1].isdigit():
				length_max = int(list_length[1])
	if not selection:
		selection = list(map(int,seq_id_list))
		
	else:
		selection = list(map(int,selection))
	if length:
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length = %s;",(idt,length,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0].split("|")[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	elif length_min and length_max:
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length >= %s AND block_length <= %s;",(idt,length_min,length_max,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0].split("|")[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	elif length_min and not length_max:
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length >= %s;",(idt,length_min,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0].split("|")[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	elif not length_min and length_max:
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length <= %s;",(idt,length_max,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0].split("|")[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	else:
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			j = 0
			for i in seq_id:
				j +=1
				i['chromosome_name'] = chr_name[0].split("|")[0]
				i['sequence_name'] = sequence_result['sequence_name']
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])

			block_list.extend(seq_id)
	return json.dumps(block_list)

@app.route("/datagrid_families", methods=['POST', 'GET'])
def families_data():
	global block_id_list
	global family_id_list
	family_id_list = []
	monomers_id = []
	selection = request.args.getlist('selection[]')
	seq_id = []
	families_id = []
	if not selection:
		selection = list(map(int,block_id_list))
	else:
		selection = list(map(int,selection))
	for block in selection:
		try:
			cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id = %s;",(block,))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomers = cursor.fetchall()
		cursor.execute('commit')
		for monomer in monomers:
			monomers_id.append(monomer['monomer_id'])
		try:
			cursor.execute("SELECT DISTINCT family_id FROM Belongs WHERE monomer_id = ANY(%s);",(monomers_id,))
		except psycopg2.Error as e:
			print (e.pgerror)
		family = cursor.fetchall()
		cursor.execute('commit')
		for i in family:
			if i['family_id'] not in families_id:
				families_id.append(i['family_id'])	
	families_id = list(map(int,families_id))
	try:
		cur.execute("SELECT family_id,family_name,superfamily_id,family_classification FROM Family WHERE family_id = ANY(%s);",(families_id,))
	except psycopg2.Error as e:
		print (e.pgerror)
	identifiant = cur.fetchall()
	for i in identifiant:
		if i['superfamily_id']:
			try:
				cur.execute("SELECT family_name FROM Family WHERE family_id = %s;",(i['superfamily_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			superfamily_name = cursor.fetchone()
			i['superfamily_name'] = superfamily_name[0]
		else:
			i['superfamily_name'] = None
		try:
			cursor.execute("SELECT count(monomer_id) FROM Belongs WHERE family_id = %s;",(i['family_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomers_number = cursor.fetchone()
		cursor.execute('commit')
		i['monomers_number'] = monomers_number[0]				
		seq_id.append(i)
		family_id_list.append(i['family_id'])
	return json.dumps(seq_id)


@app.route("/datagrid_monomers", methods=['POST', 'GET'])
def monomers_data():
	global block_id_list
	global block_id_selection_list
	global monomer_id_list
	global monomers_list
	global family_id_list
	monomers_list = []
	monomers_temp = []
	monomer_ids = []
	selection = request.args.getlist('selection[]',None)
	selection_family = request.args.getlist('selection_family[]',None)
	family_id_list = selection_family
	monomer_length = request.args.get('search[monomer_length]')
	monomer_position = request.args.get('search[monomer_begin_end]')
	print("monomer_position")
	print(monomer_position)
	selection = request.args.getlist('selection[]',None)
	length = None
	length_max = None
	length_min = None
	position = None
	position_min = None
	position_max = None
	seq_id = []
	families_id = []
	#Taille des blocks
	if monomer_length:
		list_length = monomer_length.rstrip("\n").split(":")
		if monomer_length.isdigit():
			length = int(monomer_length)
		elif len(list_length) == 2:
			if list_length[0].isdigit():
				length_min = int(list_length[0])
			if list_length[1].isdigit():
				length_max = int(list_length[1])
	if monomer_position:
		list_position = monomer_position.rstrip("\n").split(":")
		if monomer_position.isdigit():
			position = int(monomer_position)
		elif len(list_position) == 2:
			if list_position[0].isdigit():
				position_min = int(list_position[0])
			if list_position[1].isdigit():
				position_max = int(list_position[1])				

	if not selection and not selection_family:
		selection = list(map(int,block_id_list))
	elif selection and not selection_family:
		selection = list(map(int,selection))
	elif not selection and selection_family:
		try:
			cursor.execute("SELECT monomer_id FROM Belongs WHERE family_id IN (%s);",(tuple(selection_family),))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomers = cursor.fetchall()
		cursor.execute('commit')
		for i in monomers:
			monomer_ids.append(i['monomer_id'])
	if length:
		if selection_family:
			print("azerty")
			for idt in monomer_id_list:
				if idt in monomer_ids:
					try:
						cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length = %s;",(idt,length,))
					except psycopg2.Error as e:
						print (e.pgerror)
					monomers_infos = cur.fetchall()
					cur.execute('commit')
					try:
						cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
					except psycopg2.Error as e:
						print (e.pgerror)
					block_name = cursor.fetchone()
					cursor.execute('commit')
					#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
					if monomers_infos:
						try:
							cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						sequence_infos = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						chr_name = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						tx_name = cursor.fetchone()
						cursor.execute('commit')
					for monomer in monomers_infos:
						cursor.execute('commit')
						monomer['block_name'] = block_name[0]
						monomer['sequence_name'] = sequence_infos['sequence_name']
						monomer['chromosome_name'] = chr_name[0].split("|")[0]
						monomer['taxon_name'] = tx_name[0]
						monomers_temp.append(monomer['monomer_id'])
					monomer_id_list = monomers_temp
					monomers_list.extend(monomers_infos)
		else:
			for idt in selection:
				try:
					cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length = %s;",(idt,length,))
				except psycopg2.Error as e:
					print (e.pgerror)
				monomers_infos = cur.fetchall()
				cur.execute('commit')
				try:
					cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
				except psycopg2.Error as e:
					print (e.pgerror)
				block_name = cursor.fetchone()
				cursor.execute('commit')
				#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
				if monomers_infos:
					try:
						cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					sequence_infos = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					chr_name = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					tx_name = cursor.fetchone()
					cursor.execute('commit')
				for monomer in monomers_infos:
					cursor.execute('commit')
					monomer['block_name'] = block_name[0]
					monomer['sequence_name'] = sequence_infos['sequence_name']
					monomer['chromosome_name'] = chr_name[0].split("|")[0]
					monomer['taxon_name'] = tx_name[0]
					monomers_temp.append(monomer['monomer_id'])
				monomer_id_list = monomers_temp
				monomers_list.extend(monomers_infos)
	elif length_min and length_max:
		if selection_family:
			for idt in monomer_id_list:
				if idt in monomer_ids:
					try:
						cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length >= %s AND monomer_length <= %s;",(idt,length_min,length_max,))
					except psycopg2.Error as e:
						print (e.pgerror)
					monomers_infos = cur.fetchall()
					cur.execute('commit')
					try:
						cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
					except psycopg2.Error as e:
						print (e.pgerror)
					block_name = cursor.fetchone()
					cursor.execute('commit')
					#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
					if monomers_infos:
						try:
							cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						sequence_infos = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						chr_name = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						tx_name = cursor.fetchone()
						cursor.execute('commit')
					for monomer in monomers_infos:
						cursor.execute('commit')
						monomer['block_name'] = block_name[0]
						monomer['sequence_name'] = sequence_infos['sequence_name']
						monomer['chromosome_name'] = chr_name[0].split("|")[0]
						monomer['taxon_name'] = tx_name[0]
						monomers_temp.append(monomer['monomer_id'])
					monomer_id_list = monomers_temp
					monomers_list.extend(monomers_infos)
		else:
			for idt in selection:
				try:
					cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length >= %s AND monomer_length <= %s;",(idt,length_min,length_max,))
				except psycopg2.Error as e:
					print (e.pgerror)
				monomers_infos = cur.fetchall()
				cur.execute('commit')
				try:
					cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
				except psycopg2.Error as e:
					print (e.pgerror)
				block_name = cursor.fetchone()
				cursor.execute('commit')
				#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
				if monomers_infos:
					try:
						cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					sequence_infos = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					chr_name = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					tx_name = cursor.fetchone()
					cursor.execute('commit')
				for monomer in monomers_infos:
					cursor.execute('commit')
					monomer['block_name'] = block_name[0]
					monomer['sequence_name'] = sequence_infos['sequence_name']
					monomer['chromosome_name'] = chr_name[0].split("|")[0]
					monomer['taxon_name'] = tx_name[0]
					monomers_temp.append(monomer['monomer_id'])
				monomer_id_list = monomers_temp
				monomers_list.extend(monomers_infos)
	elif length_min and not length_max:
		if selection_family:
			for idt in monomer_id_list:
				if idt in monomer_ids:
					try:
						cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length >= %s;",(idt,length_min,))
					except psycopg2.Error as e:
						print (e.pgerror)
					monomers_infos = cur.fetchall()
					cur.execute('commit')
					try:
						cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
					except psycopg2.Error as e:
						print (e.pgerror)
					block_name = cursor.fetchone()
					cursor.execute('commit')
					#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
					if monomers_infos:
						try:
							cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						sequence_infos = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						chr_name = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						tx_name = cursor.fetchone()
						cursor.execute('commit')
					for monomer in monomers_infos:
						cursor.execute('commit')
						monomer['block_name'] = block_name[0]
						monomer['sequence_name'] = sequence_infos['sequence_name']
						monomer['chromosome_name'] = chr_name[0].split("|")[0]
						monomer['taxon_name'] = tx_name[0]
						monomers_temp.append(monomer['monomer_id'])
					monomer_id_list = monomers_temp
					monomers_list.extend(monomers_infos)
		else:
			for idt in selection:
				try:
					cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length >= %s;",(idt,length_min,))
				except psycopg2.Error as e:
					print (e.pgerror)
				monomers_infos = cur.fetchall()
				cur.execute('commit')
				try:
					cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
				except psycopg2.Error as e:
					print (e.pgerror)
				block_name = cursor.fetchone()
				cursor.execute('commit')
				#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
				if monomers_infos:
					try:
						cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					sequence_infos = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					chr_name = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					tx_name = cursor.fetchone()
					cursor.execute('commit')
				for monomer in monomers_infos:
					cursor.execute('commit')
					monomer['block_name'] = block_name[0]
					monomer['sequence_name'] = sequence_infos['sequence_name']
					monomer['chromosome_name'] = chr_name[0].split("|")[0]
					monomer['taxon_name'] = tx_name[0]
					monomers_temp.append(monomer['monomer_id'])
				monomer_id_list = monomers_temp
				monomers_list.extend(monomers_infos)
	elif not length_min and length_max:
		if selection_family:
			for idt in monomer_id_list:
				if idt in monomer_ids:
					try:
						cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length <= %s;",(idt,length_max,))
					except psycopg2.Error as e:
						print (e.pgerror)
					monomers_infos = cur.fetchall()
					cur.execute('commit')
					try:
						cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
					except psycopg2.Error as e:
						print (e.pgerror)
					block_name = cursor.fetchone()
					cursor.execute('commit')
					#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
					if monomers_infos:
						try:
							cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						sequence_infos = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						chr_name = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						tx_name = cursor.fetchone()
						cursor.execute('commit')
					for monomer in monomers_infos:
						cursor.execute('commit')
						monomer['block_name'] = block_name[0]
						monomer['sequence_name'] = sequence_infos['sequence_name']
						monomer['chromosome_name'] = chr_name[0].split("|")[0]
						monomer['taxon_name'] = tx_name[0]
						monomers_temp.append(monomer['monomer_id'])
					monomer_id_list = monomers_temp
					monomers_list.extend(monomers_infos)
		else:
			for idt in selection:
				try:
					cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length <= %s;",(idt,length_max,))
				except psycopg2.Error as e:
					print (e.pgerror)
				monomers_infos = cur.fetchall()
				cur.execute('commit')
				try:
					cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
				except psycopg2.Error as e:
					print (e.pgerror)
				block_name = cursor.fetchone()
				cursor.execute('commit')
				#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
				if monomers_infos:
					try:
						cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					sequence_infos = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					chr_name = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					tx_name = cursor.fetchone()
					cursor.execute('commit')
				for monomer in monomers_infos:
					cursor.execute('commit')
					monomer['block_name'] = block_name[0]
					monomer['sequence_name'] = sequence_infos['sequence_name']
					monomer['chromosome_name'] = chr_name[0].split("|")[0]
					monomer['taxon_name'] = tx_name[0]
					monomers_temp.append(monomer['monomer_id'])
				monomer_id_list = monomers_temp
				monomers_list.extend(monomers_infos)
	else:
		if selection_family:
			for idt in monomer_id_list:
				if idt in monomer_ids:
					try:
						cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand, block_id FROM Monomer WHERE monomer_id = %s;",(idt,))
					except psycopg2.Error as e:
						print (e.pgerror)
					monomers_infos = cur.fetchall()
					cur.execute('commit')
					try:
						cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(monomers_infos[0]['block_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					block_name = cursor.fetchone()
					cursor.execute('commit')
					#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
					if monomers_infos:
						try:
							cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						sequence_infos = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						chr_name = cursor.fetchone()
						cursor.execute('commit')
						try:
							cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						tx_name = cursor.fetchone()
						cursor.execute('commit')
					for monomer in monomers_infos:
						monomer['block_name'] = block_name[0]
						monomer['sequence_name'] = sequence_infos['sequence_name']
						monomer['chromosome_name'] = chr_name[0].split("|")[0]
						monomer['taxon_name'] = tx_name[0]
						monomers_temp.append(monomer['monomer_id'])
					monomer_id_list = monomers_temp
					monomers_list.extend(monomers_infos)

		else:
			for idt in selection:
				try:
					cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s;",(idt,))
				except psycopg2.Error as e:
					print (e.pgerror)
				monomers_infos = cur.fetchall()
				cur.execute('commit')
				try:
					cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
				except psycopg2.Error as e:
					print (e.pgerror)
				block_name = cursor.fetchone()
				cursor.execute('commit')
				#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
				if monomers_infos:
					try:
						cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					sequence_infos = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					chr_name = cursor.fetchone()
					cursor.execute('commit')
					try:
						cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
					except psycopg2.Error as e:
						print (e.pgerror)
					tx_name = cursor.fetchone()
					cursor.execute('commit')
				for monomer in monomers_infos:
					cursor.execute('commit')
					monomer['block_name'] = block_name[0]
					monomer['sequence_name'] = sequence_infos['sequence_name']
					monomer['chromosome_name'] = chr_name[0].split("|")[0]
					monomer['taxon_name'] = tx_name[0]
					monomers_temp.append(monomer['monomer_id'])
				monomer_id_list = monomers_temp
				monomers_list.extend(monomers_infos)
	if monomer_position:
		monomers_list2 = monomers_list
		monomers_list = []
		if position:
			for mono in monomers_list2:
				if int(mono['monomer_begin_sequence']) <= position and int(mono['monomer_end_sequence']) >= position:
					monomers_list.append(mono)
		elif position_min and position_max:
			for mono in monomers_list2:
				if (int(mono['monomer_begin_sequence']) >= position_min and int(mono['monomer_begin_sequence']) <= position_max) or (int(mono['monomer_end_sequence']) <= position_min and int(mono['monomer_end_sequence']) >= position_max) or (int(mono['monomer_begin_sequence']) <= position_min and int(mono['monomer_end_sequence']) >= position_max):
					monomers_list.append(mono)
		elif position_min and not position_max:
			for mono in monomers_list2:
				if int(mono['monomer_end_sequence']) >= position_min:
					monomers_list.append(mono)
		elif position_max and not position_min:
			for mono in monomers_list2:
				if int(mono['monomer_begin_sequence']) <= position_max:
					monomers_list.append(mono)
	return json.dumps(monomers_list)
def monomer_name(name_format,identifiant,separator_format):
	liste_name= []
	print(separator_format)
	if name_format == "database_name":
		try:
			cursor.execute("SELECT monomer_name FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_database = cursor.fetchone()
		cursor.execute('commit')
		name = monomer_database['monomer_name']
	else:
		if "block_name" in name_format:
			try:
				cursor.execute("SELECT block_id FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)		
			block_id = cursor.fetchone()
			cursor.execute('commit')					
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s ;",(block_id[0],) )
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(block_name['block_name'])
		if "monomer_id" in name_format:
			liste_name.append(identifiant)
		if "begin" in name_format:
			try:
				cursor.execute("SELECT monomer_begin FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_begin = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_begin['monomer_begin']))
		if "end" in name_format:
			try:
				cursor.execute("SELECT monomer_end FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_end = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_end['monomer_end']))
		if "strand" in name_format:
			try:
				cursor.execute("SELECT monomer_strand FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_strand = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_strand['monomer_strand']))
		if "monomer_length" in name_format:
			try:
				cursor.execute("SELECT monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_length = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_length['monomer_length']))
		liste_name2 = list(map(str,liste_name))
		name = separator_format.join(liste_name2)
	return(name)
	
def hidden_encode(data):
	return base64.b64encode(json.dumps(data).encode('utf-8')).decode('ascii')
	
def hidden_decode(text):
	return json.loads(base64.b64decode(text).decode('utf-8'))

@app.route("/alphasat/accueil" , methods=['POST', 'GET'])
def accueil():
	return render_template("alphasat_accueil.html")

@app.route("/alphasat/contact", methods=['POST', 'GET'])
def contact():
	return render_template("contact.html")
	
@app.route("/alphasat/help_and_documentation", methods=['POST', 'GET'])
def documentation():
	return render_template("help_and_documentation.html")
	
if __name__ == "__main__":
	app.run(port=PORT, host="0.0.0.0", debug=True)

