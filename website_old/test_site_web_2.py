from ORM import *
from flask import *
import io
import json
import ORM
import pymongo
import ipdb
import gzip
import json
import base64
import re
import time
import psycopg2
import psycopg2.extras
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
app = Flask(__name__)

"""Connexion a la base de donnees"""
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)


@app.route("/")
def hello():
	return ("Hello Loic !!!!")

#Fonction utile benchmark
@app.route("/list/Taxon", methods=['POST', 'GET'])
#affiche tous les taxons de la base
def taxon_all():
	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	cursor.execute("SELECT taxon_id, taxon_name from Taxon ;")
	FF1 = time.time()
	print( "temps de la requete 1 %f" %(FF1 - F1))
	results = cursor.fetchall()
	resultsf = dict()
	taxon_name = list()
	for i in results:
		taxon_name.append(i[1])
		resultsf[i[1]] =  i[0]
	taxon_encode = dumps(resultsf)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction list/Taxon : %f" %(FT1 - T1))
	print("Temps CPU de la fonction list/Taxon : %f" %(FT2 - T2))	
	return render_template("taxon_selection.html", taxon_name = taxon_name, taxon_dict = taxon_encode)

def hidden_encode(data):
	return base64.b64encode(json.dumps(data).encode('utf-8')).decode('ascii')
	
def hidden_decode(text):
	return json.loads(base64.b64decode(text).decode('utf-8'))

#Fonction utilise benchmark
@app.route("/taxons/chromosomes", methods=['POST', 'GET'])
def taxon_chromosomes():
	T1 = time.time()
	T2 = time.clock()
	#Noms des taxons
	list_taxon_name = request.form.getlist('Taxons')
	#Liste des identifiants des taxons
	taxon_list = request.form.getlist('taxon_dict')[0]
	#Dictionnaire des noms des taxons et de leurs identifiants
	taxon_decode = loads(taxon_list)
	chromosomes_list = dict()
	family_name = list()
	family = dict()
	chromosome_name_id = dict()
	taxon_dict2 = dict()  
	#Identifiant du taxon selectionne
	taxon_id = [taxon_decode[name] for name in list_taxon_name]
	#Requetes liste des chromosomes avec l'identifiant du taxon selectionne
	chromosomes = list()
	F1 = time.time()
	cursor.execute("SELECT chromosome_id, chromosome_name, taxon_id FROM Chromosome WHERE taxon_id IN %s ;", (tuple(taxon_id),))
	FF1 = time.time()
	print(" temps de la requete 1 %f" %(FF1 - F1))
	chromosomes = cursor.fetchall()
	#print(chromosomes)
	#Dictionnaire des noms et identifiants des chromosomes
	chromosomes_id = {d[1] : d[0] for d in chromosomes}
	#Liste des identifiants des chromosomes
	all_chromosomes_id = [d[0] for d in chromosomes]
	#Dictionnaire des noms des taxons et de leurs identifiants
	dict2 = {str(taxon_decode[d]) : d for d in taxon_decode}
	#Dictionnaire des identifiants des taxons : noms des chromosomes
	#Modifications:
	#Liste de tri croissant des caracteres ASCII des clés du dictionnaire
	#Dictionnaire des noms des chromosomes (nomEspece+nomChro) et des idenfiants des chromosomes
	chromosome_list = {"%s : %s" %(dict2[str(d[2])], d[1]) : d[0]for d in chromosomes}
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction /taxons/chromosomes : %f" %(FT1 - T1))
	print("Temps CPU de la fonction /taxons/chromosomes : %f" %(FT2 - T2))
	return render_template("taxons_chromosomes_test.html", chromosome = chromosome_list,list_taxon = taxon_id, chro_dict = dumps(all_chromosomes_id)) #family = family,

def gen_chromosome_id_list(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	chromosome_id = list()
	for name in chromosome_list:
		(taxon_name, chromosome_name) = name.split(" : ")
		cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = %s ;", (taxon_name,))
		taxon_identifiant = cursor.fetchall()
		taxon_id = taxon_identifiant[0]
		cursor.execute("SELECT chromosome_id FROM Chromosomes WHERE chromosome_name = %s AND taxon_id = %s ;", (chromosome_name, taxon_id,))
		chr_id = cursor.fetchall()
		chromosome_id.append(chr_id)
	FF1 = time.time()
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction gen_chromosome_id_list() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction gen_chromosome_id_list() : %f" %(FT2 - T2))
	return chromosome_id
   
def find_blocks(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	cursor.execute("SELECT block_id FROM Block where chromosome_id IN %s ;", (tuple(chromosome_list),))
	FF1 = time.time()
	print("Temps de la requete 1 %f" %(FF1 - F1))
	block_identifiant = cursor.fetchall()
	blocks = [d for d in block_identifiant]
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction find_blocks() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction find_blocks() : %f" %(FT2 - T2))
	return blocks

def find_monomers(chromosome_list):
	## Voir pour passer directement par chromosome ##
	T1 = time.time()
	T2 = time.clock()
	#cursor.execute("SELECT block_id FROM Block WHERE chromosome_id IN %s ;", (chromosome_list,))
	#block_identifiant = cursor.fetchall()
	#blocks = [d for d in block_identifiant]
	cursor.execute("SELECT monomer_id FROM Monomer WHERE chromosome_id IN %s ;", (chromosome_list,))
	monomer_identifiant = cursor.fetchall()
	monomers = [d[0] for d in monomer_identifiant]
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction find_monomers() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction find_monomers() : %f" %(FT2 - T2))
	return monomers

#Fonction utile benchmark
@app.route("/taxons/filters", methods=['POST', 'GET'])
def filters():
	T1 = time.time()
	T2 = time.clock()
	#Liste de tous les chromosomes selectionnes de la requete choix des chromosomes 
	chromosome_list = request.form.getlist('Chromosomes')
	family_list = request.form.getlist('Family')
	base_begin = request.form.getlist('base_pair_begin')[0]
	base_end = request.form.getlist('base_pair_end')[0]
	distance_centro = request.form.getlist('distance_from_centromer')[0]
	block_name = request.form.getlist('textarea')[0]
	taxon_list = request.form.getlist('list_taxon')
	#Liste de tous les identifiants de chromosomes de l'espece
	all_chromosomes_id = loads(request.form.getlist('list_chromosomes')[0])
	#print(all_chromosomes_id)
	aa = [str(i) for i in all_chromosomes_id]
	#print(aa[1:10])
	liste_chrs = open("list_chrs", "w")
	liste_chrs.write(",".join(aa))
	liste_chrs.close()
	chromosome_id = list()
	monomers = list()
	blocks = list()
	family_name_show = list()
	
	##Conditions si les differents champs sont remplis##
	
	if not isinstance(base_end,str):
		b_end = int(base_end)
	else:
		b_end = 4000000000
	if not isinstance(base_begin,str):
		b_begin = int(base_begin)
	else:
	   b_begin = 0
	if not isinstance(distance_centro,str):
		distance_centro = int(distance_centro)

	#Selection par noms de chromosomes
	if isinstance(chromosome_list,list):
		chromosome_list = [d for d in chromosome_list]
		#print("liste chro")
	else:
		#print(" pas une liste chros")
		chromosome_list = all_chromosomes_id
		chromosome_list = [str(d) for d in chromosome_list]
		chromosome_name = list()
		chromosome_list = [ObjectId(d) for d in chromosome_list]
		#print(chromosome_list)
		#Tri de la liste de chromosomes selectionnes
		chromosome_list2 = sorted(chromosome_list)
	#cursor.execute("SELECT block_id FROM Block where chromosome_id IN %s ;", (tuple(all_chromosomes_id),))
	#all_blocks_id = list()
	#for i in cursor:
	#	all_blocks_id.append(i[0])
	F1 = time.time()
	cursor.execute("SELECT count(monomer_id) FROM Monomer where chromosome_id IN %s ;", (tuple(all_chromosomes_id),))
	FF1 = time.time()
	print("Temps de la requete 1 %f" %(FF1 - F1))
	monomerstot = cursor.fetchone()
	print("comptage monomer fait")
	F2 = time.time()
	cursor.execute("SELECT count(block_id) FROM Block where chromosome_id IN %s ;", (tuple(all_chromosomes_id),))
	FF2 = time.time()
	print("Temps de la requete 2 %f" %(FF2 - F2))
	blockstot = cursor.fetchone()
	print("comptage block fait")
	F3 = time.time()
	if not base_begin or not base_end:
		print("pas de begin end")
		cursor.execute("SELECT monomer_id FROM Monomer where chromosome_id IN %s ;", (tuple(chromosome_list),))
	else:	
		cursor.execute("SELECT monomer_id FROM Monomer where chromosome_id IN %s AND monomer_begin >= %s AND monomer_end <= %s ;", (tuple(chromosome_list), b_begin, b_end,))
	FF3 = time.time()
	print("temps de la requete 3 %f" %(FF3 -F3))
	monomer_identifiant = cursor.fetchall()
	monomers = [d[0] for d in monomer_identifiant]
	blocks = find_blocks(chromosome_list)

	#Selection par noms de blocks
	if not isinstance(block_name,str):
		monomers2 = []
		block_list = block_name.split("\r\n")
		cursor.execute("SELECT block_id FROM Block WHERE block_name IN %s ;", (tuple(block_list),))
		blocks_id = cursor.fetchall()
		cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id IN %s ;", (tuple(blocks_id),))
		monomers2 = cursor.fetchall()
		monomers2 = [d[0] for d in monomers2]
		monomers = [element for element in monomers if element in monomers2 ]
		block_name = block_list
		#modification
#	else:
#		print("une chaine block")
	monomers_number = len(monomers)
	blocks_number = len(blocks)
	F4 = time.time()
	monomers_list_encode = dumps([d for d in monomers])
	blocks_list_encode = dumps([d for d in blocks])
	FF4 = time.time()
	print("Temps de la requete 4 %f" %(FF4 - F4)) 
	#Liste des identifiants des chromosomes selectionnes
	F5 = time.time()	
	cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id IN %s ;", (tuple(chromosome_list),))
	chromosome_list_to_name = cursor.fetchall()
	FF5 = time.time()
	print("Temps de la requete 5 %f" %(FF5 - F5))
	#Liste des noms des chromosomes selectionnes
	chromosome_name_show = [d[0] for d in chromosome_list_to_name]
	#Modifications:
	#Tri des caracteres ASCII des noms des chromosomes par ordre croissant
	chromosome_name_show = sorted(chromosome_name_show)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction /taxons/filters : %f" %(FT1 - T1))
	print("Temps CPU de la fonction /taxons/filters : %f" %(FT2 - T2))
	TEST = time.time()
	TEST2 = time.clock()
	#cursor.execute("SELECT * FROM Monomer ;")
	cursor.execute("SELECT monomer_id FROM Monomer where chromosome_id IN %s ;", (tuple(all_chromosomes_id),))
	FTEST = time.time()
	FTEST2 = time.clock()
	print("Temps total requete test : %f" %(FTEST - TEST))
	print("Temps CPU requete test : %f" %(FTEST2 - TEST2))
	a = cursor.fetchall()
	#print(a[0:10])
	print("taille de a",len(a))
	return render_template("show_filters.html", chromosomes = chromosome_list, family = family_list, monomers = monomers_number,
	monomerstot = monomerstot[0], blocks = blocks_number, blockstot = blockstot[0],
	monomers_list_encode = monomers_list_encode, blocks_list_encode = blocks_list_encode,
	base_begin = base_begin, base_end = base_end, distance_centro = distance_centro,
	block_name = block_name, family_name_show = family_name_show, chromosome_name_show = chromosome_name_show )
	
@app.route("/alphasat/accueil" , methods=['POST', 'GET'])
def accueil():
	return render_template("alphasat_accueil.html")

@app.route("/alphasat/contact", methods=['POST', 'GET'])
def contact():
	return render_template("contact.html")
	
@app.route("/alphasat/help_and_documentation", methods=['POST', 'GET'])
def documentation():
	return render_template("help_and_documentation.html")
	
if __name__ == "__main__":
	app.run(port=PORT, host="0.0.0.0", debug=True)
